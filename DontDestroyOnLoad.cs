﻿using System;
using UnityEngine;

public class DontDestroyOnLoad : MonoBehaviour
{
	private void OnEnable()
	{
		UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
	}
}

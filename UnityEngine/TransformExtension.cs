﻿using System;
using System.Collections.Generic;

namespace UnityEngine
{
	public static class TransformExtension
	{
		public static T GetOrAddComponent<T>(this Transform transform) where T : Component
		{
			return transform.gameObject.getOrAddComponent<T>();
		}

		public static void FindAllChildrenWithName(this Transform transform, string name, List<GameObject> gameObjects)
		{
			int childCount = transform.childCount;
			for (int i = 0; i < childCount; i++)
			{
				Transform child = transform.GetChild(i);
				if (child.name == name)
				{
					gameObjects.Add(child.gameObject);
				}
				child.FindAllChildrenWithName(name, gameObjects);
			}
		}
	}
}

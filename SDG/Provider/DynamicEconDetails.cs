﻿using System;
using UnityEngine;

namespace SDG.Provider
{
	public struct DynamicEconDetails
	{
		public DynamicEconDetails(string tags, string dynamic_props)
		{
			this.tags = ((!string.IsNullOrEmpty(tags)) ? tags : string.Empty);
			this.dynamic_props = ((!string.IsNullOrEmpty(dynamic_props)) ? dynamic_props : string.Empty);
		}

		public bool getStatTrackerType(out EStatTrackerType type)
		{
			type = EStatTrackerType.NONE;
			if (this.tags.Contains("stat_tracker:total_kills"))
			{
				type = EStatTrackerType.TOTAL;
				return true;
			}
			if (this.tags.Contains("stat_tracker:player_kills"))
			{
				type = EStatTrackerType.PLAYER;
				return true;
			}
			return false;
		}

		public bool getStatTrackerValue(out EStatTrackerType type, out int kills)
		{
			kills = -1;
			if (!this.getStatTrackerType(out type))
			{
				return false;
			}
			if (type == EStatTrackerType.TOTAL)
			{
				if (string.IsNullOrEmpty(this.dynamic_props))
				{
					kills = 0;
				}
				else
				{
					kills = JsonUtility.FromJson<StatTrackerTotalKillsJson>(this.dynamic_props).total_kills;
				}
				return true;
			}
			if (type != EStatTrackerType.PLAYER)
			{
				return false;
			}
			if (string.IsNullOrEmpty(this.dynamic_props))
			{
				kills = 0;
			}
			else
			{
				kills = JsonUtility.FromJson<StatTrackerPlayerKillsJson>(this.dynamic_props).player_kills;
			}
			return true;
		}

		public string getPredictedDynamicPropsJsonForStatTracker(EStatTrackerType type, int kills)
		{
			if (type == EStatTrackerType.TOTAL)
			{
				return JsonUtility.ToJson(new StatTrackerTotalKillsJson
				{
					total_kills = kills
				});
			}
			if (type != EStatTrackerType.PLAYER)
			{
				return string.Empty;
			}
			return JsonUtility.ToJson(new StatTrackerPlayerKillsJson
			{
				player_kills = kills
			});
		}

		public string tags;

		public string dynamic_props;
	}
}

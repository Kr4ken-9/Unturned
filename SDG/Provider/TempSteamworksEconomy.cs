﻿using System;
using System.Collections.Generic;
using System.Globalization;
using SDG.Framework.Debug;
using SDG.Framework.IO.Deserialization;
using SDG.SteamworksProvider;
using SDG.Unturned;
using Steamworks;
using UnityEngine;

namespace SDG.Provider
{
	public class TempSteamworksEconomy
	{
		public TempSteamworksEconomy(SteamworksAppInfo newAppInfo)
		{
			this.appInfo = newAppInfo;
			IDeserializer deserializer = new JSONDeserializer();
			string path = ReadWrite.PATH + "/EconInfo.json";
			TempSteamworksEconomy.econInfo = deserializer.deserialize<List<UnturnedEconInfo>>(path);
			if (this.appInfo.isDedicated)
			{
				this.inventoryResultReady = Callback<SteamInventoryResultReady_t>.CreateGameServer(new Callback<SteamInventoryResultReady_t>.DispatchDelegate(this.onInventoryResultReady));
			}
			else
			{
				this.inventoryResultReady = Callback<SteamInventoryResultReady_t>.Create(new Callback<SteamInventoryResultReady_t>.DispatchDelegate(this.onInventoryResultReady));
				this.gameOverlayActivated = Callback<GameOverlayActivated_t>.Create(new Callback<GameOverlayActivated_t>.DispatchDelegate(this.onGameOverlayActivated));
			}
		}

		public bool canOpenInventory
		{
			get
			{
				return SteamUtils.IsOverlayEnabled();
			}
		}

		public void open(ulong id)
		{
			SteamFriends.ActivateGameOverlayToWebPage(string.Concat(new object[]
			{
				"http://steamcommunity.com/profiles/",
				SteamUser.GetSteamID(),
				"/inventory/?sellOnLoad=1#",
				SteamUtils.GetAppID(),
				"_2_",
				id
			}));
		}

		public static List<UnturnedEconInfo> econInfo { get; private set; }

		public SteamItemDetails_t[] inventory
		{
			get
			{
				return this.inventoryDetails;
			}
		}

		public ulong getInventoryPackage(int item)
		{
			if (this.inventoryDetails != null)
			{
				for (int i = 0; i < this.inventoryDetails.Length; i++)
				{
					if (this.inventoryDetails[i].m_iDefinition.m_SteamItemDef == item)
					{
						return this.inventoryDetails[i].m_itemId.m_SteamItemInstanceID;
					}
				}
			}
			return 0UL;
		}

		public int countInventoryPackages(int item)
		{
			int num = 0;
			if (this.inventoryDetails != null)
			{
				for (int i = 0; i < this.inventoryDetails.Length; i++)
				{
					if (this.inventoryDetails[i].m_iDefinition.m_SteamItemDef == item)
					{
						num++;
					}
				}
			}
			return num;
		}

		public bool getInventoryPackages(int item, ulong[] instances)
		{
			int num = 0;
			if (this.inventoryDetails != null)
			{
				for (int i = 0; i < this.inventoryDetails.Length; i++)
				{
					if (this.inventoryDetails[i].m_iDefinition.m_SteamItemDef == item)
					{
						instances[num] = this.inventoryDetails[i].m_itemId.m_SteamItemInstanceID;
						num++;
						if (num == instances.Length)
						{
							return true;
						}
					}
				}
			}
			return false;
		}

		public int getInventoryItem(ulong package)
		{
			if (this.inventoryDetails != null)
			{
				for (int i = 0; i < this.inventoryDetails.Length; i++)
				{
					if (this.inventoryDetails[i].m_itemId.m_SteamItemInstanceID == package)
					{
						return this.inventoryDetails[i].m_iDefinition.m_SteamItemDef;
					}
				}
			}
			return 0;
		}

		public string getInventoryTags(ulong instance)
		{
			DynamicEconDetails dynamicEconDetails;
			if (!this.dynamicInventoryDetails.TryGetValue(instance, out dynamicEconDetails))
			{
				return string.Empty;
			}
			return dynamicEconDetails.tags;
		}

		public string getInventoryDynamicProps(ulong instance)
		{
			DynamicEconDetails dynamicEconDetails;
			if (!this.dynamicInventoryDetails.TryGetValue(instance, out dynamicEconDetails))
			{
				return string.Empty;
			}
			return dynamicEconDetails.dynamic_props;
		}

		public bool getInventoryStatTrackerValue(ulong instance, out EStatTrackerType type, out int kills)
		{
			type = EStatTrackerType.NONE;
			kills = -1;
			DynamicEconDetails dynamicEconDetails;
			return this.dynamicInventoryDetails.TryGetValue(instance, out dynamicEconDetails) && dynamicEconDetails.getStatTrackerValue(out type, out kills);
		}

		public string getInventoryName(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			if (unturnedEconInfo == null)
			{
				return string.Empty;
			}
			return unturnedEconInfo.name;
		}

		public string getInventoryType(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			if (unturnedEconInfo == null)
			{
				return string.Empty;
			}
			return unturnedEconInfo.type;
		}

		public string getInventoryDescription(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			if (unturnedEconInfo == null)
			{
				return string.Empty;
			}
			return unturnedEconInfo.description;
		}

		public bool getInventoryMarketable(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			return unturnedEconInfo != null && unturnedEconInfo.marketable;
		}

		public bool getInventoryScrapable(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			return unturnedEconInfo != null && unturnedEconInfo.scrapable;
		}

		public Color getInventoryColor(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			if (unturnedEconInfo == null)
			{
				return Color.white;
			}
			uint num;
			if (unturnedEconInfo.name_color != null && unturnedEconInfo.name_color.Length > 0 && uint.TryParse(unturnedEconInfo.name_color, NumberStyles.HexNumber, CultureInfo.CurrentCulture, out num))
			{
				uint num2 = num >> 16 & 255u;
				uint num3 = num >> 8 & 255u;
				uint num4 = num & 255u;
				return new Color(num2 / 255f, num3 / 255f, num4 / 255f);
			}
			return Color.white;
		}

		public Color getStatTrackerColor(EStatTrackerType type)
		{
			if (type == EStatTrackerType.NONE)
			{
				return Color.white;
			}
			if (type == EStatTrackerType.TOTAL)
			{
				return new Color(1f, 0.5f, 0f);
			}
			if (type != EStatTrackerType.PLAYER)
			{
				return Color.black;
			}
			return new Color(1f, 0f, 0f);
		}

		public string getStatTrackerPropertyName(EStatTrackerType type)
		{
			if (type == EStatTrackerType.TOTAL)
			{
				return "total_kills";
			}
			if (type != EStatTrackerType.PLAYER)
			{
				return null;
			}
			return "player_kills";
		}

		public ushort getInventoryMythicID(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			if (unturnedEconInfo == null)
			{
				return 0;
			}
			return (ushort)unturnedEconInfo.item_effect;
		}

		public void getInventoryTargetID(int item, out ushort item_id, out ushort vehicle_id)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			if (unturnedEconInfo == null)
			{
				item_id = 0;
				vehicle_id = 0;
				return;
			}
			item_id = (ushort)unturnedEconInfo.item_id;
			vehicle_id = (ushort)unturnedEconInfo.vehicle_id;
		}

		public ushort getInventoryItemID(int item)
		{
			ushort result;
			ushort num;
			this.getInventoryTargetID(item, out result, out num);
			return result;
		}

		public ushort getInventorySkinID(int item)
		{
			UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == item);
			if (unturnedEconInfo == null)
			{
				return 0;
			}
			return (ushort)unturnedEconInfo.item_skin;
		}

		public void consumeItem(ulong instance)
		{
			Terminal.print("Consume item: " + instance, null, Provider.STEAM_IC, Provider.STEAM_DC, true);
			SteamInventoryResult_t steamInventoryResult_t;
			SteamInventory.ConsumeItem(out steamInventoryResult_t, (SteamItemInstanceID_t)instance, 1u);
		}

		public void exchangeInventory(int generate, params ulong[] destroy)
		{
			Debug.Log("Exchange these item instances for " + generate);
			foreach (ulong num in destroy)
			{
				int inventoryItem = this.getInventoryItem(num);
				Debug.Log(string.Concat(new object[]
				{
					inventoryItem,
					" [",
					num,
					"]"
				}));
			}
			SteamItemDef_t[] array = new SteamItemDef_t[1];
			uint[] array2 = new uint[1];
			array[0] = (SteamItemDef_t)generate;
			array2[0] = 1u;
			SteamItemInstanceID_t[] array3 = new SteamItemInstanceID_t[destroy.Length];
			uint[] array4 = new uint[destroy.Length];
			for (int j = 0; j < destroy.Length; j++)
			{
				array3[j] = (SteamItemInstanceID_t)destroy[j];
				array4[j] = 1u;
			}
			SteamInventory.ExchangeItems(out this.exchangeResult, array, array2, (uint)array.Length, array3, array4, (uint)array3.Length);
		}

		public void updateInventory()
		{
			if (Time.realtimeSinceStartup - this.lastHeartbeat < 30f)
			{
				return;
			}
			this.lastHeartbeat = Time.realtimeSinceStartup;
			SteamInventory.SendItemDropHeartbeat();
		}

		public void dropInventory()
		{
			SteamInventory.TriggerItemDrop(out this.dropResult, (SteamItemDef_t)10000);
		}

		public void refreshInventory()
		{
			if (!SteamInventory.GetAllItems(out this.inventoryResult))
			{
				Provider.isLoadingInventory = false;
			}
		}

		public void addLocalItem(SteamItemDetails_t item, string tags, string dynamic_props)
		{
			SteamItemDetails_t[] array = new SteamItemDetails_t[this.inventory.Length + 1];
			for (int i = 0; i < this.inventory.Length; i++)
			{
				array[i] = this.inventory[i];
			}
			array[this.inventory.Length] = item;
			this.inventoryDetails = array;
			this.dynamicInventoryDetails.Remove(item.m_itemId.m_SteamItemInstanceID);
			DynamicEconDetails value = default(DynamicEconDetails);
			value.tags = ((!string.IsNullOrEmpty(tags)) ? tags : string.Empty);
			value.dynamic_props = ((!string.IsNullOrEmpty(dynamic_props)) ? dynamic_props : string.Empty);
			this.dynamicInventoryDetails.Add(item.m_itemId.m_SteamItemInstanceID, value);
		}

		private void onInventoryResultReady(SteamInventoryResultReady_t callback)
		{
			if (this.appInfo.isDedicated)
			{
				SteamPending steamPending = null;
				for (int i = 0; i < Provider.pending.Count; i++)
				{
					if (Provider.pending[i].inventoryResult == callback.m_handle)
					{
						steamPending = Provider.pending[i];
						break;
					}
				}
				if (steamPending == null)
				{
					return;
				}
				if (callback.m_result != EResult.k_EResultOK || !SteamGameServerInventory.CheckResultSteamID(callback.m_handle, steamPending.playerID.steamID))
				{
					Debug.Log(string.Concat(new object[]
					{
						"inventory auth: ",
						callback.m_result,
						" ",
						SteamGameServerInventory.CheckResultSteamID(callback.m_handle, steamPending.playerID.steamID)
					}));
					Provider.reject(steamPending.playerID.steamID, ESteamRejection.AUTH_ECON_VERIFY);
					return;
				}
				uint num = 0u;
				if (SteamGameServerInventory.GetResultItems(callback.m_handle, null, ref num) && num > 0u)
				{
					steamPending.inventoryDetails = new SteamItemDetails_t[num];
					SteamGameServerInventory.GetResultItems(callback.m_handle, steamPending.inventoryDetails, ref num);
					for (uint num2 = 0u; num2 < num; num2 += 1u)
					{
						uint num3 = 1024u;
						string text;
						SteamGameServerInventory.GetResultItemProperty(callback.m_handle, num2, "tags", out text, ref num3);
						uint num4 = 1024u;
						string text2;
						SteamGameServerInventory.GetResultItemProperty(callback.m_handle, num2, "dynamic_props", out text2, ref num4);
						DynamicEconDetails value = default(DynamicEconDetails);
						value.tags = ((!string.IsNullOrEmpty(text)) ? text : string.Empty);
						value.dynamic_props = ((!string.IsNullOrEmpty(text2)) ? text2 : string.Empty);
						steamPending.dynamicInventoryDetails.Add(steamPending.inventoryDetails[(int)((UIntPtr)num2)].m_itemId.m_SteamItemInstanceID, value);
					}
				}
				steamPending.inventoryDetailsReady();
			}
			else if (this.promoResult != SteamInventoryResult_t.Invalid && callback.m_handle == this.promoResult)
			{
				SteamInventory.DestroyResult(this.promoResult);
				this.promoResult = SteamInventoryResult_t.Invalid;
			}
			else if (this.exchangeResult != SteamInventoryResult_t.Invalid && callback.m_handle == this.exchangeResult)
			{
				SteamItemDetails_t[] array = null;
				string tags = null;
				string dynamic_props = null;
				uint num5 = 0u;
				if (SteamInventory.GetResultItems(this.exchangeResult, null, ref num5) && num5 > 0u)
				{
					array = new SteamItemDetails_t[num5];
					SteamInventory.GetResultItems(this.exchangeResult, array, ref num5);
					uint num6 = 1024u;
					SteamInventory.GetResultItemProperty(this.exchangeResult, num5 - 1u, "tags", out tags, ref num6);
					uint num7 = 1024u;
					SteamInventory.GetResultItemProperty(this.exchangeResult, num5 - 1u, "dynamic_props", out dynamic_props, ref num7);
				}
				Terminal.print("onInventoryResultReady: Exchange " + num5, null, Provider.STEAM_IC, Provider.STEAM_DC, true);
				if (array != null && num5 > 0u)
				{
					SteamItemDetails_t item = array[(int)((UIntPtr)(num5 - 1u))];
					this.addLocalItem(item, tags, dynamic_props);
					if (this.onInventoryExchanged != null)
					{
						this.onInventoryExchanged(item.m_iDefinition.m_SteamItemDef, item.m_unQuantity, item.m_itemId.m_SteamItemInstanceID);
					}
					this.refreshInventory();
				}
				SteamInventory.DestroyResult(this.exchangeResult);
				this.exchangeResult = SteamInventoryResult_t.Invalid;
			}
			else if (this.dropResult != SteamInventoryResult_t.Invalid && callback.m_handle == this.dropResult)
			{
				SteamItemDetails_t[] array2 = null;
				string tags2 = null;
				string dynamic_props2 = null;
				uint num8 = 0u;
				if (SteamInventory.GetResultItems(this.dropResult, null, ref num8) && num8 > 0u)
				{
					array2 = new SteamItemDetails_t[num8];
					SteamInventory.GetResultItems(this.dropResult, array2, ref num8);
					uint num9 = 1024u;
					SteamInventory.GetResultItemProperty(this.dropResult, 0u, "tags", out tags2, ref num9);
					uint num10 = 1024u;
					SteamInventory.GetResultItemProperty(this.dropResult, 0u, "dynamic_props", out dynamic_props2, ref num10);
				}
				Terminal.print("onInventoryResultReady: Drop " + num8, null, Provider.STEAM_IC, Provider.STEAM_DC, true);
				if (array2 != null && num8 > 0u)
				{
					SteamItemDetails_t item2 = array2[0];
					this.addLocalItem(item2, tags2, dynamic_props2);
					if (this.onInventoryDropped != null)
					{
						this.onInventoryDropped(item2.m_iDefinition.m_SteamItemDef, item2.m_unQuantity, item2.m_itemId.m_SteamItemInstanceID);
					}
					this.refreshInventory();
				}
				SteamInventory.DestroyResult(this.dropResult);
				this.dropResult = SteamInventoryResult_t.Invalid;
			}
			else if (this.inventoryResult != SteamInventoryResult_t.Invalid && callback.m_handle == this.inventoryResult)
			{
				this.dynamicInventoryDetails.Clear();
				uint num11 = 0u;
				if (SteamInventory.GetResultItems(this.inventoryResult, null, ref num11) && num11 > 0u)
				{
					this.inventoryDetails = new SteamItemDetails_t[num11];
					SteamInventory.GetResultItems(this.inventoryResult, this.inventoryDetails, ref num11);
					for (uint num12 = 0u; num12 < num11; num12 += 1u)
					{
						uint num13 = 1024u;
						string text3;
						SteamInventory.GetResultItemProperty(this.inventoryResult, num12, "tags", out text3, ref num13);
						uint num14 = 1024u;
						string text4;
						SteamInventory.GetResultItemProperty(this.inventoryResult, num12, "dynamic_props", out text4, ref num14);
						DynamicEconDetails value2 = default(DynamicEconDetails);
						value2.tags = ((!string.IsNullOrEmpty(text3)) ? text3 : string.Empty);
						value2.dynamic_props = ((!string.IsNullOrEmpty(text4)) ? text4 : string.Empty);
						this.dynamicInventoryDetails.Add(this.inventoryDetails[(int)((UIntPtr)num12)].m_itemId.m_SteamItemInstanceID, value2);
					}
				}
				if (this.onInventoryRefreshed != null)
				{
					this.onInventoryRefreshed();
				}
				this.isInventoryAvailable = true;
				Provider.isLoadingInventory = false;
				SteamInventory.DestroyResult(this.inventoryResult);
				this.inventoryResult = SteamInventoryResult_t.Invalid;
			}
			else if (this.commitResult != SteamInventoryResult_t.Invalid && callback.m_handle == this.commitResult)
			{
				Debug.Log("Commit dynamic properties result: " + callback.m_result);
				SteamInventory.DestroyResult(this.commitResult);
				this.commitResult = SteamInventoryResult_t.Invalid;
			}
		}

		private void onGameOverlayActivated(GameOverlayActivated_t callback)
		{
			if (callback.m_bActive == 0)
			{
				this.refreshInventory();
			}
		}

		public void loadTranslationEconInfo()
		{
			string path = ReadWrite.PATH + "/EconInfo_" + Provider.language;
			if (!ReadWrite.fileExists(path, false, false))
			{
				path = Provider.path + Provider.language + "/EconInfo.json";
				if (!ReadWrite.fileExists(path, false, false))
				{
					return;
				}
			}
			IDeserializer deserializer = new JSONDeserializer();
			List<UnturnedEconInfo> list = new List<UnturnedEconInfo>();
			list = deserializer.deserialize<List<UnturnedEconInfo>>(path);
			using (List<UnturnedEconInfo>.Enumerator enumerator = list.GetEnumerator())
			{
				while (enumerator.MoveNext())
				{
					UnturnedEconInfo translatedItem = enumerator.Current;
					UnturnedEconInfo unturnedEconInfo = TempSteamworksEconomy.econInfo.Find((UnturnedEconInfo x) => x.itemdefid == translatedItem.itemdefid);
					if (unturnedEconInfo != null)
					{
						unturnedEconInfo.name = translatedItem.name;
						unturnedEconInfo.type = translatedItem.type;
						unturnedEconInfo.description = translatedItem.description;
					}
				}
			}
		}

		public TempSteamworksEconomy.InventoryRefreshed onInventoryRefreshed;

		public TempSteamworksEconomy.InventoryDropped onInventoryDropped;

		public TempSteamworksEconomy.InventoryExchanged onInventoryExchanged;

		public SteamInventoryResult_t promoResult = SteamInventoryResult_t.Invalid;

		public SteamInventoryResult_t exchangeResult = SteamInventoryResult_t.Invalid;

		public SteamInventoryResult_t dropResult = SteamInventoryResult_t.Invalid;

		public SteamInventoryResult_t wearingResult = SteamInventoryResult_t.Invalid;

		public SteamInventoryResult_t inventoryResult = SteamInventoryResult_t.Invalid;

		public SteamInventoryResult_t commitResult = SteamInventoryResult_t.Invalid;

		public SteamItemDetails_t[] inventoryDetails = new SteamItemDetails_t[0];

		public Dictionary<ulong, DynamicEconDetails> dynamicInventoryDetails = new Dictionary<ulong, DynamicEconDetails>();

		public bool isInventoryAvailable;

		private SteamworksAppInfo appInfo;

		private float lastHeartbeat;

		private Callback<SteamInventoryResultReady_t> inventoryResultReady;

		private Callback<GameOverlayActivated_t> gameOverlayActivated;

		public delegate void InventoryRefreshed();

		public delegate void InventoryDropped(int item, ushort quantity, ulong instance);

		public delegate void InventoryExchanged(int item, ushort quantity, ulong instance);
	}
}

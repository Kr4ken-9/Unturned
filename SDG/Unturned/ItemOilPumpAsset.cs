﻿using System;

namespace SDG.Unturned
{
	public class ItemOilPumpAsset : ItemBarricadeAsset
	{
		public ItemOilPumpAsset(Bundle bundle, Data data, Local localization, ushort id) : base(bundle, data, localization, id)
		{
			this.fuelCapacity = data.readUInt16("Fuel_Capacity");
		}

		public ushort fuelCapacity { get; protected set; }
	}
}

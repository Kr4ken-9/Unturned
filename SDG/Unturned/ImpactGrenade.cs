﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class ImpactGrenade : TriggerGrenadeBase
	{
		protected override void GrenadeTriggered()
		{
			base.GrenadeTriggered();
			if (this.explodable == null)
			{
				Debug.LogWarning("Missing explodable", this);
				return;
			}
			this.explodable.Explode();
		}

		public IExplodableThrowable explodable;
	}
}

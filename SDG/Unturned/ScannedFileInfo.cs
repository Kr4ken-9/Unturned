﻿using System;

namespace SDG.Unturned
{
	public struct ScannedFileInfo
	{
		public ScannedFileInfo(string name, string assetPath, string dataPath, string bundlePath, bool dataUsePath, bool bundleUsePath, bool loadFromResources, bool canUse, AssetDirectory directory, EAssetOrigin origin, bool overrideExistingIDs)
		{
			this.name = name;
			this.assetPath = assetPath;
			this.dataPath = dataPath;
			this.bundlePath = bundlePath;
			this.dataUsePath = dataUsePath;
			this.bundleUsePath = bundleUsePath;
			this.loadFromResources = loadFromResources;
			this.canUse = canUse;
			this.directory = directory;
			this.origin = origin;
			this.overrideExistingIDs = overrideExistingIDs;
		}

		public string name;

		public string assetPath;

		public string dataPath;

		public string bundlePath;

		public bool dataUsePath;

		public bool bundleUsePath;

		public bool loadFromResources;

		public bool canUse;

		public AssetDirectory directory;

		public EAssetOrigin origin;

		public bool overrideExistingIDs;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public struct SkinIconInfo
	{
		public SkinIconInfo(ushort newID, ESkinIconSize newSize)
		{
			this.id = newID;
			this.size = newSize;
		}

		public ushort id;

		public ESkinIconSize size;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class NPCCompareFlagsCondition : NPCLogicCondition
	{
		public NPCCompareFlagsCondition(ushort newFlag_A_ID, ushort newFlag_B_ID, bool newAllowFlag_A_Unset, bool newAllowFlag_B_Unset, ENPCLogicType newLogicType, string newText, bool newShouldReset) : base(newLogicType, newText, newShouldReset)
		{
			this.flag_A_ID = newFlag_A_ID;
			this.allowFlag_A_Unset = newAllowFlag_A_Unset;
			this.flag_B_ID = newFlag_B_ID;
			this.allowFlag_B_Unset = newAllowFlag_B_Unset;
		}

		public ushort flag_A_ID { get; protected set; }

		public bool allowFlag_A_Unset { get; protected set; }

		public bool allowFlag_B_Unset { get; protected set; }

		public override bool isConditionMet(Player player)
		{
			short a;
			short b;
			return (player.quests.getFlag(this.flag_A_ID, out a) || this.allowFlag_A_Unset) && (player.quests.getFlag(this.flag_B_ID, out b) || this.allowFlag_B_Unset) && base.doesLogicPass<short>(a, b);
		}

		public override void applyCondition(Player player, bool shouldSend)
		{
			if (!this.shouldReset)
			{
				return;
			}
			if (shouldSend)
			{
				player.quests.sendRemoveFlag(this.flag_A_ID);
				player.quests.sendRemoveFlag(this.flag_B_ID);
			}
			else
			{
				player.quests.removeFlag(this.flag_A_ID);
				player.quests.removeFlag(this.flag_B_ID);
			}
		}

		public override string formatCondition(Player player)
		{
			if (string.IsNullOrEmpty(this.text))
			{
				return null;
			}
			return this.text;
		}

		public ushort flag_B_ID;
	}
}

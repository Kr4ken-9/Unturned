﻿using System;
using SDG.Framework.Modules;
using UnityEngine;

namespace SDG.Unturned
{
	public class Setup : MonoBehaviour
	{
		private void Awake()
		{
			if (this.awakeDedicator)
			{
				base.GetComponent<Dedicator>().awake();
			}
			if (this.awakeLogs)
			{
				base.GetComponent<Logs>().awake();
			}
			if (this.awakeModuleHook)
			{
				base.GetComponent<ModuleHook>().awake();
			}
			if (this.awakeProvider)
			{
				base.GetComponent<Provider>().awake();
			}
			if (this.startModuleHook)
			{
				base.GetComponent<ModuleHook>().start();
			}
			if (this.startProvider)
			{
				base.GetComponent<Provider>().start();
			}
		}

		private void Start()
		{
			if (!Dedicator.isDedicated)
			{
				MenuSettings.load();
				GraphicsSettings.resize();
				LoadingUI.updateScene();
			}
		}

		public bool awakeDedicator = true;

		public bool awakeLogs = true;

		public bool awakeModuleHook = true;

		public bool awakeProvider = true;

		public bool startModuleHook = true;

		public bool startProvider = true;
	}
}

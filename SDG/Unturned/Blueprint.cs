﻿using System;

namespace SDG.Unturned
{
	public class Blueprint
	{
		public Blueprint(ItemAsset newSourceItem, byte newID, EBlueprintType newType, BlueprintSupply[] newSupplies, BlueprintOutput[] newOutputs, ushort newTool, bool newToolCritical, ushort newBuild, byte newLevel, EBlueprintSkill newSkill, bool newTransferState, string newMap, INPCCondition[] newQuestConditions, INPCReward[] newQuestRewards)
		{
			this.sourceItem = newSourceItem;
			this._id = newID;
			this._type = newType;
			this._supplies = newSupplies;
			this._outputs = newOutputs;
			this._tool = newTool;
			this._toolCritical = newToolCritical;
			this._build = newBuild;
			this._level = newLevel;
			this._skill = newSkill;
			this._transferState = newTransferState;
			this.map = newMap;
			this.questConditions = newQuestConditions;
			this.questRewards = newQuestRewards;
			this.hasSupplies = false;
			this.hasTool = false;
			this.tools = 0;
		}

		public ItemAsset sourceItem { get; protected set; }

		public byte id
		{
			get
			{
				return this._id;
			}
		}

		public EBlueprintType type
		{
			get
			{
				return this._type;
			}
		}

		public BlueprintSupply[] supplies
		{
			get
			{
				return this._supplies;
			}
		}

		public BlueprintOutput[] outputs
		{
			get
			{
				return this._outputs;
			}
		}

		public ushort tool
		{
			get
			{
				return this._tool;
			}
		}

		public bool toolCritical
		{
			get
			{
				return this._toolCritical;
			}
		}

		public ushort build
		{
			get
			{
				return this._build;
			}
		}

		public byte level
		{
			get
			{
				return this._level;
			}
		}

		public EBlueprintSkill skill
		{
			get
			{
				return this._skill;
			}
		}

		public bool transferState
		{
			get
			{
				return this._transferState;
			}
		}

		public string map { get; private set; }

		public INPCCondition[] questConditions { get; protected set; }

		public INPCReward[] questRewards { get; protected set; }

		public bool areConditionsMet(Player player)
		{
			if (this.questConditions != null)
			{
				for (int i = 0; i < this.questConditions.Length; i++)
				{
					if (!this.questConditions[i].isConditionMet(player))
					{
						return false;
					}
				}
			}
			return true;
		}

		public void applyConditions(Player player, bool shouldSend)
		{
			if (this.questConditions != null)
			{
				for (int i = 0; i < this.questConditions.Length; i++)
				{
					this.questConditions[i].applyCondition(player, shouldSend);
				}
			}
		}

		public void grantRewards(Player player, bool shouldSend)
		{
			if (this.questRewards != null)
			{
				for (int i = 0; i < this.questRewards.Length; i++)
				{
					this.questRewards[i].grantReward(player, shouldSend);
				}
			}
		}

		public override string ToString()
		{
			string text = string.Empty;
			text += this.type;
			text += ": ";
			byte b = 0;
			while ((int)b < this.supplies.Length)
			{
				if (b > 0)
				{
					text += " + ";
				}
				text += this.supplies[(int)b].id;
				text += "x";
				text += this.supplies[(int)b].amount;
				b += 1;
			}
			text += " = ";
			byte b2 = 0;
			while ((int)b2 < this.outputs.Length)
			{
				if (b2 > 0)
				{
					text += " + ";
				}
				text += this.outputs[(int)b2].id;
				text += "x";
				text += this.outputs[(int)b2].amount;
				b2 += 1;
			}
			return text;
		}

		private byte _id;

		private EBlueprintType _type;

		private BlueprintSupply[] _supplies;

		private BlueprintOutput[] _outputs;

		private ushort _tool;

		private bool _toolCritical;

		private ushort _build;

		private byte _level;

		private EBlueprintSkill _skill;

		private bool _transferState;

		public bool hasSupplies;

		public bool hasTool;

		public bool hasItem;

		public bool hasSkills;

		public ushort tools;

		public ushort products;

		public ushort items;
	}
}

﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public struct ReceivedChatMessage
	{
		public ReceivedChatMessage(SteamPlayer speaker, string iconURL, EChatMode newMode, Color newColor, bool newRich, string newContents)
		{
			this.speaker = speaker;
			this.iconURL = iconURL;
			this.mode = newMode;
			this.color = newColor;
			this.useRichTextFormatting = newRich;
			this.contents = newContents;
			this.receivedTimestamp = Time.time;
		}

		public float age
		{
			get
			{
				return Time.time - this.receivedTimestamp;
			}
		}

		public void queryIcon(Provider.IconQueryCallback callback)
		{
			if (string.IsNullOrEmpty(this.iconURL))
			{
				Texture2D icon;
				if (OptionsSettings.streamer || this.speaker == null)
				{
					icon = null;
				}
				else
				{
					icon = Provider.provider.communityService.getIcon(this.speaker.playerID.steamID, true);
				}
				callback(icon);
			}
			else
			{
				Provider.refreshIcon(new Provider.IconQueryParams
				{
					url = this.iconURL,
					callback = callback,
					shouldCache = true
				});
			}
		}

		public SteamPlayer speaker;

		public string iconURL;

		public EChatMode mode;

		public Color color;

		public bool useRichTextFormatting;

		public string contents;

		public float receivedTimestamp;
	}
}

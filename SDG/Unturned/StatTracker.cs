﻿using System;
using SDG.Provider;
using UnityEngine;
using UnityEngine.UI;

namespace SDG.Unturned
{
	public class StatTracker : MonoBehaviour
	{
		public Text statTrackerText { get; protected set; }

		public Transform statTrackerHook { get; protected set; }

		public void updateStatTracker(bool viewmodel)
		{
			GameObject gameObject = UnityEngine.Object.Instantiate<GameObject>(Resources.Load<GameObject>("Economy/Attachments/Stat_Tracker"));
			gameObject.transform.SetParent(this.statTrackerHook);
			gameObject.transform.localPosition = Vector3.zero;
			gameObject.transform.localRotation = Quaternion.identity;
			this.statTrackerText = gameObject.GetComponentInChildren<Text>();
			if (viewmodel)
			{
				Layerer.relayer(gameObject.transform, LayerMasks.VIEWMODEL);
			}
		}

		protected void Update()
		{
			if (this.statTrackerCallback == null)
			{
				return;
			}
			EStatTrackerType type;
			int num;
			if (!this.statTrackerCallback(out type, out num))
			{
				return;
			}
			if (this.oldStatValue == num)
			{
				return;
			}
			this.oldStatValue = num;
			this.statTrackerText.color = Provider.provider.economyService.getStatTrackerColor(type);
			this.statTrackerText.text = num.ToString("D7");
		}

		protected void Awake()
		{
			this.statTrackerHook = base.transform.FindChild("Stat_Tracker");
		}

		public GetStatTrackerValueHandler statTrackerCallback;

		protected int oldStatValue = -1;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class StructuresConfigData
	{
		public StructuresConfigData(EGameMode mode)
		{
			this.Decay_Time = 604800u;
			this.Armor_Multiplier = 1f;
			this.Gun_Lowcal_Damage_Multiplier = 1f;
			this.Gun_Highcal_Damage_Multiplier = 1f;
			this.Melee_Damage_Multiplier = 1f;
		}

		public uint Decay_Time;

		public float Armor_Multiplier;

		public float Gun_Lowcal_Damage_Multiplier;

		public float Gun_Highcal_Damage_Multiplier;

		public float Melee_Damage_Multiplier;
	}
}

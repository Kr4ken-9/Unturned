﻿using System;

namespace SDG.Unturned
{
	public abstract class VendorSellingBase : VendorElement
	{
		public VendorSellingBase(byte newIndex, ushort newID, uint newCost, INPCCondition[] newConditions) : base(newIndex, newID, newCost, newConditions)
		{
		}

		public bool canBuy(Player player)
		{
			return player.skills.experience >= base.cost;
		}

		public virtual void buy(Player player)
		{
			player.skills.askSpend(base.cost);
		}

		public virtual void format(Player player, out ushort total)
		{
			total = 0;
		}
	}
}

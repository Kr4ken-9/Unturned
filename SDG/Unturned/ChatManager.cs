﻿using System;
using System.Collections.Generic;
using SDG.Framework.Debug;
using Steamworks;
using UnityEngine;

namespace SDG.Unturned
{
	public class ChatManager : SteamCaller
	{
		public static ChatManager instance
		{
			get
			{
				return ChatManager.manager;
			}
		}

		public static List<ReceivedChatMessage> receivedChatHistory
		{
			get
			{
				return ChatManager._receivedChatHistory;
			}
		}

		public static void receiveChatMessage(CSteamID speakerSteamID, string iconURL, EChatMode mode, Color color, bool isRich, string text)
		{
			text = text.Trim();
			if (OptionsSettings.filter)
			{
				ProfanityFilter.filterOutCurseWords(ref text, '#');
			}
			if (OptionsSettings.streamer)
			{
				color = Color.white;
			}
			SteamPlayer speaker;
			if (speakerSteamID == CSteamID.Nil)
			{
				speaker = null;
			}
			else
			{
				if (!OptionsSettings.chatText && speakerSteamID != Provider.client)
				{
					return;
				}
				speaker = PlayerTool.getSteamPlayer(speakerSteamID);
			}
			ReceivedChatMessage item = new ReceivedChatMessage(speaker, iconURL, mode, color, isRich, text);
			ChatManager.receivedChatHistory.Insert(0, item);
			if (ChatManager.receivedChatHistory.Count > Provider.preferenceData.Chat.History_Length)
			{
				ChatManager.receivedChatHistory.RemoveAt(ChatManager.receivedChatHistory.Count - 1);
			}
			if (ChatManager.onChatMessageReceived != null)
			{
				ChatManager.onChatMessageReceived();
			}
		}

		public static bool process(SteamPlayer player, string cmd)
		{
			bool flag = false;
			bool result = true;
			string a = cmd.Substring(0, 1);
			if ((a == "@" || a == "/") && (!Dedicator.isDedicated || player.isAdmin))
			{
				flag = true;
				result = false;
			}
			if (ChatManager.onCheckPermissions != null)
			{
				ChatManager.onCheckPermissions(player, cmd, ref flag, ref result);
			}
			if (flag)
			{
				Commander.execute(player.playerID.steamID, cmd.Substring(1));
			}
			return result;
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellVoteStart(CSteamID steamID, CSteamID origin, CSteamID target, byte votesNeeded)
		{
			if (base.channel.checkServer(steamID))
			{
				SteamPlayer steamPlayer = PlayerTool.getSteamPlayer(origin);
				if (steamPlayer == null)
				{
					return;
				}
				SteamPlayer steamPlayer2 = PlayerTool.getSteamPlayer(target);
				if (steamPlayer2 == null)
				{
					return;
				}
				ChatManager.needsVote = true;
				ChatManager.hasVote = false;
				if (ChatManager.onVotingStart != null)
				{
					ChatManager.onVotingStart(steamPlayer, steamPlayer2, votesNeeded);
				}
			}
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellVoteUpdate(CSteamID steamID, byte voteYes, byte voteNo)
		{
			if (base.channel.checkServer(steamID) && ChatManager.onVotingUpdate != null)
			{
				ChatManager.onVotingUpdate(voteYes, voteNo);
			}
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellVoteStop(CSteamID steamID, byte message)
		{
			if (base.channel.checkServer(steamID))
			{
				ChatManager.needsVote = false;
				if (ChatManager.onVotingStop != null)
				{
					ChatManager.onVotingStop((EVotingMessage)message);
				}
			}
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellVoteMessage(CSteamID steamID, byte message)
		{
			if (base.channel.checkServer(steamID) && ChatManager.onVotingMessage != null)
			{
				ChatManager.onVotingMessage((EVotingMessage)message);
			}
		}

		[SteamCall(ESteamCallValidation.SERVERSIDE)]
		public void askVote(CSteamID steamID, bool vote)
		{
			if (Provider.isServer)
			{
				SteamPlayer steamPlayer = PlayerTool.getSteamPlayer(steamID);
				if (steamPlayer == null)
				{
					return;
				}
				if (!ChatManager.isVoting)
				{
					return;
				}
				if (!steamPlayer.player.tryToPerformRateLimitedAction())
				{
					return;
				}
				if (ChatManager.votes.Contains(steamID))
				{
					return;
				}
				ChatManager.votes.Add(steamID);
				if (vote)
				{
					ChatManager.voteYes += 1;
				}
				else
				{
					ChatManager.voteNo += 1;
				}
				ChatManager.manager.channel.send("tellVoteUpdate", ESteamCall.CLIENTS, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
				{
					ChatManager.voteYes,
					ChatManager.voteNo
				});
			}
		}

		[SteamCall(ESteamCallValidation.SERVERSIDE)]
		public void askCallVote(CSteamID steamID, CSteamID target)
		{
			if (Provider.isServer)
			{
				if (ChatManager.isVoting)
				{
					return;
				}
				SteamPlayer steamPlayer = PlayerTool.getSteamPlayer(steamID);
				if (steamPlayer == null || Time.realtimeSinceStartup < steamPlayer.nextVote)
				{
					ChatManager.manager.channel.send("tellVoteMessage", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
					{
						1
					});
					return;
				}
				if (!steamPlayer.player.tryToPerformRateLimitedAction())
				{
					return;
				}
				if (!ChatManager.voteAllowed)
				{
					ChatManager.manager.channel.send("tellVoteMessage", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
					{
						0
					});
					return;
				}
				SteamPlayer steamPlayer2 = PlayerTool.getSteamPlayer(target);
				if (steamPlayer2 == null || steamPlayer2.isAdmin)
				{
					return;
				}
				if (Provider.clients.Count < (int)ChatManager.votePlayers)
				{
					ChatManager.manager.channel.send("tellVoteMessage", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
					{
						2
					});
					return;
				}
				CommandWindow.Log(Provider.localization.format("Vote_Kick", new object[]
				{
					steamPlayer.playerID.characterName,
					steamPlayer.playerID.playerName,
					steamPlayer2.playerID.characterName,
					steamPlayer2.playerID.playerName
				}));
				ChatManager.lastVote = Time.realtimeSinceStartup;
				ChatManager.isVoting = true;
				ChatManager.voteYes = 0;
				ChatManager.voteNo = 0;
				ChatManager.votesPossible = (byte)Provider.clients.Count;
				ChatManager.votesNeeded = (byte)Mathf.Ceil((float)ChatManager.votesPossible * ChatManager.votePercentage);
				ChatManager.voteOrigin = steamPlayer;
				ChatManager.voteTarget = target;
				ChatManager.votes = new List<CSteamID>();
				P2PSessionState_t p2PSessionState_t;
				if (SteamGameServerNetworking.GetP2PSessionState(ChatManager.voteTarget, out p2PSessionState_t))
				{
					ChatManager.voteIP = p2PSessionState_t.m_nRemoteIP;
				}
				else
				{
					ChatManager.voteIP = 0u;
				}
				ChatManager.manager.channel.send("tellVoteStart", ESteamCall.CLIENTS, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
				{
					steamID,
					target,
					ChatManager.votesNeeded
				});
			}
		}

		public static void sendVote(bool vote)
		{
			ChatManager.manager.channel.send("askVote", ESteamCall.SERVER, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				vote
			});
		}

		public static void sendCallVote(CSteamID target)
		{
			ChatManager.manager.channel.send("askCallVote", ESteamCall.SERVER, ESteamPacket.UPDATE_UNRELIABLE_BUFFER, new object[]
			{
				target
			});
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellChat(CSteamID steamID, CSteamID owner, string iconURL, byte mode, Color color, bool rich, string text)
		{
			if (base.channel.checkServer(steamID))
			{
				ChatManager.receiveChatMessage(owner, iconURL, (EChatMode)mode, color, rich, text);
			}
		}

		[SteamCall(ESteamCallValidation.SERVERSIDE)]
		public void askChat(CSteamID steamID, byte mode, string text)
		{
			if (Provider.isServer)
			{
				SteamPlayer steamPlayer = PlayerTool.getSteamPlayer(steamID);
				if (steamPlayer == null || steamPlayer.player == null)
				{
					return;
				}
				if (Time.realtimeSinceStartup - steamPlayer.lastChat < ChatManager.chatrate)
				{
					return;
				}
				steamPlayer.lastChat = Time.realtimeSinceStartup;
				if (!steamPlayer.player.tryToPerformRateLimitedAction())
				{
					return;
				}
				if (text.Length < 2)
				{
					return;
				}
				text = text.Trim();
				if (text.Length > ChatManager.MAX_MESSAGE_LENGTH)
				{
					text = text.Substring(0, ChatManager.MAX_MESSAGE_LENGTH);
				}
				if (mode == 0)
				{
					if (CommandWindow.shouldLogChat)
					{
						CommandWindow.Log(Provider.localization.format("Global", new object[]
						{
							steamPlayer.playerID.characterName,
							steamPlayer.playerID.playerName,
							text
						}));
					}
				}
				else if (mode == 1)
				{
					if (CommandWindow.shouldLogChat)
					{
						CommandWindow.Log(Provider.localization.format("Local", new object[]
						{
							steamPlayer.playerID.characterName,
							steamPlayer.playerID.playerName,
							text
						}));
					}
				}
				else
				{
					if (mode != 2)
					{
						return;
					}
					if (CommandWindow.shouldLogChat)
					{
						CommandWindow.Log(Provider.localization.format("Group", new object[]
						{
							steamPlayer.playerID.characterName,
							steamPlayer.playerID.playerName,
							text
						}));
					}
				}
				Color color = Color.white;
				if (steamPlayer.isAdmin && !Provider.hideAdmins)
				{
					color = Palette.ADMIN;
				}
				else if (steamPlayer.isPro)
				{
					color = Palette.PRO;
				}
				bool flag = false;
				bool flag2 = true;
				if (ChatManager.onChatted != null)
				{
					ChatManager.onChatted(steamPlayer, (EChatMode)mode, ref color, ref flag, text, ref flag2);
				}
				if (ChatManager.process(steamPlayer, text) && flag2)
				{
					if (ChatManager.onServerFormattingMessage != null)
					{
						ChatManager.onServerFormattingMessage(steamPlayer, (EChatMode)mode, ref text);
					}
					else
					{
						text = "%SPEAKER%: " + text;
						if (mode != 1)
						{
							if (mode == 2)
							{
								text = "[G] " + text;
							}
						}
						else
						{
							text = "[A] " + text;
						}
					}
					if (mode == 0)
					{
						ChatManager.serverSendMessage(text, color, steamPlayer, null, EChatMode.GLOBAL, null, flag);
					}
					else if (mode == 1)
					{
						float num = 16384f;
						foreach (SteamPlayer steamPlayer2 in Provider.clients)
						{
							if (!(steamPlayer2.player == null))
							{
								if ((steamPlayer2.player.transform.position - steamPlayer.player.transform.position).sqrMagnitude < num)
								{
									string text2 = text;
									Color color2 = color;
									SteamPlayer steamPlayer3 = steamPlayer;
									SteamPlayer steamPlayer4 = steamPlayer2;
									EChatMode mode2 = EChatMode.LOCAL;
									bool useRichTextFormatting = flag;
									ChatManager.serverSendMessage(text2, color2, steamPlayer3, steamPlayer4, mode2, null, useRichTextFormatting);
								}
							}
						}
					}
					else if (mode == 2 && steamPlayer.player.quests.groupID != CSteamID.Nil)
					{
						foreach (SteamPlayer steamPlayer5 in Provider.clients)
						{
							if (!(steamPlayer5.player == null))
							{
								if (steamPlayer5.player.quests.isMemberOfSameGroupAs(steamPlayer.player))
								{
									string text2 = text;
									Color color2 = color;
									SteamPlayer steamPlayer4 = steamPlayer;
									SteamPlayer steamPlayer3 = steamPlayer5;
									EChatMode mode2 = EChatMode.GROUP;
									bool useRichTextFormatting = flag;
									ChatManager.serverSendMessage(text2, color2, steamPlayer4, steamPlayer3, mode2, null, useRichTextFormatting);
								}
							}
						}
					}
				}
			}
		}

		[TerminalCommandMethod("chat.send", "broadcast message in chat")]
		public static void terminalChat([TerminalCommandParameter("message", "text to send")] string message)
		{
			ChatManager.sendChat(EChatMode.GLOBAL, message);
		}

		public static void sendChat(EChatMode mode, string text)
		{
			ChatManager.manager.channel.send("askChat", ESteamCall.SERVER, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				(byte)mode,
				text
			});
		}

		public static void say(CSteamID target, string text, Color color, bool isRich = false)
		{
			ChatManager.say(target, text, color, EChatMode.WELCOME, isRich);
		}

		public static void say(CSteamID target, string text, Color color, EChatMode mode, bool isRich = false)
		{
			SteamPlayer steamPlayer = PlayerTool.getSteamPlayer(target);
			if (steamPlayer == null)
			{
				return;
			}
			SteamPlayer toPlayer = steamPlayer;
			ChatManager.serverSendMessage(text, color, null, toPlayer, EChatMode.SAY, null, isRich);
		}

		public static void say(string text, Color color, bool isRich = false)
		{
			ChatManager.serverSendMessage(text, color, null, null, EChatMode.SAY, null, isRich);
		}

		public static void serverSendMessage(string text, Color color, SteamPlayer fromPlayer = null, SteamPlayer toPlayer = null, EChatMode mode = EChatMode.SAY, string iconURL = null, bool useRichTextFormatting = false)
		{
			if (!Provider.isServer)
			{
				throw new Exception("Tried to send server message, but currently a client! Text: " + text);
			}
			if (ChatManager.onServerSendingMessage != null)
			{
				ChatManager.onServerSendingMessage(ref text, ref color, fromPlayer, toPlayer, mode, ref iconURL, ref useRichTextFormatting);
			}
			else if (fromPlayer != null && toPlayer != null)
			{
				string newValue;
				if (!string.IsNullOrEmpty(fromPlayer.playerID.nickName) && fromPlayer != toPlayer && toPlayer.player != null && fromPlayer.player != null && fromPlayer.player.quests.isMemberOfSameGroupAs(toPlayer.player))
				{
					newValue = fromPlayer.playerID.nickName;
				}
				else
				{
					newValue = fromPlayer.playerID.characterName;
				}
				text = text.Replace("%SPEAKER%", newValue);
			}
			if (text.Length > 255)
			{
				throw new ArgumentException("Longer than the maximum send length: " + text, "text");
			}
			if (iconURL == null)
			{
				iconURL = string.Empty;
			}
			else if (iconURL.Length > 255)
			{
				throw new ArgumentException("Longer than the maximum send length: " + iconURL, "iconURL");
			}
			CSteamID csteamID = (fromPlayer != null) ? fromPlayer.playerID.steamID : CSteamID.Nil;
			if (toPlayer == null)
			{
				foreach (SteamPlayer steamPlayer in Provider.clients)
				{
					if (steamPlayer != null)
					{
						ChatManager.serverSendMessage(text, color, fromPlayer, steamPlayer, mode, iconURL, useRichTextFormatting);
					}
				}
			}
			else
			{
				CSteamID steamID = toPlayer.playerID.steamID;
				ChatManager.manager.channel.send("tellChat", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
				{
					csteamID,
					iconURL,
					(byte)mode,
					color,
					useRichTextFormatting,
					text
				});
			}
		}

		private void onLevelLoaded(int level)
		{
			if (level > Level.BUILD_INDEX_SETUP)
			{
				ChatManager.receivedChatHistory.Clear();
			}
		}

		private void onServerConnected(CSteamID steamID)
		{
			if (Provider.isServer && ChatManager.welcomeText != string.Empty)
			{
				SteamPlayer steamPlayer = PlayerTool.getSteamPlayer(steamID);
				ChatManager.say(steamPlayer.playerID.steamID, string.Format(ChatManager.welcomeText, steamPlayer.playerID.characterName), ChatManager.welcomeColor, false);
			}
		}

		private void Update()
		{
			if (ChatManager.isVoting && (Time.realtimeSinceStartup - ChatManager.lastVote > ChatManager.voteDuration || ChatManager.voteYes >= ChatManager.votesNeeded || ChatManager.voteNo > ChatManager.votesPossible - ChatManager.votesNeeded))
			{
				ChatManager.isVoting = false;
				if (ChatManager.voteYes >= ChatManager.votesNeeded)
				{
					if (ChatManager.voteOrigin != null)
					{
						ChatManager.voteOrigin.nextVote = Time.realtimeSinceStartup + ChatManager.votePassCooldown;
					}
					CommandWindow.Log(Provider.localization.format("Vote_Pass"));
					ChatManager.manager.channel.send("tellVoteStop", ESteamCall.CLIENTS, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
					{
						3
					});
					SteamBlacklist.ban(ChatManager.voteTarget, ChatManager.voteIP, CSteamID.Nil, "you were vote kicked", SteamBlacklist.TEMPORARY);
				}
				else
				{
					if (ChatManager.voteOrigin != null)
					{
						ChatManager.voteOrigin.nextVote = Time.realtimeSinceStartup + ChatManager.voteFailCooldown;
					}
					CommandWindow.Log(Provider.localization.format("Vote_Fail"));
					ChatManager.manager.channel.send("tellVoteStop", ESteamCall.CLIENTS, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
					{
						4
					});
				}
			}
			if (ChatManager.needsVote && !ChatManager.hasVote)
			{
				if (Input.GetKeyDown(KeyCode.F1))
				{
					ChatManager.needsVote = false;
					ChatManager.hasVote = true;
					ChatManager.sendVote(true);
				}
				else if (Input.GetKeyDown(KeyCode.F2))
				{
					ChatManager.needsVote = false;
					ChatManager.hasVote = true;
					ChatManager.sendVote(false);
				}
			}
		}

		private void Start()
		{
			ChatManager.manager = this;
			Level.onLevelLoaded = (LevelLoaded)Delegate.Combine(Level.onLevelLoaded, new LevelLoaded(this.onLevelLoaded));
			Provider.onServerConnected = (Provider.ServerConnected)Delegate.Combine(Provider.onServerConnected, new Provider.ServerConnected(this.onServerConnected));
		}

		public static readonly int MAX_MESSAGE_LENGTH = 127;

		public static ChatMessageReceivedHandler onChatMessageReceived;

		public static ServerSendingChatMessageHandler onServerSendingMessage;

		public static ServerFormattingChatMessageHandler onServerFormattingMessage;

		public static Chatted onChatted;

		public static CheckPermissions onCheckPermissions;

		public static VotingStart onVotingStart;

		public static VotingUpdate onVotingUpdate;

		public static VotingStop onVotingStop;

		public static VotingMessage onVotingMessage;

		public static string welcomeText = string.Empty;

		public static Color welcomeColor = Palette.SERVER;

		public static float chatrate = 0.25f;

		public static bool voteAllowed = false;

		public static float votePassCooldown = 5f;

		public static float voteFailCooldown = 60f;

		public static float voteDuration = 15f;

		public static float votePercentage = 0.75f;

		public static byte votePlayers = 3;

		private static float lastVote;

		private static bool isVoting;

		private static bool needsVote;

		private static bool hasVote;

		private static byte voteYes;

		private static byte voteNo;

		private static byte votesPossible;

		private static byte votesNeeded;

		private static SteamPlayer voteOrigin;

		private static CSteamID voteTarget;

		private static uint voteIP;

		private static List<CSteamID> votes;

		private static ChatManager manager;

		private static List<ReceivedChatMessage> _receivedChatHistory = new List<ReceivedChatMessage>();
	}
}

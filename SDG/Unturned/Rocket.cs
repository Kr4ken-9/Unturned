﻿using System;
using System.Collections.Generic;
using Steamworks;
using UnityEngine;

namespace SDG.Unturned
{
	public class Rocket : MonoBehaviour
	{
		private void OnTriggerEnter(Collider other)
		{
			if (this.isExploded)
			{
				return;
			}
			if (other.isTrigger)
			{
				return;
			}
			if (other.transform == this.ignoreTransform)
			{
				return;
			}
			this.isExploded = true;
			if (Provider.isServer)
			{
				Vector3 point = this.lastPos;
				float damageRadius = this.range;
				EDeathCause cause = EDeathCause.MISSILE;
				CSteamID csteamID = this.killer;
				float num = this.playerDamage;
				float num2 = this.zombieDamage;
				float num3 = this.animalDamage;
				float num4 = this.barricadeDamage;
				float num5 = this.structureDamage;
				float num6 = this.vehicleDamage;
				float num7 = this.resourceDamage;
				float num8 = this.objectDamage;
				List<EPlayerKill> list;
				ref List<EPlayerKill> kills = ref list;
				bool flag = this.penetrateBuildables;
				DamageTool.explode(point, damageRadius, cause, csteamID, num, num2, num3, num4, num5, num6, num7, num8, out kills, EExplosionDamageType.CONVENTIONAL, 32f, true, flag, EDamageOrigin.Rocket_Explosion);
				EffectManager.sendEffect(this.explosion, EffectManager.LARGE, this.lastPos);
			}
			UnityEngine.Object.Destroy(base.gameObject);
		}

		private void FixedUpdate()
		{
			this.lastPos = base.transform.position;
		}

		private void Awake()
		{
			this.lastPos = base.transform.position;
		}

		public CSteamID killer;

		public float range;

		public float playerDamage;

		public float zombieDamage;

		public float animalDamage;

		public float barricadeDamage;

		public float structureDamage;

		public float vehicleDamage;

		public float resourceDamage;

		public float objectDamage;

		public ushort explosion;

		public bool penetrateBuildables;

		public Transform ignoreTransform;

		private bool isExploded;

		private Vector3 lastPos;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class ItemTrapAsset : ItemBarricadeAsset
	{
		public ItemTrapAsset(Bundle bundle, Data data, Local localization, ushort id) : base(bundle, data, localization, id)
		{
			this._range2 = data.readSingle("Range2");
			this.playerDamage = data.readSingle("Player_Damage");
			this.zombieDamage = data.readSingle("Zombie_Damage");
			this.animalDamage = data.readSingle("Animal_Damage");
			this.barricadeDamage = data.readSingle("Barricade_Damage");
			this.structureDamage = data.readSingle("Structure_Damage");
			this.vehicleDamage = data.readSingle("Vehicle_Damage");
			this.resourceDamage = data.readSingle("Resource_Damage");
			if (data.has("Object_Damage"))
			{
				this.objectDamage = data.readSingle("Object_Damage");
			}
			else
			{
				this.objectDamage = this.resourceDamage;
			}
			this._explosion2 = data.readUInt16("Explosion2");
			this._isBroken = data.has("Broken");
			this._isExplosive = data.has("Explosive");
			this.damageTires = data.has("Damage_Tires");
		}

		public float range2
		{
			get
			{
				return this._range2;
			}
		}

		public ushort explosion2
		{
			get
			{
				return this._explosion2;
			}
		}

		public bool isBroken
		{
			get
			{
				return this._isBroken;
			}
		}

		public bool isExplosive
		{
			get
			{
				return this._isExplosive;
			}
		}

		protected float _range2;

		public float playerDamage;

		public float zombieDamage;

		public float animalDamage;

		public float barricadeDamage;

		public float structureDamage;

		public float vehicleDamage;

		public float resourceDamage;

		public float objectDamage;

		private ushort _explosion2;

		protected bool _isBroken;

		protected bool _isExplosive;

		public bool damageTires;
	}
}

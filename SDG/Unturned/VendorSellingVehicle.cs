﻿using System;
using SDG.Framework.Devkit;
using UnityEngine;

namespace SDG.Unturned
{
	public class VendorSellingVehicle : VendorSellingBase
	{
		public VendorSellingVehicle(byte newIndex, ushort newID, uint newCost, string newSpawnpoint, INPCCondition[] newConditions) : base(newIndex, newID, newCost, newConditions)
		{
			this.spawnpoint = newSpawnpoint;
		}

		public override string displayName
		{
			get
			{
				VehicleAsset vehicleAsset = Assets.find(EAssetType.VEHICLE, base.id) as VehicleAsset;
				return (vehicleAsset == null) ? null : vehicleAsset.vehicleName;
			}
		}

		public override EItemRarity rarity
		{
			get
			{
				VehicleAsset vehicleAsset = Assets.find(EAssetType.VEHICLE, base.id) as VehicleAsset;
				return (vehicleAsset == null) ? EItemRarity.COMMON : vehicleAsset.rarity;
			}
		}

		public override bool hasIcon
		{
			get
			{
				return false;
			}
		}

		public string spawnpoint { get; protected set; }

		public override void buy(Player player)
		{
			base.buy(player);
			Spawnpoint spawnpoint = SpawnpointSystem.getSpawnpoint(this.spawnpoint);
			if (spawnpoint == null)
			{
				Debug.LogError("Failed to find vendor selling spawnpoint: " + this.spawnpoint);
				return;
			}
			VehicleManager.spawnLockedVehicleForPlayer(base.id, spawnpoint.transform.position, spawnpoint.transform.rotation, player);
		}
	}
}

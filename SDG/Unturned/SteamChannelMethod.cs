﻿using System;
using System.Reflection;
using UnityEngine;

namespace SDG.Unturned
{
	public class SteamChannelMethod
	{
		public SteamChannelMethod(Component newComponent, MethodInfo newMethod, Type[] newTypes, SteamCall attribute)
		{
			this.component = newComponent;
			this.method = newMethod;
			this.types = newTypes;
			this.attribute = attribute;
		}

		public Component component { get; protected set; }

		public MethodInfo method { get; protected set; }

		public Type[] types { get; protected set; }

		public SteamCall attribute { get; protected set; }
	}
}

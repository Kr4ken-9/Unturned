﻿using System;
using System.Collections.Generic;
using System.Globalization;

namespace SDG.Unturned
{
	public class LevelInfoConfigData
	{
		public LevelInfoConfigData()
		{
			this.Creators = new string[0];
			this.Collaborators = new string[0];
			this.Thanks = new string[0];
			this.Item = 0;
			this.Feedback = null;
			this.Status = EMapStatus.NONE;
			this.Load_From_Resources = false;
			this.Asset = AssetReference<LevelAsset>.invalid;
			this.Trains = new List<LevelTrainAssociation>();
			this.Mode_Config_Overrides = new Dictionary<string, object>();
			this.Allow_Underwater_Features = false;
			this.Terrain_Snow_Sparkle = false;
			this.Use_Legacy_Clip_Borders = true;
			this.Use_Legacy_Ground = true;
			this.Use_Legacy_Water = true;
			this.Use_Legacy_Objects = true;
			this.Use_Legacy_Snow_Height = true;
			this.Use_Legacy_Fog_Height = true;
			this.Use_Legacy_Oxygen_Height = true;
			this.Use_Rain_Volumes = false;
			this.Use_Snow_Volumes = false;
			this.Is_Aurora_Borealis_Visible = false;
			this.Snow_Affects_Temperature = true;
			this.Has_Atmosphere = true;
			this.Can_Use_Bundles = true;
			this.Gravity = -9.81f;
			this.Blimp_Altitude = 150f;
			this.Max_Walkable_Slope = -1f;
			this.Category = ESingleplayerMapCategory.MISC;
			this.Visible_In_Matchmaking = false;
			this.Use_Arena_Compactor = true;
			this.Arena_Loadouts = new List<ArenaLoadout>();
		}

		public DateTime getCuratedMapTimestamp()
		{
			DateTime result;
			if (DateTime.TryParseExact(this.CuratedMapTimestamp, "yyyyMMdd'T'HHmmss'Z'", CultureInfo.InvariantCulture, DateTimeStyles.AssumeUniversal, out result))
			{
				return result;
			}
			return DateTime.MinValue;
		}

		public bool isNowBeforeCuratedMapTimestamp
		{
			get
			{
				DateTime curatedMapTimestamp = this.getCuratedMapTimestamp();
				return DateTime.UtcNow < curatedMapTimestamp;
			}
		}

		public bool isAvailableToPlay
		{
			get
			{
				return this.CuratedMapMode != ECuratedMapMode.TIMED || this.isNowBeforeCuratedMapTimestamp;
			}
		}

		public string[] Creators;

		public string[] Collaborators;

		public string[] Thanks;

		public int Item;

		public string Feedback;

		public EMapStatus Status;

		public bool Load_From_Resources;

		public AssetReference<LevelAsset> Asset;

		public List<LevelTrainAssociation> Trains;

		public Dictionary<string, object> Mode_Config_Overrides;

		public bool Allow_Underwater_Features;

		public bool Terrain_Snow_Sparkle;

		public bool Use_Legacy_Clip_Borders;

		public bool Use_Legacy_Ground;

		public bool Use_Legacy_Water;

		public bool Use_Legacy_Objects;

		public bool Use_Legacy_Snow_Height;

		public bool Use_Legacy_Fog_Height;

		public bool Use_Legacy_Oxygen_Height;

		public bool Use_Rain_Volumes;

		public bool Use_Snow_Volumes;

		public bool Is_Aurora_Borealis_Visible;

		public bool Snow_Affects_Temperature;

		public bool Has_Atmosphere;

		public bool Can_Use_Bundles;

		public float Gravity;

		public float Blimp_Altitude;

		public float Max_Walkable_Slope;

		public ESingleplayerMapCategory Category;

		public bool Visible_In_Matchmaking;

		public ECuratedMapMode CuratedMapMode;

		public string CuratedMapTimestamp;

		public bool Use_Arena_Compactor;

		public List<ArenaLoadout> Arena_Loadouts;
	}
}

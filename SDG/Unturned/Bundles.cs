﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class Bundles : MonoBehaviour
	{
		public static bool isInitialized
		{
			get
			{
				return Bundles._isInitialized;
			}
		}

		public static Bundle getBundle(string path)
		{
			return Bundles.getBundle(path, true);
		}

		public static Bundle getBundle(string path, bool usePath)
		{
			return Bundles.getBundle(path, usePath, false);
		}

		public static Bundle getBundle(string path, bool usePath, bool loadFromResources)
		{
			return new Bundle(path, usePath, loadFromResources, null);
		}

		private void Awake()
		{
			if (Bundles.isInitialized)
			{
				UnityEngine.Object.Destroy(base.gameObject);
				return;
			}
			Bundles._isInitialized = true;
			UnityEngine.Object.DontDestroyOnLoad(base.gameObject);
		}

		private static bool _isInitialized;
	}
}

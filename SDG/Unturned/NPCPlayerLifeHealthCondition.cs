﻿using System;

namespace SDG.Unturned
{
	public class NPCPlayerLifeHealthCondition : NPCLogicCondition
	{
		public NPCPlayerLifeHealthCondition(int newHealth, ENPCLogicType newLogicType, string newText) : base(newLogicType, newText, false)
		{
			this.health = newHealth;
		}

		public int health { get; protected set; }

		public override bool isConditionMet(Player player)
		{
			return base.doesLogicPass<int>((int)player.life.health, this.health);
		}

		public override string formatCondition(Player player)
		{
			if (string.IsNullOrEmpty(this.text))
			{
				return null;
			}
			return string.Format(this.text, player.life.health, this.health);
		}
	}
}

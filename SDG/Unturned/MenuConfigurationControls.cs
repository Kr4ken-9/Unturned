﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class MenuConfigurationControls : MonoBehaviour
	{
		public static byte binding
		{
			get
			{
				return MenuConfigurationControls._binding;
			}
			set
			{
				MenuConfigurationControls._binding = value;
				SleekRender.allowInput = (MenuConfigurationControls.binding == byte.MaxValue);
			}
		}

		private static void cancel()
		{
			MenuConfigurationControlsUI.cancel();
			MenuConfigurationControls.binding = byte.MaxValue;
		}

		private static void bind(KeyCode key)
		{
			MenuConfigurationControlsUI.bind(key);
			MenuConfigurationControls.binding = byte.MaxValue;
		}

		private void Update()
		{
			if (MenuConfigurationControls.binding != 255)
			{
				if (Event.current.type == EventType.KeyDown)
				{
					if (Event.current.keyCode == KeyCode.Backspace || Event.current.keyCode == KeyCode.Escape)
					{
						MenuConfigurationControls.cancel();
					}
					else
					{
						MenuConfigurationControls.bind(Event.current.keyCode);
					}
				}
				else if (Event.current.type == EventType.MouseDown)
				{
					if (Event.current.button == 0)
					{
						MenuConfigurationControls.bind(KeyCode.Mouse0);
					}
					else if (Event.current.button == 1)
					{
						MenuConfigurationControls.bind(KeyCode.Mouse1);
					}
					else if (Event.current.button == 2)
					{
						MenuConfigurationControls.bind(KeyCode.Mouse2);
					}
					else if (Event.current.button == 3)
					{
						MenuConfigurationControls.bind(KeyCode.Mouse3);
					}
					else if (Event.current.button == 4)
					{
						MenuConfigurationControls.bind(KeyCode.Mouse4);
					}
					else if (Event.current.button == 5)
					{
						MenuConfigurationControls.bind(KeyCode.Mouse5);
					}
					else if (Event.current.button == 6)
					{
						MenuConfigurationControls.bind(KeyCode.Mouse6);
					}
				}
				else if (Event.current.shift)
				{
					MenuConfigurationControls.bind(KeyCode.LeftShift);
				}
			}
		}

		private void Awake()
		{
			MenuConfigurationControls.binding = byte.MaxValue;
		}

		private static byte _binding;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class GameStatusData
	{
		public byte Major_Version;

		public byte Minor_Version;

		public byte Patch_Version;

		public string Server_List_Blacklist_URL;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class PreferenceData
	{
		public PreferenceData()
		{
			this.Graphics = new GraphicsPreferenceData();
			this.Viewmodel = new ViewmodelPreferenceData();
			this.Chat = new ChatPreferenceData();
		}

		public GraphicsPreferenceData Graphics;

		public ViewmodelPreferenceData Viewmodel;

		public ChatPreferenceData Chat;
	}
}

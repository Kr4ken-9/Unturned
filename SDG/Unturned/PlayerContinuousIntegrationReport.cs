﻿using System;

namespace SDG.Unturned
{
	public class PlayerContinuousIntegrationReport
	{
		public PlayerContinuousIntegrationReport()
		{
			this.ExitCode = 0;
			this.ErrorMessage = null;
		}

		public PlayerContinuousIntegrationReport(string ErrorMessage)
		{
			this.ExitCode = 1;
			this.ErrorMessage = ErrorMessage;
		}

		public int ExitCode;

		public string ErrorMessage;
	}
}

﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SDG.Unturned
{
	public class ZombieRegion
	{
		public ZombieRegion(byte newNav)
		{
			this._zombies = new List<Zombie>();
			this.nav = newNav;
			this.updates = 0;
			this.respawnZombieIndex = 0;
			this.alive = 0;
			this.isNetworked = false;
			this.lastMega = -1000f;
			this.hasMega = false;
		}

		public List<Zombie> zombies
		{
			get
			{
				return this._zombies;
			}
		}

		public byte nav { get; protected set; }

		public bool hasBeacon
		{
			get
			{
				return this._hasBeacon;
			}
			set
			{
				if (value != this._hasBeacon)
				{
					this._hasBeacon = value;
					if (this.onHyperUpdated != null)
					{
						this.onHyperUpdated(this.isHyper);
					}
				}
			}
		}

		public bool isHyper
		{
			get
			{
				return LightingManager.isFullMoon || this.hasBeacon;
			}
		}

		public void UpdateRegion()
		{
			if (this.bossZombie == null)
			{
				return;
			}
			bool flag = false;
			bool flag2 = false;
			for (int i = 0; i < Provider.clients.Count; i++)
			{
				SteamPlayer steamPlayer = Provider.clients[i];
				if (!(steamPlayer.player == null) && !(steamPlayer.player.movement == null) && !(steamPlayer.player.life == null) && !steamPlayer.player.life.isDead)
				{
					if (steamPlayer.player.movement.bound == this.nav)
					{
						flag = true;
					}
					if (steamPlayer.player.movement.nav == this.nav)
					{
						flag2 = true;
					}
					if (flag && flag2)
					{
						break;
					}
				}
			}
			if (flag)
			{
				if (this.bossZombie.isDead)
				{
					this.bossZombie = null;
					if (flag2)
					{
						this.UpdateBoss();
					}
				}
			}
			else
			{
				EPlayerKill eplayerKill;
				uint num;
				this.bossZombie.askDamage(50000, Vector3.up, out eplayerKill, out num, false, false, EZombieStunOverride.None);
			}
		}

		public void UpdateBoss()
		{
			if (this.bossZombie != null)
			{
				return;
			}
			for (int i = 0; i < Provider.clients.Count; i++)
			{
				SteamPlayer steamPlayer = Provider.clients[i];
				if (!(steamPlayer.player == null) && !(steamPlayer.player.movement == null) && !(steamPlayer.player.life == null) && !steamPlayer.player.life.isDead)
				{
					if (steamPlayer.player.movement.nav == this.nav)
					{
						for (int j = 0; j < steamPlayer.player.quests.questsList.Count; j++)
						{
							PlayerQuest playerQuest = steamPlayer.player.quests.questsList[j];
							if (playerQuest != null && playerQuest.asset != null)
							{
								for (int k = 0; k < playerQuest.asset.conditions.Length; k++)
								{
									NPCZombieKillsCondition npczombieKillsCondition = playerQuest.asset.conditions[k] as NPCZombieKillsCondition;
									if (npczombieKillsCondition != null)
									{
										if (npczombieKillsCondition.nav == this.nav && npczombieKillsCondition.spawn && !npczombieKillsCondition.isConditionMet(steamPlayer.player))
										{
											bool flag = npczombieKillsCondition.spawnQuantity < 2;
											int num = Mathf.Min(this.zombies.Count, npczombieKillsCondition.spawnQuantity);
											int num2 = 0;
											foreach (Zombie zombie in this.zombies)
											{
												if (zombie != null && !zombie.isDead && zombie.speciality == npczombieKillsCondition.zombie)
												{
													num2++;
												}
											}
											int l;
											for (l = num2; l < num; l++)
											{
												Zombie zombie2 = null;
												for (int m = 0; m < this.zombies.Count; m++)
												{
													Zombie zombie3 = this.zombies[m];
													if (zombie3 != null && zombie3.isDead)
													{
														zombie2 = zombie3;
														break;
													}
												}
												if (zombie2 == null)
												{
													for (int n = 0; n < this.zombies.Count; n++)
													{
														Zombie zombie4 = this.zombies[n];
														if (zombie4 != null && !zombie4.isDead && zombie4.speciality != npczombieKillsCondition.zombie && !zombie4.isHunting)
														{
															zombie2 = zombie4;
															break;
														}
													}
												}
												if (zombie2 == null)
												{
													for (int num3 = 0; num3 < this.zombies.Count; num3++)
													{
														Zombie zombie5 = this.zombies[num3];
														if (zombie5 != null && !zombie5.isDead && zombie5.speciality != npczombieKillsCondition.zombie)
														{
															zombie2 = zombie5;
															break;
														}
													}
												}
												if (zombie2 != null)
												{
													Vector3 position = zombie2.transform.position;
													if (zombie2.isDead)
													{
														for (int num4 = 0; num4 < 10; num4++)
														{
															ZombieSpawnpoint zombieSpawnpoint = LevelZombies.zombies[(int)this.nav][UnityEngine.Random.Range(0, LevelZombies.zombies[(int)this.nav].Count)];
															if (SafezoneManager.checkPointValid(zombieSpawnpoint.point))
															{
																break;
															}
															position = zombieSpawnpoint.point;
															position.y += 0.1f;
														}
													}
													zombie2.sendRevive(zombie2.type, (byte)npczombieKillsCondition.zombie, zombie2.shirt, zombie2.pants, zombie2.hat, zombie2.gear, position, UnityEngine.Random.Range(0f, 360f));
													if (flag)
													{
														this.bossZombie = zombie2;
													}
												}
											}
											Debug.Log(string.Concat(new object[]
											{
												"Spawned ",
												l,
												" ",
												npczombieKillsCondition.zombie,
												" zombies in nav ",
												this.nav,
												" for quest ",
												playerQuest.id,
												", isBoss ",
												flag,
												" boss = ",
												this.bossZombie
											}));
										}
									}
								}
							}
						}
					}
				}
			}
		}

		private void onMoonUpdated(bool isFullMoon)
		{
			if (this.onHyperUpdated != null)
			{
				this.onHyperUpdated(this.isHyper);
			}
		}

		public void destroy()
		{
			ushort num = 0;
			while ((int)num < this.zombies.Count)
			{
				UnityEngine.Object.Destroy(this.zombies[(int)num].gameObject);
				num += 1;
			}
			this.zombies.Clear();
			this.hasMega = false;
		}

		public void init()
		{
			LightingManager.onMoonUpdated = (MoonUpdated)Delegate.Combine(LightingManager.onMoonUpdated, new MoonUpdated(this.onMoonUpdated));
		}

		public HyperUpdated onHyperUpdated;

		public ZombieLifeUpdated onZombieLifeUpdated;

		private List<Zombie> _zombies;

		public ushort updates;

		public ushort respawnZombieIndex;

		public int alive;

		public bool isNetworked;

		public float lastMega;

		public bool hasMega;

		private bool _hasBeacon;

		public bool isRadioactive;

		private Zombie bossZombie;
	}
}

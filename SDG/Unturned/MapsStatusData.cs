﻿using System;
using System.Collections.Generic;

namespace SDG.Unturned
{
	public class MapsStatusData
	{
		public MapsStatusData()
		{
			this.Official = EMapStatus.NONE;
			this.Curated = EMapStatus.NONE;
			this.Misc = EMapStatus.NONE;
			this.Retired_Curated_Maps = new List<RetiredCuratedMap>();
		}

		public EMapStatus Official;

		public EMapStatus Curated;

		public EMapStatus Misc;

		public List<RetiredCuratedMap> Retired_Curated_Maps;
	}
}

﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class GraphicsSettingsResolution
	{
		public GraphicsSettingsResolution(Resolution resolution)
		{
			this.Width = resolution.width;
			this.Height = resolution.height;
		}

		public GraphicsSettingsResolution()
		{
			if (Screen.resolutions.Length > 0)
			{
				Resolution resolution = Screen.resolutions[Screen.resolutions.Length - 1];
				this.Width = resolution.width;
				this.Height = resolution.height;
			}
		}

		public int Width { get; set; }

		public int Height { get; set; }
	}
}

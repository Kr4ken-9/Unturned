﻿using System;

namespace SDG.Unturned
{
	public class EconCraftOption
	{
		public EconCraftOption(string newToken, int newGenerate, int newScrapsNeeded)
		{
			this.token = newToken;
			this.generate = newGenerate;
			this.scrapsNeeded = newScrapsNeeded;
		}

		public string token;

		public int generate;

		public int scrapsNeeded;
	}
}

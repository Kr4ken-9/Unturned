﻿using System;
using System.Collections.Generic;
using SDG.Provider;
using Steamworks;
using UnityEngine;

namespace SDG.Unturned
{
	public class SteamPlayer
	{
		public SteamPlayer(SteamPlayerID newPlayerID, Transform newModel, bool newPro, bool newAdmin, int newChannel, byte newFace, byte newHair, byte newBeard, Color newSkin, Color newColor, Color newMarkerColor, bool newHand, int newShirtItem, int newPantsItem, int newHatItem, int newBackpackItem, int newVestItem, int newMaskItem, int newGlassesItem, int[] newSkinItems, string[] newSkinTags, string[] newSkinDynamicProps, EPlayerSkillset newSkillset, string newLanguage, CSteamID newLobbyID)
		{
			this._playerID = newPlayerID;
			this._model = newModel;
			this.model.name = this.playerID.characterName + " [" + this.playerID.playerName + "]";
			this.model.parent = LevelPlayers.models;
			this.model.GetComponent<SteamChannel>().id = newChannel;
			this.model.GetComponent<SteamChannel>().owner = this;
			this.model.GetComponent<SteamChannel>().isOwner = (this.playerID.steamID == Provider.client);
			this.model.GetComponent<SteamChannel>().setup();
			this._player = this.model.GetComponent<Player>();
			this._isPro = newPro;
			this._channel = newChannel;
			this.isAdmin = newAdmin;
			this.face = newFace;
			this._hair = newHair;
			this._beard = newBeard;
			this._skin = newSkin;
			this._color = newColor;
			this._markerColor = newMarkerColor;
			this._hand = newHand;
			this._skillset = newSkillset;
			this._language = newLanguage;
			this.shirtItem = newShirtItem;
			this.pantsItem = newPantsItem;
			this.hatItem = newHatItem;
			this.backpackItem = newBackpackItem;
			this.vestItem = newVestItem;
			this.maskItem = newMaskItem;
			this.glassesItem = newGlassesItem;
			this.skinItems = newSkinItems;
			this.skinTags = newSkinTags;
			this.skinDynamicProps = newSkinDynamicProps;
			this.itemSkins = new Dictionary<ushort, int>();
			this.vehicleSkins = new Dictionary<ushort, int>();
			this.modifiedItems = new HashSet<ushort>();
			for (int i = 0; i < this.skinItems.Length; i++)
			{
				int num = this.skinItems[i];
				if (num != 0)
				{
					ushort num2;
					ushort num3;
					Provider.provider.economyService.getInventoryTargetID(num, out num2, out num3);
					if (num2 > 0)
					{
						if (!this.itemSkins.ContainsKey(num2))
						{
							this.itemSkins.Add(num2, num);
						}
					}
					else if (num3 > 0 && !this.vehicleSkins.ContainsKey(num2))
					{
						this.vehicleSkins.Add(num3, num);
					}
				}
			}
			this.pings = new float[4];
			this.timeLastPacketWasReceivedFromClient = Time.realtimeSinceStartup;
			this.lastChat = Time.realtimeSinceStartup;
			this.nextVote = Time.realtimeSinceStartup;
			this.lastReceivedPingRequestRealtime = Time.realtimeSinceStartup;
			this._joined = Time.realtimeSinceStartup;
			this.lobbyID = newLobbyID;
		}

		public SteamPlayerID playerID
		{
			get
			{
				return this._playerID;
			}
		}

		public Transform model
		{
			get
			{
				return this._model;
			}
		}

		public Player player
		{
			get
			{
				return this._player;
			}
		}

		public bool isPro
		{
			get
			{
				return (!OptionsSettings.streamer || !(this.playerID.steamID != Provider.user)) && this._isPro;
			}
		}

		public int channel
		{
			get
			{
				return this._channel;
			}
		}

		public bool isAdmin
		{
			get
			{
				return (!OptionsSettings.streamer || !(this.playerID.steamID != Provider.user)) && this._isAdmin;
			}
			set
			{
				this._isAdmin = value;
			}
		}

		public float ping
		{
			get
			{
				return this._ping;
			}
		}

		public float joined
		{
			get
			{
				return this._joined;
			}
		}

		public byte hair
		{
			get
			{
				return this._hair;
			}
		}

		public byte beard
		{
			get
			{
				return this._beard;
			}
		}

		public Color skin
		{
			get
			{
				return this._skin;
			}
		}

		public Color color
		{
			get
			{
				return this._color;
			}
		}

		public Color markerColor
		{
			get
			{
				return this._markerColor;
			}
		}

		public bool hand
		{
			get
			{
				return this._hand;
			}
		}

		public EPlayerSkillset skillset
		{
			get
			{
				return this._skillset;
			}
		}

		public string language
		{
			get
			{
				return this._language;
			}
		}

		public CSteamID lobbyID { get; private set; }

		public bool getItemSkinItemDefID(ushort itemID, out int itemdefid)
		{
			itemdefid = 0;
			return this.itemSkins != null && this.itemSkins.TryGetValue(itemID, out itemdefid);
		}

		public bool getVehicleSkinItemDefID(ushort vehicleID, out int itemdefid)
		{
			itemdefid = 0;
			return this.vehicleSkins != null && this.vehicleSkins.TryGetValue(vehicleID, out itemdefid);
		}

		public bool getTagsAndDynamicPropsForItem(int item, out string tags, out string dynamic_props)
		{
			tags = string.Empty;
			dynamic_props = string.Empty;
			int i = 0;
			while (i < this.skinItems.Length)
			{
				if (this.skinItems[i] == item)
				{
					if (i < this.skinTags.Length && i < this.skinDynamicProps.Length)
					{
						tags = this.skinTags[i];
						dynamic_props = this.skinDynamicProps[i];
						return true;
					}
					return false;
				}
				else
				{
					i++;
				}
			}
			return false;
		}

		public bool getStatTrackerValue(ushort itemID, out EStatTrackerType type, out int kills)
		{
			type = EStatTrackerType.NONE;
			kills = -1;
			int item;
			if (!this.getItemSkinItemDefID(itemID, out item))
			{
				return false;
			}
			string tags;
			string dynamic_props;
			if (!this.getTagsAndDynamicPropsForItem(item, out tags, out dynamic_props))
			{
				return false;
			}
			DynamicEconDetails dynamicEconDetails = new DynamicEconDetails(tags, dynamic_props);
			return dynamicEconDetails.getStatTrackerValue(out type, out kills);
		}

		public void incrementStatTrackerValue(ushort itemID, EPlayerStat stat)
		{
			int num;
			if (!this.getItemSkinItemDefID(itemID, out num))
			{
				return;
			}
			string tags;
			string dynamic_props;
			if (!this.getTagsAndDynamicPropsForItem(num, out tags, out dynamic_props))
			{
				return;
			}
			DynamicEconDetails dynamicEconDetails = new DynamicEconDetails(tags, dynamic_props);
			EStatTrackerType estatTrackerType;
			int num2;
			if (!dynamicEconDetails.getStatTrackerValue(out estatTrackerType, out num2))
			{
				return;
			}
			if (estatTrackerType != EStatTrackerType.TOTAL)
			{
				if (estatTrackerType != EStatTrackerType.PLAYER)
				{
					return;
				}
				if (stat != EPlayerStat.KILLS_PLAYERS)
				{
					return;
				}
			}
			else if (stat != EPlayerStat.KILLS_ANIMALS && stat != EPlayerStat.KILLS_PLAYERS && stat != EPlayerStat.KILLS_ZOMBIES_MEGA && stat != EPlayerStat.KILLS_ZOMBIES_NORMAL)
			{
				return;
			}
			if (!this.modifiedItems.Contains(itemID))
			{
				this.modifiedItems.Add(itemID);
			}
			num2++;
			for (int i = 0; i < this.skinItems.Length; i++)
			{
				if (this.skinItems[i] == num)
				{
					if (i < this.skinDynamicProps.Length)
					{
						this.skinDynamicProps[i] = dynamicEconDetails.getPredictedDynamicPropsJsonForStatTracker(estatTrackerType, num2);
					}
					break;
				}
			}
		}

		public void commitModifiedDynamicProps()
		{
			if (this.modifiedItems.Count < 1)
			{
				return;
			}
			SteamInventoryUpdateHandle_t handle = SteamInventory.StartUpdateProperties();
			foreach (ushort itemID in this.modifiedItems)
			{
				ulong value;
				if (Characters.getPackageForItemID(itemID, out value))
				{
					EStatTrackerType type;
					int num;
					if (this.getStatTrackerValue(itemID, out type, out num))
					{
						string statTrackerPropertyName = Provider.provider.economyService.getStatTrackerPropertyName(type);
						if (!string.IsNullOrEmpty(statTrackerPropertyName))
						{
							SteamInventory.SetProperty(handle, new SteamItemInstanceID_t(value), statTrackerPropertyName, (long)num);
						}
					}
				}
			}
			SteamInventory.SubmitUpdateProperties(handle, out Provider.provider.economyService.commitResult);
		}

		public void lag(float value)
		{
			value = Mathf.Clamp01(value);
			this._ping = value;
			for (int i = this.pings.Length - 1; i > 0; i--)
			{
				this.pings[i] = this.pings[i - 1];
				if (this.pings[i] > 0.001f)
				{
					this._ping += this.pings[i];
				}
			}
			this._ping /= (float)this.pings.Length;
			this.pings[0] = value;
		}

		private SteamPlayerID _playerID;

		private Transform _model;

		private Player _player;

		private bool _isPro;

		private int _channel;

		private bool _isAdmin;

		private float[] pings;

		private float _ping;

		private float _joined;

		public byte face;

		private byte _hair;

		private byte _beard;

		private Color _skin;

		private Color _color;

		private Color _markerColor;

		private bool _hand;

		public int shirtItem;

		public int pantsItem;

		public int hatItem;

		public int backpackItem;

		public int vestItem;

		public int maskItem;

		public int glassesItem;

		public int[] skinItems;

		public string[] skinTags;

		public string[] skinDynamicProps;

		public Dictionary<ushort, int> itemSkins;

		public Dictionary<ushort, int> vehicleSkins;

		public HashSet<ushort> modifiedItems;

		private EPlayerSkillset _skillset;

		private string _language;

		public float timeLastPacketWasReceivedFromClient;

		public float timeLastPingRequestWasSentToClient;

		public float lastChat;

		public float nextVote;

		public bool isMuted;

		public float rpcCredits;

		public float lastReceivedPingRequestRealtime;

		public HashSet<string> oncePerPlayerRPCs = new HashSet<string>();
	}
}

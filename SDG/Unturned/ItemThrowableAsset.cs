﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class ItemThrowableAsset : ItemWeaponAsset
	{
		public ItemThrowableAsset(Bundle bundle, Data data, Local localization, ushort id) : base(bundle, data, localization, id)
		{
			this._use = (AudioClip)bundle.load("Use");
			this._throwable = (GameObject)bundle.load("Throwable");
			this._explosion = data.readUInt16("Explosion");
			this._isExplosive = data.has("Explosive");
			this._isFlash = data.has("Flash");
			this._isSticky = data.has("Sticky");
			this._explodeOnImpact = data.has("Explode_On_Impact");
			if (data.has("Fuse_Length"))
			{
				this._fuseLength = data.readSingle("Fuse_Length");
			}
			else if (this.isExplosive || this.isFlash)
			{
				this._fuseLength = 2.5f;
			}
			else
			{
				this._fuseLength = 180f;
			}
			bundle.unload();
		}

		public AudioClip use
		{
			get
			{
				return this._use;
			}
		}

		public GameObject throwable
		{
			get
			{
				return this._throwable;
			}
		}

		public ushort explosion
		{
			get
			{
				return this._explosion;
			}
		}

		public bool isExplosive
		{
			get
			{
				return this._isExplosive;
			}
		}

		public bool isFlash
		{
			get
			{
				return this._isFlash;
			}
		}

		public bool isSticky
		{
			get
			{
				return this._isSticky;
			}
		}

		public bool explodeOnImpact
		{
			get
			{
				return this._explodeOnImpact;
			}
		}

		public float fuseLength
		{
			get
			{
				return this._fuseLength;
			}
		}

		public override bool isDangerous
		{
			get
			{
				return this.isExplosive;
			}
		}

		protected AudioClip _use;

		protected GameObject _throwable;

		private ushort _explosion;

		private bool _isExplosive;

		private bool _isFlash;

		private bool _isSticky;

		private bool _explodeOnImpact;

		private float _fuseLength;
	}
}

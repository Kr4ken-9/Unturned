﻿using System;

namespace SDG.Unturned
{
	public class EventsConfigData
	{
		public EventsConfigData(EGameMode mode)
		{
			this.Rain_Frequency_Min = 2.3f;
			this.Rain_Frequency_Max = 5.6f;
			this.Rain_Duration_Min = 0.05f;
			this.Rain_Duration_Max = 0.15f;
			this.Snow_Frequency_Min = 1.3f;
			this.Snow_Frequency_Max = 4.6f;
			this.Snow_Duration_Min = 0.2f;
			this.Snow_Duration_Max = 0.5f;
			this.Airdrop_Frequency_Min = 0.8f;
			this.Airdrop_Frequency_Max = 6.5f;
			this.Airdrop_Speed = 128f;
			this.Airdrop_Force = 9.5f;
			this.Arena_Clear_Timer = 5u;
			this.Arena_Finale_Timer = 10u;
			this.Arena_Restart_Timer = 15u;
			this.Arena_Compactor_Delay_Timer = 1u;
			this.Arena_Compactor_Pause_Timer = 5u;
			this.Arena_Min_Players = 2u;
			this.Arena_Compactor_Damage = 10u;
			this.Arena_Use_Airdrops = true;
			this.Arena_Use_Compactor_Pause = true;
			this.Arena_Compactor_Speed_Tiny = 0.5f;
			this.Arena_Compactor_Speed_Small = 1.5f;
			this.Arena_Compactor_Speed_Medium = 3f;
			this.Arena_Compactor_Speed_Large = 4.5f;
			this.Arena_Compactor_Speed_Insane = 6f;
			this.Arena_Compactor_Shrink_Factor = 0.5f;
		}

		public float Rain_Frequency_Min;

		public float Rain_Frequency_Max;

		public float Rain_Duration_Min;

		public float Rain_Duration_Max;

		public float Snow_Frequency_Min;

		public float Snow_Frequency_Max;

		public float Snow_Duration_Min;

		public float Snow_Duration_Max;

		public float Airdrop_Frequency_Min;

		public float Airdrop_Frequency_Max;

		public float Airdrop_Speed;

		public float Airdrop_Force;

		public uint Arena_Min_Players;

		public uint Arena_Compactor_Damage;

		public uint Arena_Clear_Timer;

		public uint Arena_Finale_Timer;

		public uint Arena_Restart_Timer;

		public uint Arena_Compactor_Delay_Timer;

		public uint Arena_Compactor_Pause_Timer;

		public bool Arena_Use_Airdrops;

		public bool Arena_Use_Compactor_Pause;

		public float Arena_Compactor_Speed_Tiny;

		public float Arena_Compactor_Speed_Small;

		public float Arena_Compactor_Speed_Medium;

		public float Arena_Compactor_Speed_Large;

		public float Arena_Compactor_Speed_Insane;

		public float Arena_Compactor_Shrink_Factor;
	}
}

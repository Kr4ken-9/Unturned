﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.CompilerServices;
using SDG.Framework.Translations;
using Steamworks;

namespace SDG.Unturned
{
	public static class DedicatedUGC
	{
		public static List<SteamContent> ugc { get; private set; }

		public static List<ulong> installing { get; private set; }

		public static event DedicatedUGCInstalledHandler installed;

		public static void registerItemInstallation(ulong id)
		{
			DedicatedUGC.installing.Add(id);
		}

		public static void beginInstallingItems()
		{
			CommandWindow.Log(DedicatedUGC.installing.Count + " workshop items to install...");
			if (DedicatedUGC.installing.Count == 0)
			{
				DedicatedUGC.triggerInstalled();
			}
			else
			{
				CommandWindow.Log("Submitting workshop item query...");
				PublishedFileId_t[] array = new PublishedFileId_t[DedicatedUGC.installing.Count];
				for (int i = 0; i < array.Length; i++)
				{
					array[i] = new PublishedFileId_t(DedicatedUGC.installing[i]);
				}
				DedicatedUGC.queryHandle = SteamGameServerUGC.CreateQueryUGCDetailsRequest(array, (uint)array.Length);
				SteamGameServerUGC.SetReturnKeyValueTags(DedicatedUGC.queryHandle, true);
				SteamAPICall_t hAPICall = SteamGameServerUGC.SendQueryUGCRequest(DedicatedUGC.queryHandle);
				DedicatedUGC.queryCompleted.Set(hAPICall, null);
			}
		}

		public static void installNextItem()
		{
			if (DedicatedUGC.installing.Count == 0)
			{
				DedicatedUGC.triggerInstalled();
			}
			else
			{
				CommandWindow.Log("Downloading workshop item: " + DedicatedUGC.installing[0]);
				if (!SteamGameServerUGC.DownloadItem((PublishedFileId_t)DedicatedUGC.installing[0], true))
				{
					DedicatedUGC.installing.RemoveAt(0);
					CommandWindow.Log("Unable to download item!");
					DedicatedUGC.installNextItem();
				}
			}
		}

		private static void onQueryCompleted(SteamUGCQueryCompleted_t callback, bool ioFailure)
		{
			if (callback.m_handle != DedicatedUGC.queryHandle)
			{
				return;
			}
			if (!ioFailure)
			{
				if (callback.m_eResult == EResult.k_EResultOK)
				{
					CommandWindow.Log("Workshop query yielded " + callback.m_unNumResultsReturned + " result(s)");
					string ipfromUInt = Parser.getIPFromUInt32(SteamGameServer.GetPublicIP());
					CommandWindow.Log("This server's allowed IP for Workshop downloads: " + ipfromUInt);
					List<ulong> list = new List<ulong>();
					for (uint num = 0u; num < callback.m_unNumResultsReturned; num += 1u)
					{
						SteamUGCDetails_t steamUGCDetails_t;
						SteamGameServerUGC.GetQueryUGCResult(DedicatedUGC.queryHandle, num, out steamUGCDetails_t);
						if (!string.IsNullOrEmpty(steamUGCDetails_t.m_rgchTitle))
						{
							string text = string.Concat(new object[]
							{
								steamUGCDetails_t.m_nPublishedFileId,
								" '",
								steamUGCDetails_t.m_rgchTitle,
								"'"
							});
							list.Add(steamUGCDetails_t.m_nPublishedFileId.m_PublishedFileId);
							string text2 = null;
							uint queryUGCNumKeyValueTags = SteamGameServerUGC.GetQueryUGCNumKeyValueTags(DedicatedUGC.queryHandle, num);
							for (uint num2 = 0u; num2 < queryUGCNumKeyValueTags; num2 += 1u)
							{
								string text3;
								string text4;
								if (SteamGameServerUGC.GetQueryUGCKeyValueTag(DedicatedUGC.queryHandle, num, num2, out text3, 255u, out text4, 255u) && text3.Equals("allowed_ips", StringComparison.InvariantCultureIgnoreCase))
								{
									text2 = text4;
									break;
								}
							}
							bool flag = false;
							if (text2 != null)
							{
								string[] array = text2.Split(new char[]
								{
									','
								});
								if (array.Length > 0)
								{
									flag = true;
									bool flag2 = false;
									foreach (string a in array)
									{
										if (a == ipfromUInt)
										{
											flag2 = true;
											break;
										}
									}
									if (flag2)
									{
										CommandWindow.Log("Authorized to download " + text);
									}
									else
									{
										CommandWindow.LogWarning("Not registered in the allowed IPs list for: " + text);
										DedicatedUGC.installing.Remove(steamUGCDetails_t.m_nPublishedFileId.m_PublishedFileId);
									}
								}
							}
							if (!flag)
							{
								CommandWindow.Log(text + " has no download restrictions");
							}
						}
					}
					for (int j = DedicatedUGC.installing.Count - 1; j >= 0; j--)
					{
						ulong num3 = DedicatedUGC.installing[j];
						if (!list.Contains(num3))
						{
							CommandWindow.LogWarning("No workshop item was found matching this id: " + num3);
							DedicatedUGC.installing.RemoveAtFast(j);
						}
					}
					CommandWindow.Log(DedicatedUGC.installing.Count + " workshop item(s) to download...");
					DedicatedUGC.installNextItem();
				}
				else
				{
					CommandWindow.LogError("Encountered an error when querying workshop: " + callback.m_eResult);
				}
			}
			else
			{
				CommandWindow.LogError("Encountered an IO error when querying workshop!");
			}
			SteamGameServerUGC.ReleaseQueryUGCRequest(DedicatedUGC.queryHandle);
			DedicatedUGC.queryHandle = UGCQueryHandle_t.Invalid;
		}

		private static void onItemDownloaded(DownloadItemResult_t callback)
		{
			if (DedicatedUGC.installing == null || DedicatedUGC.installing.Count == 0)
			{
				return;
			}
			if (!DedicatedUGC.installing.Remove(callback.m_nPublishedFileId.m_PublishedFileId))
			{
				return;
			}
			if (callback.m_eResult == EResult.k_EResultOK)
			{
				CommandWindow.Log("Successfully downloaded workshop item: " + callback.m_nPublishedFileId.m_PublishedFileId);
				Provider.registerServerUsingWorkshopFileId(callback.m_nPublishedFileId.m_PublishedFileId);
				ulong num;
				string text;
				uint num2;
				if (SteamGameServerUGC.GetItemInstallInfo(callback.m_nPublishedFileId, out num, out text, 1024u, out num2) && ReadWrite.folderExists(text, false))
				{
					if (WorkshopTool.checkMapMeta(text, false))
					{
						DedicatedUGC.ugc.Add(new SteamContent(callback.m_nPublishedFileId, text, ESteamUGCType.MAP));
						if (ReadWrite.folderExists(text + "/Bundles", false))
						{
							Assets.load(text + "/Bundles", false, false, true, EAssetOrigin.WORKSHOP, true);
						}
					}
					else if (WorkshopTool.checkLocalizationMeta(text, false))
					{
						DedicatedUGC.ugc.Add(new SteamContent(callback.m_nPublishedFileId, text, ESteamUGCType.LOCALIZATION));
					}
					else if (WorkshopTool.checkObjectMeta(text, false))
					{
						DedicatedUGC.ugc.Add(new SteamContent(callback.m_nPublishedFileId, text, ESteamUGCType.OBJECT));
						Assets.load(text, false, false, true, EAssetOrigin.WORKSHOP, true);
					}
					else if (WorkshopTool.checkItemMeta(text, false))
					{
						DedicatedUGC.ugc.Add(new SteamContent(callback.m_nPublishedFileId, text, ESteamUGCType.ITEM));
						Assets.load(text, false, false, true, EAssetOrigin.WORKSHOP, true);
					}
					else if (WorkshopTool.checkVehicleMeta(text, false))
					{
						DedicatedUGC.ugc.Add(new SteamContent(callback.m_nPublishedFileId, text, ESteamUGCType.VEHICLE));
						Assets.load(text, false, false, true, EAssetOrigin.WORKSHOP, true);
					}
					if (Directory.Exists(text + "/Translations"))
					{
						Translator.registerTranslationDirectory(text + "/Translations");
					}
					if (Directory.Exists(text + "/Content"))
					{
						Assets.searchForAndLoadContent(text + "/Content");
					}
				}
			}
			else
			{
				CommandWindow.Log("Failed downloading workshop item: " + callback.m_nPublishedFileId.m_PublishedFileId);
			}
			DedicatedUGC.installNextItem();
		}

		public static void initialize()
		{
			if (!Dedicator.isDedicated)
			{
				throw new NotSupportedException("DedicatedUGC should only be used on dedicated server!");
			}
			DedicatedUGC.ugc = new List<SteamContent>();
			DedicatedUGC.installing = new List<ulong>();
			if (DedicatedUGC.<>f__mg$cache0 == null)
			{
				DedicatedUGC.<>f__mg$cache0 = new CallResult<SteamUGCQueryCompleted_t>.APIDispatchDelegate(DedicatedUGC.onQueryCompleted);
			}
			DedicatedUGC.queryCompleted = CallResult<SteamUGCQueryCompleted_t>.Create(DedicatedUGC.<>f__mg$cache0);
			if (DedicatedUGC.<>f__mg$cache1 == null)
			{
				DedicatedUGC.<>f__mg$cache1 = new Callback<DownloadItemResult_t>.DispatchDelegate(DedicatedUGC.onItemDownloaded);
			}
			DedicatedUGC.itemDownloaded = Callback<DownloadItemResult_t>.CreateGameServer(DedicatedUGC.<>f__mg$cache1);
			string text = string.Concat(new string[]
			{
				ReadWrite.PATH,
				ServerSavedata.directory,
				"/",
				Provider.serverID,
				"/Workshop/Steam"
			});
			if (!Directory.Exists(text))
			{
				Directory.CreateDirectory(text);
			}
			CommandWindow.Log("Workshop install folder: " + text);
			SteamGameServerUGC.BInitWorkshopForGameServer((DepotId_t)Provider.APP_ID.m_AppId, text);
		}

		private static void triggerInstalled()
		{
			if (!DedicatedUGC.linkedSpawns)
			{
				DedicatedUGC.linkedSpawns = true;
				Assets.linkSpawns();
			}
			if (DedicatedUGC.installed != null)
			{
				DedicatedUGC.installed();
			}
		}

		private static UGCQueryHandle_t queryHandle;

		private static CallResult<SteamUGCQueryCompleted_t> queryCompleted;

		private static Callback<DownloadItemResult_t> itemDownloaded;

		private static bool linkedSpawns;

		[CompilerGenerated]
		private static CallResult<SteamUGCQueryCompleted_t>.APIDispatchDelegate <>f__mg$cache0;

		[CompilerGenerated]
		private static Callback<DownloadItemResult_t>.DispatchDelegate <>f__mg$cache1;
	}
}

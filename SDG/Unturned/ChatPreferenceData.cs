﻿using System;

namespace SDG.Unturned
{
	public class ChatPreferenceData
	{
		public ChatPreferenceData()
		{
			this.Fade_Delay = 10f;
			this.History_Length = 16;
			this.Preview_Length = 5;
		}

		public float Fade_Delay;

		public int History_Length;

		public int Preview_Length;
	}
}

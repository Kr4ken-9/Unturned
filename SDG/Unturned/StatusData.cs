﻿using System;

namespace SDG.Unturned
{
	public class StatusData
	{
		public StatusData()
		{
			this.Game = new GameStatusData();
			this.Alert = new AlertStatusData();
			this.News = new NewsStatusData();
			this.Maps = new MapsStatusData();
			this.Stockpile = new StockpileStatusData();
		}

		public GameStatusData Game;

		public AlertStatusData Alert;

		public NewsStatusData News;

		public MapsStatusData Maps;

		public StockpileStatusData Stockpile;
	}
}

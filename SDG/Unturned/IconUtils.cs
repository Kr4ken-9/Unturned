﻿using System;
using System.Collections.Generic;
using UnityEngine;

namespace SDG.Unturned
{
	public class IconUtils
	{
		public static ItemDefIconInfo getItemDefIcon(ushort itemID, ushort vehicleID, ushort skinID)
		{
			if (itemID == 0 && vehicleID == 0)
			{
				return null;
			}
			ItemAsset itemAsset = (ItemAsset)Assets.find(EAssetType.ITEM, itemID);
			VehicleAsset vehicleAsset = (VehicleAsset)Assets.find(EAssetType.VEHICLE, vehicleID);
			if (itemAsset == null && vehicleAsset == null)
			{
				Debug.LogWarning("Couldn't find a matching item or vehicle asset!");
				return null;
			}
			ItemDefIconInfo itemDefIconInfo = new ItemDefIconInfo();
			if (skinID != 0)
			{
				SkinAsset skinAsset = (SkinAsset)Assets.find(EAssetType.SKIN, skinID);
				if (skinAsset == null)
				{
					Debug.LogWarning("Couldn't find a skin asset for: " + skinID);
					return null;
				}
				ushort num;
				if (vehicleAsset != null)
				{
					num = vehicleID;
				}
				else
				{
					num = itemID;
				}
				string text;
				if (vehicleAsset != null)
				{
					text = vehicleAsset.sharedSkinName;
				}
				else
				{
					text = itemAsset.name;
				}
				itemDefIconInfo.economyUploadPath = string.Concat(new string[]
				{
					GameProject.PROJECT_PATH,
					"/Economy/Icons/Skins/",
					text,
					"/",
					skinAsset.name
				});
				itemDefIconInfo.gameResourcePath = string.Concat(new string[]
				{
					Application.dataPath,
					"/Resources/Economy/Skins/",
					text,
					"/",
					skinAsset.name
				});
				itemDefIconInfo.assetImporterPath = "Assets/Resources/Economy/Skins/" + text + "/" + skinAsset.name;
				itemDefIconInfo.extraPath = string.Concat(new object[]
				{
					GameProject.PROJECT_PATH,
					"/Builds/Shared/Extras/Econ/",
					text,
					"_",
					num,
					"_",
					skinAsset.name,
					"_",
					skinAsset.id
				});
				if (vehicleAsset != null)
				{
					VehicleTool.getIcon(vehicleAsset.id, skinAsset.id, vehicleAsset, skinAsset, 200, 200, new VehicleIconReady(itemDefIconInfo.onSmallItemIconReady));
					VehicleTool.getIcon(vehicleAsset.id, skinAsset.id, vehicleAsset, skinAsset, 400, 400, new VehicleIconReady(itemDefIconInfo.onLargeItemIconReady));
				}
				else
				{
					ItemTool.getIcon(itemAsset.id, skinAsset.id, 100, itemAsset.getState(), itemAsset, skinAsset, string.Empty, string.Empty, 200, 200, true, new ItemIconReady(itemDefIconInfo.onSmallItemIconReady));
					ItemTool.getIcon(itemAsset.id, skinAsset.id, 100, itemAsset.getState(), itemAsset, skinAsset, string.Empty, string.Empty, 400, 400, true, new ItemIconReady(itemDefIconInfo.onLargeItemIconReady));
				}
			}
			else
			{
				if (itemAsset != null && string.IsNullOrEmpty(itemAsset.proPath))
				{
					Debug.LogError(string.Concat(new object[]
					{
						"Failed to find pro path for: ",
						itemID,
						" ",
						vehicleID,
						" ",
						skinID
					}));
					return null;
				}
				itemDefIconInfo.economyUploadPath = GameProject.PROJECT_PATH + "/Economy/Icons" + itemAsset.proPath;
				itemDefIconInfo.gameResourcePath = Application.dataPath + "/Resources/Economy" + itemAsset.proPath;
				itemDefIconInfo.assetImporterPath = "Assets/Resources/Economy" + itemAsset.proPath;
				itemDefIconInfo.extraPath = string.Concat(new object[]
				{
					GameProject.PROJECT_PATH,
					"/Builds/Shared/Extras/Econ/",
					itemAsset.name,
					"_",
					itemAsset.id
				});
				ItemTool.getIcon(itemAsset.id, 0, 100, itemAsset.getState(), itemAsset, null, string.Empty, string.Empty, 200, 200, true, new ItemIconReady(itemDefIconInfo.onSmallItemIconReady));
				ItemTool.getIcon(itemAsset.id, 0, 100, itemAsset.getState(), itemAsset, null, string.Empty, string.Empty, 400, 400, true, new ItemIconReady(itemDefIconInfo.onLargeItemIconReady));
			}
			IconUtils.icons.Add(itemDefIconInfo);
			return itemDefIconInfo;
		}

		public static void captureItemIcon(ItemAsset itemAsset)
		{
			if (itemAsset == null)
			{
				return;
			}
			ExtraItemIconInfo extraItemIconInfo = new ExtraItemIconInfo();
			extraItemIconInfo.extraPath = string.Concat(new object[]
			{
				ReadWrite.PATH,
				"/Extras/Icons/",
				itemAsset.name,
				"_",
				itemAsset.id
			});
			ItemTool.getIcon(itemAsset.id, 0, 100, itemAsset.getState(), itemAsset, null, string.Empty, string.Empty, (int)itemAsset.size_x * 512, (int)itemAsset.size_y * 512, false, new ItemIconReady(extraItemIconInfo.onItemIconReady));
			IconUtils.extraIcons.Add(extraItemIconInfo);
		}

		public static void captureAllItemIcons()
		{
			Asset[] array = Assets.find(EAssetType.ITEM);
			for (int i = 0; i < array.Length; i++)
			{
				ItemAsset itemAsset = array[i] as ItemAsset;
				IconUtils.captureItemIcon(itemAsset);
			}
		}

		public static List<ItemDefIconInfo> icons = new List<ItemDefIconInfo>();

		public static List<ExtraItemIconInfo> extraIcons = new List<ExtraItemIconInfo>();
	}
}

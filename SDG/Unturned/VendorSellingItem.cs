﻿using System;
using System.Collections.Generic;

namespace SDG.Unturned
{
	public class VendorSellingItem : VendorSellingBase
	{
		public VendorSellingItem(byte newIndex, ushort newID, uint newCost, INPCCondition[] newConditions) : base(newIndex, newID, newCost, newConditions)
		{
		}

		public override string displayName
		{
			get
			{
				ItemAsset itemAsset = Assets.find(EAssetType.ITEM, base.id) as ItemAsset;
				return (itemAsset == null) ? null : itemAsset.itemName;
			}
		}

		public override string displayDesc
		{
			get
			{
				ItemAsset itemAsset = Assets.find(EAssetType.ITEM, base.id) as ItemAsset;
				return (itemAsset == null) ? null : itemAsset.itemDescription;
			}
		}

		public override EItemRarity rarity
		{
			get
			{
				ItemAsset itemAsset = Assets.find(EAssetType.ITEM, base.id) as ItemAsset;
				return (itemAsset == null) ? EItemRarity.COMMON : itemAsset.rarity;
			}
		}

		public override void buy(Player player)
		{
			base.buy(player);
			player.inventory.forceAddItem(new Item(base.id, EItemOrigin.ADMIN), false, false);
		}

		public override void format(Player player, out ushort total)
		{
			VendorSellingItem.search.Clear();
			player.inventory.search(VendorSellingItem.search, base.id, false, true);
			total = 0;
			byte b = 0;
			while ((int)b < VendorSellingItem.search.Count)
			{
				total += (ushort)VendorSellingItem.search[(int)b].jar.item.amount;
				b += 1;
			}
		}

		private static List<InventorySearch> search = new List<InventorySearch>();
	}
}

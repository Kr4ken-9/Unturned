﻿using System;

namespace SDG.Unturned
{
	public enum EDeleteMode
	{
		DELETE,
		SALVAGE,
		TAG_TOOL
	}
}

﻿using System;
using System.Collections.Generic;
using SDG.Framework.Landscapes;
using Steamworks;
using UnityEngine;
using UnityEngine.UI;

namespace SDG.Unturned
{
	public class EffectManager : SteamCaller
	{
		public static EffectManager instance
		{
			get
			{
				return EffectManager.manager;
			}
		}

		public static GameObject Instantiate(GameObject element)
		{
			GameObject gameObject = EffectManager.pool.Instantiate(element);
			ParticleSystem component = gameObject.GetComponent<ParticleSystem>();
			if (component != null)
			{
				component.Stop(true);
				component.Clear(true);
			}
			gameObject.transform.parent = Level.effects;
			gameObject.tag = "Debris";
			gameObject.layer = LayerMasks.DEBRIS;
			return gameObject;
		}

		public static void Destroy(GameObject element)
		{
			if (element == null)
			{
				return;
			}
			EffectManager.pool.Destroy(element);
		}

		public static void Destroy(GameObject element, float t)
		{
			if (element == null)
			{
				return;
			}
			EffectManager.pool.Destroy(element, t);
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellEffectClearByID(CSteamID steamID, ushort id)
		{
			if (base.channel.checkServer(steamID))
			{
				string b = id.ToString();
				for (int i = 0; i < Level.effects.childCount; i++)
				{
					Transform child = Level.effects.GetChild(i);
					if (child.gameObject.activeSelf)
					{
						if (!(child.name != b))
						{
							EffectManager.Destroy(child.gameObject);
						}
					}
				}
			}
		}

		public static void askEffectClearByID(ushort id, CSteamID steamID)
		{
			if (Provider.isServer)
			{
				EffectManager.manager.channel.send("tellEffectClearByID", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
				{
					id
				});
			}
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellEffectClearAll(CSteamID steamID)
		{
			if (base.channel.checkServer(steamID))
			{
				for (int i = 0; i < Level.effects.childCount; i++)
				{
					Transform child = Level.effects.GetChild(i);
					if (child.gameObject.activeSelf)
					{
						if (!(child.name == "System"))
						{
							EffectManager.Destroy(child.gameObject);
						}
					}
				}
			}
		}

		public static void askEffectClearAll()
		{
			if (Provider.isServer)
			{
				EffectManager.manager.channel.send("tellEffectClearAll", ESteamCall.ALL, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[0]);
			}
		}

		public static void sendEffect(ushort id, byte x, byte y, byte area, Vector3 point, Vector3 normal)
		{
			EffectManager.manager.channel.send("tellEffectPointNormal", ESteamCall.CLIENTS, x, y, area, ESteamPacket.UPDATE_UNRELIABLE_BUFFER, new object[]
			{
				id,
				point,
				normal
			});
		}

		public static void sendEffect(ushort id, byte x, byte y, byte area, Vector3 point)
		{
			EffectManager.manager.channel.send("tellEffectPoint", ESteamCall.CLIENTS, x, y, area, ESteamPacket.UPDATE_UNRELIABLE_BUFFER, new object[]
			{
				id,
				point
			});
		}

		public static void sendEffectReliable(ushort id, byte x, byte y, byte area, Vector3 point, Vector3 normal)
		{
			EffectManager.manager.channel.send("tellEffectPointNormal", ESteamCall.CLIENTS, x, y, area, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point,
				normal
			});
		}

		public static void sendEffectReliable(ushort id, byte x, byte y, byte area, Vector3 point)
		{
			EffectManager.manager.channel.send("tellEffectPoint", ESteamCall.CLIENTS, x, y, area, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point
			});
		}

		public static void sendEffect(ushort id, float radius, Vector3 point, Vector3 normal)
		{
			EffectManager.manager.channel.send("tellEffectPointNormal", ESteamCall.CLIENTS, point, radius, ESteamPacket.UPDATE_UNRELIABLE_BUFFER, new object[]
			{
				id,
				point,
				normal
			});
		}

		public static void sendEffect(ushort id, float radius, Vector3 point)
		{
			EffectManager.manager.channel.send("tellEffectPoint", ESteamCall.CLIENTS, point, radius, ESteamPacket.UPDATE_UNRELIABLE_BUFFER, new object[]
			{
				id,
				point
			});
		}

		public static void sendEffectReliable(ushort id, float radius, Vector3 point, Vector3 normal)
		{
			EffectManager.manager.channel.send("tellEffectPointNormal", ESteamCall.CLIENTS, point, radius, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point,
				normal
			});
		}

		public static void sendEffectReliable(ushort id, float radius, Vector3 point)
		{
			EffectManager.manager.channel.send("tellEffectPoint", ESteamCall.CLIENTS, point, radius, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point
			});
		}

		public static void sendEffect(ushort id, CSteamID steamID, Vector3 point, Vector3 normal)
		{
			EffectManager.manager.channel.send("tellEffectPointNormal", steamID, ESteamPacket.UPDATE_UNRELIABLE_BUFFER, new object[]
			{
				id,
				point,
				normal
			});
		}

		public static void sendEffect(ushort id, CSteamID steamID, Vector3 point)
		{
			EffectManager.manager.channel.send("tellEffectPoint", steamID, ESteamPacket.UPDATE_UNRELIABLE_BUFFER, new object[]
			{
				id,
				point
			});
		}

		public static void sendEffectReliable(ushort id, CSteamID steamID, Vector3 point, Vector3 normal)
		{
			EffectManager.manager.channel.send("tellEffectPointNormal", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point,
				normal
			});
		}

		public static void sendEffectReliable(ushort id, CSteamID steamID, Vector3 point)
		{
			EffectManager.manager.channel.send("tellEffectPoint", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point
			});
		}

		public static void sendEffectReliable(ushort id, CSteamID steamID, Vector3 point, Vector3 normal, float uniformScale)
		{
			EffectManager.manager.channel.send("tellEffectPointNormal_UniformScale", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point,
				normal,
				uniformScale
			});
		}

		public static void sendEffectReliable(ushort id, CSteamID steamID, Vector3 point, float uniformScale)
		{
			EffectManager.manager.channel.send("tellEffectPoint_UniformScale", steamID, ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				point,
				uniformScale
			});
		}

		public static void sendUIEffect(ushort id, short key, bool reliable)
		{
			EffectManager.manager.channel.send("tellUIEffect", ESteamCall.CLIENTS, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key
			});
		}

		public static void sendUIEffect(ushort id, short key, bool reliable, string arg0)
		{
			EffectManager.manager.channel.send("tellUIEffect1Arg", ESteamCall.CLIENTS, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0
			});
		}

		public static void sendUIEffect(ushort id, short key, bool reliable, string arg0, string arg1)
		{
			EffectManager.manager.channel.send("tellUIEffect2Args", ESteamCall.CLIENTS, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0,
				arg1
			});
		}

		public static void sendUIEffect(ushort id, short key, bool reliable, string arg0, string arg1, string arg2)
		{
			EffectManager.manager.channel.send("tellUIEffect3Args", ESteamCall.CLIENTS, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0,
				arg1,
				arg2
			});
		}

		public static void sendUIEffect(ushort id, short key, bool reliable, string arg0, string arg1, string arg2, string arg3)
		{
			EffectManager.manager.channel.send("tellUIEffect4Args", ESteamCall.CLIENTS, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0,
				arg1,
				arg2,
				arg3
			});
		}

		public static void sendUIEffect(ushort id, short key, CSteamID steamID, bool reliable)
		{
			EffectManager.manager.channel.send("tellUIEffect", steamID, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key
			});
		}

		public static void sendUIEffect(ushort id, short key, CSteamID steamID, bool reliable, string arg0)
		{
			EffectManager.manager.channel.send("tellUIEffect1Arg", steamID, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0
			});
		}

		public static void sendUIEffect(ushort id, short key, CSteamID steamID, bool reliable, string arg0, string arg1)
		{
			EffectManager.manager.channel.send("tellUIEffect2Args", steamID, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0,
				arg1
			});
		}

		public static void sendUIEffect(ushort id, short key, CSteamID steamID, bool reliable, string arg0, string arg1, string arg2)
		{
			EffectManager.manager.channel.send("tellUIEffect3Args", steamID, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0,
				arg1,
				arg2
			});
		}

		public static void sendUIEffect(ushort id, short key, CSteamID steamID, bool reliable, string arg0, string arg1, string arg2, string arg3)
		{
			EffectManager.manager.channel.send("tellUIEffect4Args", steamID, (!reliable) ? ESteamPacket.UPDATE_UNRELIABLE_BUFFER : ESteamPacket.UPDATE_RELIABLE_BUFFER, new object[]
			{
				id,
				key,
				arg0,
				arg1,
				arg2,
				arg3
			});
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellEffectPointNormal_UniformScale(CSteamID steamID, ushort id, Vector3 point, Vector3 normal, float uniformScale)
		{
			EffectManager.effect(id, point, normal, new Vector3(uniformScale, uniformScale, uniformScale));
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellEffectPointNormal(CSteamID steamID, ushort id, Vector3 point, Vector3 normal)
		{
			if (base.channel.checkServer(steamID))
			{
				EffectManager.effect(id, point, normal);
			}
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellEffectPoint_UniformScale(CSteamID steamID, ushort id, Vector3 point, float uniformScale)
		{
			EffectManager.effect(id, point, Vector3.up, new Vector3(uniformScale, uniformScale, uniformScale));
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellEffectPoint(CSteamID steamID, ushort id, Vector3 point)
		{
			if (base.channel.checkServer(steamID))
			{
				EffectManager.effect(id, point, Vector3.up);
			}
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellUIEffect(CSteamID steamID, ushort id, short key)
		{
			if (!base.channel.checkServer(steamID))
			{
				return;
			}
			EffectManager.createUIEffect(id, key);
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellUIEffect1Arg(CSteamID steamID, ushort id, short key, string arg0)
		{
			if (!base.channel.checkServer(steamID))
			{
				return;
			}
			EffectManager.createAndFormatUIEffect(id, key, new object[]
			{
				arg0
			});
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellUIEffect2Args(CSteamID steamID, ushort id, short key, string arg0, string arg1)
		{
			if (!base.channel.checkServer(steamID))
			{
				return;
			}
			EffectManager.createAndFormatUIEffect(id, key, new object[]
			{
				arg0,
				arg1
			});
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellUIEffect3Args(CSteamID steamID, ushort id, short key, string arg0, string arg1, string arg2)
		{
			if (!base.channel.checkServer(steamID))
			{
				return;
			}
			EffectManager.createAndFormatUIEffect(id, key, new object[]
			{
				arg0,
				arg1,
				arg2
			});
		}

		[SteamCall(ESteamCallValidation.ONLY_FROM_SERVER)]
		public void tellUIEffect4Args(CSteamID steamID, ushort id, short key, string arg0, string arg1, string arg2, string arg3)
		{
			if (!base.channel.checkServer(steamID))
			{
				return;
			}
			EffectManager.createAndFormatUIEffect(id, key, new object[]
			{
				arg0,
				arg1,
				arg2,
				arg3
			});
		}

		public static Transform createAndFormatUIEffect(ushort id, short key, params object[] args)
		{
			Transform transform = EffectManager.createUIEffect(id, key);
			EffectManager.formatTextIntoUIEffect(transform, args);
			return transform;
		}

		public static Transform createUIEffect(ushort id, short key)
		{
			EffectAsset effectAsset = (EffectAsset)Assets.find(EAssetType.EFFECT, id);
			if (effectAsset == null)
			{
				return null;
			}
			Transform transform = UnityEngine.Object.Instantiate<GameObject>(effectAsset.effect).transform;
			transform.name = id.ToString();
			transform.SetParent(Level.effects);
			if (key == -1)
			{
				if (effectAsset.lifetime > 1.401298E-45f)
				{
					EffectManager.Destroy(transform.gameObject, effectAsset.lifetime + UnityEngine.Random.Range(-effectAsset.lifetimeSpread, effectAsset.lifetimeSpread));
				}
			}
			else
			{
				GameObject element;
				if (EffectManager.indexedUIEffects.TryGetValue(key, out element))
				{
					EffectManager.Destroy(element);
					EffectManager.indexedUIEffects.Remove(key);
				}
				EffectManager.indexedUIEffects.Add(key, transform.gameObject);
			}
			return transform;
		}

		public static void formatTextIntoUIEffect(Transform effect, params object[] args)
		{
			EffectManager.formattingComponents.Clear();
			effect.GetComponentsInChildren<Text>(EffectManager.formattingComponents);
			foreach (Text text in EffectManager.formattingComponents)
			{
				text.text = string.Format(text.text, args);
			}
		}

		public static Transform effect(ushort id, Vector3 point, Vector3 normal)
		{
			return EffectManager.effect(id, point, normal, Vector3.one);
		}

		public static Transform effect(ushort id, Vector3 point, Vector3 normal, Vector3 scaleMultiplier)
		{
			EffectAsset effectAsset = (EffectAsset)Assets.find(EAssetType.EFFECT, id);
			if (effectAsset == null)
			{
				return null;
			}
			if (effectAsset.splatterTemperature != EPlayerTemperature.NONE)
			{
				Transform transform = new GameObject().transform;
				transform.name = "Temperature";
				transform.parent = Level.effects;
				transform.position = point + Vector3.down * -2f;
				transform.localScale = Vector3.one * 6f;
				transform.gameObject.SetActive(false);
				transform.gameObject.AddComponent<TemperatureTrigger>().temperature = effectAsset.splatterTemperature;
				transform.gameObject.SetActive(true);
				UnityEngine.Object.Destroy(transform.gameObject, effectAsset.splatterLifetime - effectAsset.splatterLifetimeSpread);
			}
			if (Dedicator.isDedicated)
			{
				if (!effectAsset.spawnOnDedicatedServer)
				{
					return null;
				}
			}
			else if (GraphicsSettings.effectQuality == EGraphicQuality.OFF && !effectAsset.splatterLiquid)
			{
				return null;
			}
			Quaternion quaternion = Quaternion.LookRotation(normal);
			if (effectAsset.randomizeRotation)
			{
				quaternion *= Quaternion.Euler(0f, 0f, (float)UnityEngine.Random.Range(0, 360));
			}
			Transform transform2 = EffectManager.pool.Instantiate(effectAsset.effect, point, quaternion).transform;
			transform2.name = id.ToString();
			transform2.parent = Level.effects;
			transform2.localScale = scaleMultiplier;
			if (effectAsset.splatter > 0 && (!effectAsset.gore || OptionsSettings.gore))
			{
				for (int i = 0; i < (int)(effectAsset.splatter * ((effectAsset.splatterLiquid || !(Player.player != null) || Player.player.skills.boost != EPlayerBoost.SPLATTERIFIC) ? 1 : 8)); i++)
				{
					RaycastHit raycastHit;
					if (effectAsset.splatterLiquid)
					{
						float f = UnityEngine.Random.Range(0f, 6.28318548f);
						float num = UnityEngine.Random.Range(1f, 6f);
						Ray ray = new Ray(point + new Vector3(Mathf.Cos(f) * num, 0f, Mathf.Sin(f) * num), Vector3.down);
						int splatter = RayMasks.SPLATTER;
						LandscapeHoleUtility.raycastIgnoreLandscapeIfNecessary(ray, 8f, ref splatter);
						Physics.Raycast(ray, out raycastHit, 8f, splatter);
					}
					else
					{
						Ray ray2 = new Ray(point, -2f * normal + new Vector3(UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f), UnityEngine.Random.Range(-1f, 1f)));
						int splatter2 = RayMasks.SPLATTER;
						LandscapeHoleUtility.raycastIgnoreLandscapeIfNecessary(ray2, 8f, ref splatter2);
						Physics.Raycast(ray2, out raycastHit, 8f, splatter2);
					}
					if (raycastHit.transform != null)
					{
						EPhysicsMaterial material = DamageTool.getMaterial(raycastHit.point, raycastHit.transform, raycastHit.collider);
						if (!PhysicsTool.isMaterialDynamic(material))
						{
							float num2 = UnityEngine.Random.Range(1f, 2f);
							Transform transform3 = EffectManager.pool.Instantiate(effectAsset.splatters[UnityEngine.Random.Range(0, effectAsset.splatters.Length)], raycastHit.point + raycastHit.normal * UnityEngine.Random.Range(0.04f, 0.06f), Quaternion.LookRotation(raycastHit.normal) * Quaternion.Euler(0f, 0f, (float)UnityEngine.Random.Range(0, 360))).transform;
							transform3.name = "Splatter";
							transform3.parent = Level.effects;
							transform3.localScale = new Vector3(num2, num2, num2);
							transform3.gameObject.SetActive(true);
							if (effectAsset.splatterLifetime > 1.401298E-45f)
							{
								EffectManager.pool.Destroy(transform3.gameObject, effectAsset.splatterLifetime + UnityEngine.Random.Range(-effectAsset.splatterLifetimeSpread, effectAsset.splatterLifetimeSpread));
							}
							else
							{
								EffectManager.pool.Destroy(transform3.gameObject, GraphicsSettings.effect);
							}
						}
					}
				}
			}
			if (effectAsset.gore)
			{
				transform2.GetComponent<ParticleSystem>().emission.enabled = OptionsSettings.gore;
			}
			if (!effectAsset.isStatic && transform2.GetComponent<AudioSource>() != null)
			{
				transform2.GetComponent<AudioSource>().pitch = UnityEngine.Random.Range(0.9f, 1.1f);
			}
			if (effectAsset.lifetime > 1.401298E-45f)
			{
				EffectManager.pool.Destroy(transform2.gameObject, effectAsset.lifetime + UnityEngine.Random.Range(-effectAsset.lifetimeSpread, effectAsset.lifetimeSpread));
			}
			else
			{
				float num3 = 0f;
				MeshRenderer component = transform2.GetComponent<MeshRenderer>();
				if (component == null)
				{
					ParticleSystem component2 = transform2.GetComponent<ParticleSystem>();
					if (component2 != null)
					{
						if (component2.main.loop)
						{
							num3 = component2.main.startLifetime.constantMax;
						}
						else
						{
							num3 = component2.main.duration + component2.main.startLifetime.constantMax;
						}
					}
					AudioSource component3 = transform2.GetComponent<AudioSource>();
					if (component3 != null && component3.clip != null && component3.clip.length > num3)
					{
						num3 = component3.clip.length;
					}
				}
				if (num3 < 1.401298E-45f)
				{
					num3 = GraphicsSettings.effect;
				}
				EffectManager.pool.Destroy(transform2.gameObject, num3);
			}
			if (effectAsset.blast > 0 && GraphicsSettings.blast && GraphicsSettings.renderMode == ERenderMode.DEFERRED)
			{
				EffectManager.effect(effectAsset.blast, point, new Vector3(UnityEngine.Random.Range(-0.1f, 0.1f), 1f, UnityEngine.Random.Range(-0.1f, 0.1f)));
			}
			return transform2;
		}

		private void onLevelLoaded(int level)
		{
			EffectManager.pool = new GameObjectPoolDictionary();
			EffectManager.indexedUIEffects = new Dictionary<short, GameObject>();
			if (Dedicator.isDedicated)
			{
				return;
			}
			Asset[] array = Assets.find(EAssetType.EFFECT);
			for (int i = 0; i < array.Length; i++)
			{
				EffectAsset effectAsset = array[i] as EffectAsset;
				if (effectAsset != null && !(effectAsset.effect == null) && effectAsset.preload != 0)
				{
					EffectManager.pool.Instantiate(effectAsset.effect, Level.effects, effectAsset.id.ToString(), (int)effectAsset.preload);
					if (effectAsset.splatter > 0 && effectAsset.splatterPreload > 0)
					{
						for (int j = 0; j < effectAsset.splatters.Length; j++)
						{
							EffectManager.pool.Instantiate(effectAsset.splatters[j], Level.effects, "Splatter", (int)effectAsset.splatterPreload);
						}
					}
				}
			}
		}

		private void Start()
		{
			EffectManager.manager = this;
			Level.onPrePreLevelLoaded = (PrePreLevelLoaded)Delegate.Combine(Level.onPrePreLevelLoaded, new PrePreLevelLoaded(this.onLevelLoaded));
		}

		public static readonly float SMALL = 64f;

		public static readonly float MEDIUM = 128f;

		public static readonly float LARGE = 256f;

		public static readonly float INSANE = 512f;

		private static List<Text> formattingComponents = new List<Text>();

		private static EffectManager manager;

		private static GameObjectPoolDictionary pool;

		private static Dictionary<short, GameObject> indexedUIEffects;
	}
}

﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class ItemDefIconInfo
	{
		public void onSmallItemIconReady(Texture2D texture)
		{
			this.hasSmall = true;
			this.complete();
		}

		public void onLargeItemIconReady(Texture2D texture)
		{
			byte[] bytes = texture.EncodeToPNG();
			ReadWrite.writeBytes(this.extraPath + ".png", false, false, bytes);
			this.hasLarge = true;
			this.complete();
		}

		private void complete()
		{
			if (!this.hasSmall || !this.hasLarge)
			{
				return;
			}
			IconUtils.icons.Remove(this);
		}

		public string economyUploadPath;

		public string gameResourcePath;

		public string assetImporterPath;

		public string extraPath;

		private bool hasSmall;

		private bool hasLarge;
	}
}

﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class ExtraItemIconInfo
	{
		public void onItemIconReady(Texture2D texture)
		{
			byte[] bytes = texture.EncodeToPNG();
			ReadWrite.writeBytes(this.extraPath + ".png", false, false, bytes);
			UnityEngine.Object.Destroy(texture);
			IconUtils.extraIcons.Remove(this);
		}

		public string extraPath;
	}
}

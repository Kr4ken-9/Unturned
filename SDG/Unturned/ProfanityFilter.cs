﻿using System;
using System.Collections.Generic;
using System.IO;
using UnityEngine;

namespace SDG.Unturned
{
	public class ProfanityFilter
	{
		public static string[] getCurseWords()
		{
			if (ProfanityFilter.curseWords == null)
			{
				if (string.IsNullOrEmpty(Provider.path))
				{
					ProfanityFilter.curseWords = File.ReadAllLines(ReadWrite.PATH + "/Localization/English/Curse_Words.txt");
				}
				else
				{
					string path = Provider.path + Provider.language + "/Curse_Words.txt";
					if (File.Exists(path))
					{
						ProfanityFilter.curseWords = File.ReadAllLines(path);
					}
					else
					{
						string path2 = Provider.path + "English/Curse_Words.txt";
						if (File.Exists(path2))
						{
							ProfanityFilter.curseWords = File.ReadAllLines(path2);
						}
						else
						{
							ProfanityFilter.curseWords = new string[0];
						}
					}
				}
				if (ProfanityFilter.curseWords == null || ProfanityFilter.curseWords.Length < 1)
				{
					Debug.LogError("Failed to load list of curse words for profanity filter!");
				}
				else
				{
					List<string> list = new List<string>();
					for (int i = ProfanityFilter.curseWords.Length - 1; i >= 0; i--)
					{
						string text = ProfanityFilter.curseWords[i];
						if (!string.IsNullOrEmpty(text) && !text.StartsWith("#"))
						{
							list.Add(text);
						}
					}
					ProfanityFilter.curseWords = list.ToArray();
				}
			}
			return ProfanityFilter.curseWords;
		}

		public static bool filterOutCurseWords(ref string text, char replacementChar = '#')
		{
			bool result = false;
			string text2 = text.ToLower();
			if (text.Length > 0)
			{
				bool flag = text.IndexOf(' ') != -1;
				foreach (string text3 in ProfanityFilter.getCurseWords())
				{
					int num = text2.IndexOf(text3, 0);
					while (num != -1)
					{
						if (!flag || ((num == 0 || !char.IsLetterOrDigit(text2[num - 1])) && (num == text2.Length - text3.Length || !char.IsLetterOrDigit(text2[num + text3.Length]))))
						{
							ProfanityFilter.replaceCurseWord(ref text2, num, text3.Length, replacementChar);
							ProfanityFilter.replaceCurseWord(ref text, num, text3.Length, replacementChar);
							num = text2.IndexOf(text3, num);
							result = true;
						}
						else
						{
							num = text2.IndexOf(text3, num + 1);
						}
					}
				}
			}
			return result;
		}

		private static void replaceCurseWord(ref string text, int startIndex, int curseWordLength, char replacementChar)
		{
			string text2 = text.Substring(0, startIndex);
			for (int i = 0; i < curseWordLength; i++)
			{
				text2 += replacementChar;
			}
			text2 += text.Substring(startIndex + curseWordLength, text.Length - startIndex - curseWordLength);
			text = text2;
		}

		private static string[] curseWords;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class BrowserConfigData
	{
		public BrowserConfigData()
		{
			this.Icon = string.Empty;
			this.Thumbnail = string.Empty;
			this.Desc_Hint = string.Empty;
			this.Desc_Full = string.Empty;
			this.Desc_Server_List = string.Empty;
		}

		public string Icon;

		public string Thumbnail;

		public string Desc_Hint;

		public string Desc_Full;

		public string Desc_Server_List;
	}
}

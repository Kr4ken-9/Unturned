﻿using System;

namespace SDG.Unturned
{
	public class NameTool
	{
		public static bool checkNames(string input, string name)
		{
			return input.Length <= name.Length && name.ToLower().IndexOf(input.ToLower()) != -1;
		}

		public static bool isValid(string name)
		{
			foreach (int num in name.ToCharArray())
			{
				if (num <= 31)
				{
					return false;
				}
				if (num >= 126)
				{
					return false;
				}
				if (num == 47 || num == 92 || num == 96)
				{
					return false;
				}
			}
			return true;
		}

		public static bool containsRichText(string name)
		{
			return name.IndexOf("<color", StringComparison.InvariantCultureIgnoreCase) != -1 || name.IndexOf("<b>", StringComparison.InvariantCultureIgnoreCase) != -1 || name.IndexOf("<i>", StringComparison.InvariantCultureIgnoreCase) != -1 || name.IndexOf("<size", StringComparison.InvariantCultureIgnoreCase) != -1;
		}
	}
}

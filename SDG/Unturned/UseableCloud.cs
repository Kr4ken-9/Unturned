﻿using System;

namespace SDG.Unturned
{
	public class UseableCloud : Useable
	{
		public override void equip()
		{
			base.player.animator.play("Equip", true);
			base.player.movement.itemGravityMultiplier = ((ItemCloudAsset)base.player.equipment.asset).gravity;
		}

		public override void dequip()
		{
			base.player.movement.itemGravityMultiplier = 1f;
		}
	}
}

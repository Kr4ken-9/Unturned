﻿using System;
using System.Collections.Generic;
using Steamworks;
using UnityEngine;

namespace SDG.Unturned
{
	public class Grenade : MonoBehaviour, IExplodableThrowable
	{
		public void Explode()
		{
			List<EPlayerKill> list;
			DamageTool.explode(base.transform.position, this.range, EDeathCause.GRENADE, this.killer, this.playerDamage, this.zombieDamage, this.animalDamage, this.barricadeDamage, this.structureDamage, this.vehicleDamage, this.resourceDamage, this.objectDamage, out list, EExplosionDamageType.CONVENTIONAL, 32f, true, false, EDamageOrigin.Grenade_Explosion);
			EffectManager.sendEffect(this.explosion, EffectManager.LARGE, base.transform.position);
			UnityEngine.Object.Destroy(base.gameObject);
		}

		private void Start()
		{
			base.Invoke("Explode", this.fuseLength);
		}

		public CSteamID killer;

		public float range;

		public float playerDamage;

		public float zombieDamage;

		public float animalDamage;

		public float barricadeDamage;

		public float structureDamage;

		public float vehicleDamage;

		public float resourceDamage;

		public float objectDamage;

		public ushort explosion;

		public float fuseLength = 2.5f;
	}
}

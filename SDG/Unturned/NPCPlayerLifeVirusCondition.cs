﻿using System;

namespace SDG.Unturned
{
	public class NPCPlayerLifeVirusCondition : NPCLogicCondition
	{
		public NPCPlayerLifeVirusCondition(int newVirus, ENPCLogicType newLogicType, string newText) : base(newLogicType, newText, false)
		{
			this.virus = newVirus;
		}

		public int virus { get; protected set; }

		public override bool isConditionMet(Player player)
		{
			return base.doesLogicPass<int>((int)player.life.virus, this.virus);
		}

		public override string formatCondition(Player player)
		{
			if (string.IsNullOrEmpty(this.text))
			{
				return null;
			}
			return string.Format(this.text, player.life.virus, this.virus);
		}
	}
}

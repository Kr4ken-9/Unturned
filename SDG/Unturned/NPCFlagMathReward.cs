﻿using System;

namespace SDG.Unturned
{
	public class NPCFlagMathReward : INPCReward
	{
		public NPCFlagMathReward(ushort newFlag_A_ID, ushort newFlag_B_ID, ENPCOperationType newOperationType, string newText) : base(newText)
		{
			this.flag_A_ID = newFlag_A_ID;
			this.flag_B_ID = newFlag_B_ID;
			this.operationType = newOperationType;
		}

		public ushort flag_A_ID { get; protected set; }

		public ushort flag_B_ID { get; protected set; }

		public ENPCOperationType operationType { get; protected set; }

		public override void grantReward(Player player, bool shouldSend)
		{
			short num;
			player.quests.getFlag(this.flag_A_ID, out num);
			short num2;
			player.quests.getFlag(this.flag_B_ID, out num2);
			switch (this.operationType)
			{
			case ENPCOperationType.ASSIGN:
				num = num2;
				break;
			case ENPCOperationType.ADDITION:
				num += num2;
				break;
			case ENPCOperationType.SUBTRACTION:
				num -= num2;
				break;
			case ENPCOperationType.MULTIPLICATION:
				num *= num2;
				break;
			case ENPCOperationType.DIVISION:
				num /= num2;
				break;
			default:
				return;
			}
			if (shouldSend)
			{
				player.quests.sendSetFlag(this.flag_A_ID, num);
			}
			else
			{
				player.quests.setFlag(this.flag_A_ID, num);
			}
		}
	}
}

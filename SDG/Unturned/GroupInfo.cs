﻿using System;
using Steamworks;

namespace SDG.Unturned
{
	public class GroupInfo
	{
		public GroupInfo(CSteamID newGroupID, string newName, uint newMembers)
		{
			this.groupID = newGroupID;
			this.name = newName;
			this.members = newMembers;
		}

		public CSteamID groupID { get; private set; }

		public bool useMaxGroupMembersLimit
		{
			get
			{
				return Provider.modeConfigData.Gameplay.Max_Group_Members > 0u;
			}
		}

		public bool hasSpaceForMoreMembersInGroup
		{
			get
			{
				return !this.useMaxGroupMembersLimit || this.members < Provider.modeConfigData.Gameplay.Max_Group_Members;
			}
		}

		public string name;

		public uint members;
	}
}

﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class StickyGrenade : TriggerGrenadeBase
	{
		protected override void GrenadeTriggered()
		{
			base.GrenadeTriggered();
			Rigidbody component = base.GetComponent<Rigidbody>();
			component.useGravity = false;
			component.isKinematic = true;
		}
	}
}

﻿using System;

namespace SDG.Unturned
{
	public enum ESteamCallFrequency
	{
		DEFAULT,
		ONCE_PER_PLAYER
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class NPCLogicCondition : INPCCondition
	{
		public NPCLogicCondition(ENPCLogicType newLogicType, string newText, bool newShouldReset) : base(newText, newShouldReset)
		{
			this.logicType = newLogicType;
		}

		public ENPCLogicType logicType { get; protected set; }

		protected bool doesLogicPass<T>(T a, T b) where T : IComparable
		{
			return NPCTool.doesLogicPass<T>(this.logicType, a, b);
		}
	}
}

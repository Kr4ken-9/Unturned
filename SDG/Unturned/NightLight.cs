﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class NightLight : MonoBehaviour
	{
		private void onLevelLoaded(int index)
		{
			if (this.isListeningLoad)
			{
				this.isListeningLoad = false;
				Level.onLevelLoaded = (LevelLoaded)Delegate.Remove(Level.onLevelLoaded, new LevelLoaded(this.onLevelLoaded));
			}
			if (!this.isListeningTime)
			{
				this.isListeningTime = true;
				LightingManager.onDayNightUpdated = (DayNightUpdated)Delegate.Combine(LightingManager.onDayNightUpdated, new DayNightUpdated(this.onDayNightUpdated));
			}
			this.onDayNightUpdated(LightingManager.isDaytime);
		}

		private void onDayNightUpdated(bool isDaytime)
		{
			if (this.target != null)
			{
				this.target.gameObject.SetActive(!isDaytime);
			}
			if (this.material != null)
			{
				this.material.SetColor("_EmissionColor", (!isDaytime) ? Color.white : Color.black);
			}
		}

		private void Awake()
		{
			this.material = HighlighterTool.getMaterialInstance(base.transform);
			if (Level.isEditor)
			{
				this.onDayNightUpdated(false);
				return;
			}
			if (!this.isListeningLoad)
			{
				this.isListeningLoad = true;
				Level.onLevelLoaded = (LevelLoaded)Delegate.Combine(Level.onLevelLoaded, new LevelLoaded(this.onLevelLoaded));
			}
		}

		private void OnDestroy()
		{
			if (this.material != null)
			{
				UnityEngine.Object.DestroyImmediate(this.material);
			}
			if (this.isListeningLoad)
			{
				this.isListeningLoad = false;
				Level.onLevelLoaded = (LevelLoaded)Delegate.Remove(Level.onLevelLoaded, new LevelLoaded(this.onLevelLoaded));
			}
			if (this.isListeningTime)
			{
				this.isListeningTime = false;
				LightingManager.onDayNightUpdated = (DayNightUpdated)Delegate.Remove(LightingManager.onDayNightUpdated, new DayNightUpdated(this.onDayNightUpdated));
			}
		}

		public Light target;

		private Material material;

		private bool isListeningLoad;

		private bool isListeningTime;
	}
}

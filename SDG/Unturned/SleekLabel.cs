﻿using System;
using System.Text.RegularExpressions;
using UnityEngine;

namespace SDG.Unturned
{
	public class SleekLabel : Sleek
	{
		public SleekLabel()
		{
			base.init();
			this.foregroundTint = ESleekTint.FONT;
			this.fontStyle = FontStyle.Bold;
			this.fontAlignment = TextAnchor.MiddleCenter;
			this.fontSize = SleekRender.FONT_SIZE;
			this.calculateContent();
		}

		public string text
		{
			get
			{
				return this._text;
			}
			set
			{
				this._text = value;
				this.calculateContent();
			}
		}

		public string tooltip
		{
			get
			{
				return this._tooltip;
			}
			set
			{
				this._tooltip = value;
				this.calculateContent();
			}
		}

		protected virtual void calculateContent()
		{
			this.content = new GUIContent(this.text, this.tooltip);
			if (this.isRich)
			{
				this.shadowContent = new GUIContent(SleekLabel.richTextColorTagRegex.Replace(this.text, string.Empty), SleekLabel.richTextColorTagRegex.Replace(this.tooltip, string.Empty));
			}
			else
			{
				this.shadowContent = null;
			}
		}

		public override void draw(bool ignoreCulling)
		{
			SleekRender.drawLabel(base.frame, this.fontStyle, this.fontAlignment, this.fontSize, this.shadowContent, base.foregroundColor, this.content);
			base.drawChildren(ignoreCulling);
		}

		private string _text = string.Empty;

		private string _tooltip = string.Empty;

		public FontStyle fontStyle;

		public TextAnchor fontAlignment;

		public int fontSize;

		public GUIContent content;

		protected GUIContent shadowContent;

		private static Regex richTextColorTagRegex = new Regex("</*color.*?>", RegexOptions.IgnoreCase);
	}
}

﻿using System;
using System.Runtime.CompilerServices;
using SDG.Provider;
using UnityEngine;

namespace SDG.Unturned
{
	public class MenuSurvivorsClothingInspectUI
	{
		public MenuSurvivorsClothingInspectUI()
		{
			MenuSurvivorsClothingInspectUI.container = new Sleek();
			MenuSurvivorsClothingInspectUI.container.positionOffset_X = 10;
			MenuSurvivorsClothingInspectUI.container.positionOffset_Y = 10;
			MenuSurvivorsClothingInspectUI.container.positionScale_Y = 1f;
			MenuSurvivorsClothingInspectUI.container.sizeOffset_X = -20;
			MenuSurvivorsClothingInspectUI.container.sizeOffset_Y = -20;
			MenuSurvivorsClothingInspectUI.container.sizeScale_X = 1f;
			MenuSurvivorsClothingInspectUI.container.sizeScale_Y = 1f;
			MenuUI.container.add(MenuSurvivorsClothingInspectUI.container);
			MenuSurvivorsClothingInspectUI.active = false;
			MenuSurvivorsClothingInspectUI.inventory = new Sleek();
			MenuSurvivorsClothingInspectUI.inventory.positionScale_X = 0.5f;
			MenuSurvivorsClothingInspectUI.inventory.positionOffset_Y = 10;
			MenuSurvivorsClothingInspectUI.inventory.sizeScale_X = 0.5f;
			MenuSurvivorsClothingInspectUI.inventory.sizeScale_Y = 1f;
			MenuSurvivorsClothingInspectUI.inventory.sizeOffset_Y = -20;
			MenuSurvivorsClothingInspectUI.inventory.constraint = ESleekConstraint.XY;
			MenuSurvivorsClothingInspectUI.container.add(MenuSurvivorsClothingInspectUI.inventory);
			MenuSurvivorsClothingInspectUI.image = new SleekInspect("RenderTextures/Item");
			MenuSurvivorsClothingInspectUI.image.positionScale_Y = 0.125f;
			MenuSurvivorsClothingInspectUI.image.sizeScale_X = 1f;
			MenuSurvivorsClothingInspectUI.image.sizeScale_Y = 0.75f;
			MenuSurvivorsClothingInspectUI.image.constraint = ESleekConstraint.XY;
			MenuSurvivorsClothingInspectUI.inventory.add(MenuSurvivorsClothingInspectUI.image);
			MenuSurvivorsClothingInspectUI.slider = new SleekSlider();
			MenuSurvivorsClothingInspectUI.slider.positionOffset_Y = 10;
			MenuSurvivorsClothingInspectUI.slider.positionScale_Y = 1f;
			MenuSurvivorsClothingInspectUI.slider.sizeOffset_Y = 20;
			MenuSurvivorsClothingInspectUI.slider.sizeScale_X = 1f;
			MenuSurvivorsClothingInspectUI.slider.orientation = ESleekOrientation.HORIZONTAL;
			SleekSlider sleekSlider = MenuSurvivorsClothingInspectUI.slider;
			if (MenuSurvivorsClothingInspectUI.<>f__mg$cache2 == null)
			{
				MenuSurvivorsClothingInspectUI.<>f__mg$cache2 = new Dragged(MenuSurvivorsClothingInspectUI.onDraggedSlider);
			}
			sleekSlider.onDragged = MenuSurvivorsClothingInspectUI.<>f__mg$cache2;
			MenuSurvivorsClothingInspectUI.image.add(MenuSurvivorsClothingInspectUI.slider);
			MenuSurvivorsClothingInspectUI.inspect = GameObject.Find("Inspect").transform;
			MenuSurvivorsClothingInspectUI.look = MenuSurvivorsClothingInspectUI.inspect.GetComponent<ItemLook>();
			MenuSurvivorsClothingInspectUI.camera = MenuSurvivorsClothingInspectUI.look.inspectCamera;
			MenuSurvivorsClothingInspectUI.backButton = new SleekButtonIcon((Texture2D)MenuDashboardUI.icons.load("Exit"));
			MenuSurvivorsClothingInspectUI.backButton.positionOffset_Y = -50;
			MenuSurvivorsClothingInspectUI.backButton.positionScale_Y = 1f;
			MenuSurvivorsClothingInspectUI.backButton.sizeOffset_X = 200;
			MenuSurvivorsClothingInspectUI.backButton.sizeOffset_Y = 50;
			MenuSurvivorsClothingInspectUI.backButton.text = MenuDashboardUI.localization.format("BackButtonText");
			MenuSurvivorsClothingInspectUI.backButton.tooltip = MenuDashboardUI.localization.format("BackButtonTooltip");
			SleekButton sleekButton = MenuSurvivorsClothingInspectUI.backButton;
			if (MenuSurvivorsClothingInspectUI.<>f__mg$cache3 == null)
			{
				MenuSurvivorsClothingInspectUI.<>f__mg$cache3 = new ClickedButton(MenuSurvivorsClothingInspectUI.onClickedBackButton);
			}
			sleekButton.onClickedButton = MenuSurvivorsClothingInspectUI.<>f__mg$cache3;
			MenuSurvivorsClothingInspectUI.backButton.fontSize = 14;
			MenuSurvivorsClothingInspectUI.backButton.iconImage.backgroundTint = ESleekTint.FOREGROUND;
			MenuSurvivorsClothingInspectUI.container.add(MenuSurvivorsClothingInspectUI.backButton);
		}

		public static void open()
		{
			if (MenuSurvivorsClothingInspectUI.active)
			{
				return;
			}
			MenuSurvivorsClothingInspectUI.active = true;
			MenuSurvivorsClothingInspectUI.camera.gameObject.SetActive(true);
			MenuSurvivorsClothingInspectUI.look._yaw = 0f;
			MenuSurvivorsClothingInspectUI.look.yaw = 0f;
			MenuSurvivorsClothingInspectUI.slider.state = 0f;
			MenuSurvivorsClothingInspectUI.container.lerpPositionScale(0f, 0f, ESleekLerp.EXPONENTIAL, 20f);
		}

		public static void close()
		{
			if (!MenuSurvivorsClothingInspectUI.active)
			{
				return;
			}
			MenuSurvivorsClothingInspectUI.active = false;
			MenuSurvivorsClothingInspectUI.camera.gameObject.SetActive(false);
			MenuSurvivorsClothingInspectUI.container.lerpPositionScale(0f, 1f, ESleekLerp.EXPONENTIAL, 20f);
		}

		private static bool getInspectedItemStatTrackerValue(out EStatTrackerType type, out int kills)
		{
			return Provider.provider.economyService.getInventoryStatTrackerValue(MenuSurvivorsClothingInspectUI.instance, out type, out kills);
		}

		public static void viewItem(int newItem, ulong newInstance)
		{
			MenuSurvivorsClothingInspectUI.item = newItem;
			MenuSurvivorsClothingInspectUI.instance = newInstance;
			if (MenuSurvivorsClothingInspectUI.model != null)
			{
				UnityEngine.Object.Destroy(MenuSurvivorsClothingInspectUI.model.gameObject);
			}
			ushort id;
			ushort id2;
			Provider.provider.economyService.getInventoryTargetID(MenuSurvivorsClothingInspectUI.item, out id, out id2);
			ushort inventorySkinID = Provider.provider.economyService.getInventorySkinID(MenuSurvivorsClothingInspectUI.item);
			ushort inventoryMythicID = Provider.provider.economyService.getInventoryMythicID(MenuSurvivorsClothingInspectUI.item);
			ItemAsset itemAsset = (ItemAsset)Assets.find(EAssetType.ITEM, id);
			VehicleAsset vehicleAsset = (VehicleAsset)Assets.find(EAssetType.VEHICLE, id2);
			if (itemAsset == null && vehicleAsset == null)
			{
				return;
			}
			if (inventorySkinID != 0)
			{
				SkinAsset skinAsset = (SkinAsset)Assets.find(EAssetType.SKIN, inventorySkinID);
				if (vehicleAsset != null)
				{
					MenuSurvivorsClothingInspectUI.model = VehicleTool.getVehicle(vehicleAsset.id, skinAsset.id, inventoryMythicID, vehicleAsset, skinAsset);
				}
				else
				{
					ushort id3 = itemAsset.id;
					ushort skin = inventorySkinID;
					byte quality = 100;
					byte[] state = itemAsset.getState();
					bool viewmodel = false;
					ItemAsset itemAsset2 = itemAsset;
					SkinAsset skinAsset2 = skinAsset;
					if (MenuSurvivorsClothingInspectUI.<>f__mg$cache0 == null)
					{
						MenuSurvivorsClothingInspectUI.<>f__mg$cache0 = new GetStatTrackerValueHandler(MenuSurvivorsClothingInspectUI.getInspectedItemStatTrackerValue);
					}
					MenuSurvivorsClothingInspectUI.model = ItemTool.getItem(id3, skin, quality, state, viewmodel, itemAsset2, skinAsset2, MenuSurvivorsClothingInspectUI.<>f__mg$cache0);
					if (inventoryMythicID != 0)
					{
						ItemTool.applyEffect(MenuSurvivorsClothingInspectUI.model, inventoryMythicID, EEffectType.THIRD);
					}
				}
			}
			else
			{
				ushort id4 = itemAsset.id;
				ushort skin2 = 0;
				byte quality2 = 100;
				byte[] state2 = itemAsset.getState();
				bool viewmodel2 = false;
				ItemAsset itemAsset3 = itemAsset;
				if (MenuSurvivorsClothingInspectUI.<>f__mg$cache1 == null)
				{
					MenuSurvivorsClothingInspectUI.<>f__mg$cache1 = new GetStatTrackerValueHandler(MenuSurvivorsClothingInspectUI.getInspectedItemStatTrackerValue);
				}
				MenuSurvivorsClothingInspectUI.model = ItemTool.getItem(id4, skin2, quality2, state2, viewmodel2, itemAsset3, MenuSurvivorsClothingInspectUI.<>f__mg$cache1);
				if (inventoryMythicID != 0)
				{
					ItemTool.applyEffect(MenuSurvivorsClothingInspectUI.model, inventoryMythicID, EEffectType.HOOK);
				}
			}
			MenuSurvivorsClothingInspectUI.model.parent = MenuSurvivorsClothingInspectUI.inspect;
			MenuSurvivorsClothingInspectUI.model.localPosition = Vector3.zero;
			if (vehicleAsset != null)
			{
				MenuSurvivorsClothingInspectUI.model.localRotation = Quaternion.identity;
			}
			else if (itemAsset != null && itemAsset.type == EItemType.MELEE)
			{
				MenuSurvivorsClothingInspectUI.model.localRotation = Quaternion.Euler(0f, -90f, 90f);
			}
			else
			{
				MenuSurvivorsClothingInspectUI.model.localRotation = Quaternion.Euler(-90f, 0f, 0f);
			}
			Bounds bounds = new Bounds(MenuSurvivorsClothingInspectUI.model.position, Vector3.zero);
			Collider[] components = MenuSurvivorsClothingInspectUI.model.GetComponents<Collider>();
			foreach (Collider collider in components)
			{
				Bounds bounds2 = collider.bounds;
				bounds.Encapsulate(bounds2);
			}
			MenuSurvivorsClothingInspectUI.look.pos = bounds.center;
			MenuSurvivorsClothingInspectUI.look.dist = bounds.extents.magnitude * 2.25f;
		}

		private static void onDraggedSlider(SleekSlider slider, float state)
		{
			MenuSurvivorsClothingInspectUI.look.yaw = state * 360f;
		}

		private static void onClickedBackButton(SleekButton button)
		{
			MenuSurvivorsClothingItemUI.open();
			MenuSurvivorsClothingInspectUI.close();
		}

		private static Sleek container;

		public static bool active;

		private static SleekButtonIcon backButton;

		private static Sleek inventory;

		private static SleekInspect image;

		private static SleekSlider slider;

		private static int item;

		private static ulong instance;

		private static Transform inspect;

		private static Transform model;

		private static ItemLook look;

		private static Camera camera;

		[CompilerGenerated]
		private static GetStatTrackerValueHandler <>f__mg$cache0;

		[CompilerGenerated]
		private static GetStatTrackerValueHandler <>f__mg$cache1;

		[CompilerGenerated]
		private static Dragged <>f__mg$cache2;

		[CompilerGenerated]
		private static ClickedButton <>f__mg$cache3;
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class ItemMapAsset : ItemAsset
	{
		public ItemMapAsset(Bundle bundle, Data data, Local localization, ushort id) : base(bundle, data, localization, id)
		{
			this.enablesCompass = data.has("Enables_Compass");
			this.enablesChart = data.has("Enables_Chart");
			this.enablesMap = data.has("Enables_Map");
		}

		public bool enablesCompass { get; protected set; }

		public bool enablesChart { get; protected set; }

		public bool enablesMap { get; protected set; }
	}
}

﻿using System;

namespace SDG.Unturned
{
	public class NPCPlayerLifeFoodCondition : NPCLogicCondition
	{
		public NPCPlayerLifeFoodCondition(int newFood, ENPCLogicType newLogicType, string newText) : base(newLogicType, newText, false)
		{
			this.food = newFood;
		}

		public int food { get; protected set; }

		public override bool isConditionMet(Player player)
		{
			return base.doesLogicPass<int>((int)player.life.food, this.food);
		}

		public override string formatCondition(Player player)
		{
			if (string.IsNullOrEmpty(this.text))
			{
				return null;
			}
			return string.Format(this.text, player.life.food, this.food);
		}
	}
}

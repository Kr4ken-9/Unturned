﻿using System;

namespace SDG.Unturned
{
	public class NPCTimeOfDayCondition : NPCLogicCondition
	{
		public NPCTimeOfDayCondition(int newSecond, ENPCLogicType newLogicType, string newText, bool newShouldReset) : base(newLogicType, newText, newShouldReset)
		{
			this.second = newSecond;
		}

		public int second { get; protected set; }

		public override bool isConditionMet(Player player)
		{
			float num;
			if (LightingManager.day < LevelLighting.bias)
			{
				num = LightingManager.day / LevelLighting.bias;
				num /= 2f;
			}
			else
			{
				num = (LightingManager.day - LevelLighting.bias) / (1f - LevelLighting.bias);
				num = 0.5f + num / 2f;
			}
			num += 0.25f;
			if (num >= 1f)
			{
				num -= 1f;
			}
			int a = (int)(num * 86400f);
			return base.doesLogicPass<int>(a, this.second);
		}

		public override string formatCondition(Player player)
		{
			if (string.IsNullOrEmpty(this.text))
			{
				return null;
			}
			int num = this.second / 3600;
			int num2 = this.second / 60 - num * 60;
			int num3 = this.second - num * 3600 - num2 * 60;
			string arg = string.Format("{0:D2}:{1:D2}:{2:D2}", num, num2, num3);
			return string.Format(this.text, arg);
		}
	}
}

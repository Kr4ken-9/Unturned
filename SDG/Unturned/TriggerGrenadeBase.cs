﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class TriggerGrenadeBase : MonoBehaviour
	{
		protected virtual void GrenadeTriggered()
		{
		}

		private void OnTriggerEnter(Collider other)
		{
			if (this.isStuck)
			{
				return;
			}
			if (other.isTrigger)
			{
				return;
			}
			if (other.transform == this.ignoreTransform)
			{
				return;
			}
			this.isStuck = true;
			this.GrenadeTriggered();
		}

		private void Awake()
		{
			BoxCollider component = base.GetComponent<BoxCollider>();
			component.isTrigger = true;
			component.size *= 2f;
		}

		public Transform ignoreTransform;

		private bool isStuck;
	}
}

﻿using System;
using System.Collections.Generic;
using SDG.Framework.Devkit;
using SDG.Framework.Landscapes;
using SDG.Framework.Utilities;
using SDG.Framework.Water;
using Steamworks;
using UnityEngine;

namespace SDG.Unturned
{
	public class InteractableVehicle : Interactable, ILandscaleHoleVolumeInteractionHandler
	{
		public event VehiclePassengersUpdated onPassengersUpdated;

		public event VehicleLockUpdated onLockUpdated;

		public event VehicleHeadlightsUpdated onHeadlightsUpdated;

		public event VehicleTaillightsUpdated onTaillightsUpdated;

		public event VehicleSirensUpdated onSirensUpdated;

		public event VehicleBlimpUpdated onBlimpUpdated;

		public event VehicleBatteryChangedHandler batteryChanged;

		public event VehicleSkinChangedHandler skinChanged;

		public Road road { get; protected set; }

		public float timeInsideSafezone { get; protected set; }

		public bool usesFuel
		{
			get
			{
				return !this.asset.isStaminaPowered;
			}
		}

		public bool usesBattery
		{
			get
			{
				return !this.asset.isStaminaPowered;
			}
		}

		public bool usesHealth
		{
			get
			{
				return this.asset.engine != EEngine.TRAIN;
			}
		}

		public bool isBoosting { get; protected set; }

		public bool isEngineOn { get; protected set; }

		public bool hasBattery
		{
			get
			{
				return !this.usesBattery || this.batteryCharge > 0;
			}
		}

		public bool isBatteryFull
		{
			get
			{
				return !this.usesBattery || this.batteryCharge >= 10000;
			}
		}

		public bool landscapeHoleAutoIgnoreTerrainCollision
		{
			get
			{
				return true;
			}
		}

		public void landscapeHoleBeginCollision(LandscapeHoleVolume volume, List<TerrainCollider> terrainColliders)
		{
			foreach (Wheel wheel in this.tires)
			{
				if (!(wheel.wheel == null))
				{
					foreach (TerrainCollider collider in terrainColliders)
					{
						Physics.IgnoreCollision(wheel.wheel, collider, true);
					}
				}
			}
		}

		public void landscapeHoleEndCollision(LandscapeHoleVolume volume, List<TerrainCollider> terrainColliders)
		{
			foreach (Wheel wheel in this.tires)
			{
				if (!(wheel.wheel == null))
				{
					foreach (TerrainCollider collider in terrainColliders)
					{
						Physics.IgnoreCollision(wheel.wheel, collider, false);
					}
				}
			}
		}

		public bool canUseHorn
		{
			get
			{
				return Time.realtimeSinceStartup - this.horned > 0.5f && (!this.usesBattery || this.hasBattery);
			}
		}

		public bool canUseTurret
		{
			get
			{
				return !this.isDead;
			}
		}

		public bool canTurnOnLights
		{
			get
			{
				return (!this.usesBattery || this.hasBattery) && !this.isUnderwater;
			}
		}

		public bool isRefillable
		{
			get
			{
				return this.usesFuel && this.fuel < this.asset.fuel && !this.isDriven && !this.isExploded;
			}
		}

		public bool isSiphonable
		{
			get
			{
				return this.usesFuel && this.fuel > 0 && !this.isDriven && !this.isExploded;
			}
		}

		public bool isRepaired
		{
			get
			{
				return this.health == this.asset.health;
			}
		}

		public bool isDriven
		{
			get
			{
				return this.passengers != null && this.passengers[0].player != null;
			}
		}

		public bool isDriver
		{
			get
			{
				return !Dedicator.isDedicated && this.checkDriver(Provider.client);
			}
		}

		public bool isEmpty
		{
			get
			{
				byte b = 0;
				while ((int)b < this.passengers.Length)
				{
					if (this.passengers[(int)b].player != null)
					{
						return false;
					}
					b += 1;
				}
				return true;
			}
		}

		public bool isDrowned
		{
			get
			{
				return this._isDrowned;
			}
		}

		public bool isUnderwater
		{
			get
			{
				if (this.waterCenterTransform != null)
				{
					return WaterUtility.isPointUnderwater(this.waterCenterTransform.position);
				}
				return WaterUtility.isPointUnderwater(base.transform.position + new Vector3(0f, 1f, 0f));
			}
		}

		public bool isBatteryReplaceable
		{
			get
			{
				return this.usesBattery && !this.isBatteryFull && !this.isDriven && !this.isExploded;
			}
		}

		public bool isTireReplaceable
		{
			get
			{
				return !this.isDriven && !this.isExploded && this.asset.canTiresBeDamaged;
			}
		}

		public bool canBeDamaged
		{
			get
			{
				return this.asset.engine != EEngine.TRAIN;
			}
		}

		public bool isGoingToRespawn
		{
			get
			{
				return this.isExploded || this.isDrowned;
			}
		}

		public bool isAutoClearable
		{
			get
			{
				if (this.isExploded)
				{
					return true;
				}
				if (this.isUnderwater && this.buoyancy == null)
				{
					return true;
				}
				if (this.asset != null)
				{
					if (this.asset.engine == EEngine.BOAT && this.fuel == 0)
					{
						return true;
					}
					if (this.asset.engine == EEngine.TRAIN)
					{
						return false;
					}
				}
				return false;
			}
		}

		public float lastDead
		{
			get
			{
				return this._lastDead;
			}
		}

		public float lastUnderwater
		{
			get
			{
				return this._lastUnderwater;
			}
		}

		public float lastExploded
		{
			get
			{
				return this._lastExploded;
			}
		}

		public float slip
		{
			get
			{
				return this._slip;
			}
		}

		public bool isDead
		{
			get
			{
				return this.health == 0;
			}
		}

		public float factor
		{
			get
			{
				return this._factor;
			}
		}

		public float speed
		{
			get
			{
				return this._speed;
			}
		}

		public float physicsSpeed
		{
			get
			{
				return this._physicsSpeed;
			}
		}

		public float spedometer
		{
			get
			{
				return this._spedometer;
			}
		}

		public int turn
		{
			get
			{
				return this._turn;
			}
		}

		public float steer
		{
			get
			{
				return this._steer;
			}
		}

		public TrainCar[] trainCars { get; protected set; }

		public bool sirensOn
		{
			get
			{
				return this._sirensOn;
			}
		}

		public Transform headlights
		{
			get
			{
				return this._headlights;
			}
		}

		public bool headlightsOn
		{
			get
			{
				return this._headlightsOn;
			}
		}

		public Transform taillights
		{
			get
			{
				return this._taillights;
			}
		}

		public bool taillightsOn
		{
			get
			{
				return this._taillightsOn;
			}
		}

		public CSteamID lockedOwner
		{
			get
			{
				return this._lockedOwner;
			}
		}

		public CSteamID lockedGroup
		{
			get
			{
				return this._lockedGroup;
			}
		}

		public bool isLocked
		{
			get
			{
				return this._isLocked;
			}
		}

		public bool isSkinned
		{
			get
			{
				return this.skinID != 0;
			}
		}

		public VehicleAsset asset
		{
			get
			{
				return this._asset;
			}
		}

		public Passenger[] passengers
		{
			get
			{
				return this._passengers;
			}
		}

		public Passenger[] turrets
		{
			get
			{
				return this._turrets;
			}
		}

		public Wheel[] tires { get; protected set; }

		private bool usesGravity
		{
			get
			{
				return this.asset.engine != EEngine.TRAIN;
			}
		}

		private bool isKinematic
		{
			get
			{
				return !this.usesGravity;
			}
		}

		public void replaceBattery(Player player, byte quality)
		{
			this.giveBatteryItem(player);
			VehicleManager.sendVehicleBatteryCharge(this, (ushort)(quality * 100));
		}

		public void stealBattery(Player player)
		{
			if (!this.giveBatteryItem(player))
			{
				return;
			}
			VehicleManager.sendVehicleBatteryCharge(this, 0);
		}

		protected bool giveBatteryItem(Player player)
		{
			byte b = (byte)Mathf.FloorToInt((float)this.batteryCharge / 100f);
			if (b == 0)
			{
				return false;
			}
			Item item = new Item(1450, 1, b);
			player.inventory.forceAddItem(item, false);
			return true;
		}

		public byte tireAliveMask
		{
			get
			{
				int num = 0;
				byte b = 0;
				while ((int)b < Mathf.Min(8, this.tires.Length))
				{
					if (this.tires[(int)b].isAlive)
					{
						int num2 = 1 << (int)b;
						num |= num2;
					}
					b += 1;
				}
				return (byte)num;
			}
			set
			{
				byte b = 0;
				while ((int)b < Mathf.Min(8, this.tires.Length))
				{
					if (!(this.tires[(int)b].wheel == null))
					{
						int num = 1 << (int)b;
						this.tires[(int)b].isAlive = (((int)value & num) == num);
					}
					b += 1;
				}
			}
		}

		public void sendTireAliveMaskUpdate()
		{
			VehicleManager.sendVehicleTireAliveMask(this, this.tireAliveMask);
		}

		public void askRepairTire(int index)
		{
			if (index < 0)
			{
				return;
			}
			this.tires[index].askRepair();
		}

		public void askDamageTire(int index)
		{
			if (index < 0)
			{
				return;
			}
			if (this.asset != null && !this.asset.canTiresBeDamaged)
			{
				return;
			}
			this.tires[index].askDamage();
		}

		public int getHitTireIndex(Vector3 position)
		{
			for (int i = 0; i < this.tires.Length; i++)
			{
				WheelCollider wheelCollider = this.tires[i].wheel;
				if (!(wheelCollider == null))
				{
					if ((wheelCollider.transform.position - position).sqrMagnitude < wheelCollider.radius * wheelCollider.radius)
					{
						return i;
					}
				}
			}
			return -1;
		}

		public int getClosestAliveTireIndex(Vector3 position, bool isAlive)
		{
			int result = -1;
			float num = 16f;
			for (int i = 0; i < this.tires.Length; i++)
			{
				if (this.tires[i].isAlive == isAlive)
				{
					if (!(this.tires[i].wheel == null))
					{
						float sqrMagnitude = (this.tires[i].wheel.transform.position - position).sqrMagnitude;
						if (sqrMagnitude < num)
						{
							result = i;
							num = sqrMagnitude;
						}
					}
				}
			}
			return result;
		}

		public void getDisplayFuel(out ushort currentFuel, out ushort maxFuel)
		{
			if (this.usesFuel)
			{
				currentFuel = this.fuel;
				maxFuel = this.asset.fuel;
			}
			else
			{
				if (this.passengers[0].player != null && this.passengers[0].player.player != null)
				{
					currentFuel = (ushort)this.passengers[0].player.player.life.stamina;
				}
				else if (Player.player != null)
				{
					currentFuel = (ushort)Player.player.life.stamina;
				}
				else
				{
					currentFuel = 0;
				}
				maxFuel = 100;
			}
		}

		public void askBurnFuel(ushort amount)
		{
			if (amount == 0 || this.isExploded)
			{
				return;
			}
			if (amount >= this.fuel)
			{
				this.fuel = 0;
			}
			else
			{
				this.fuel -= amount;
			}
		}

		public void askFillFuel(ushort amount)
		{
			if (amount == 0 || this.isExploded)
			{
				return;
			}
			if (amount >= this.asset.fuel - this.fuel)
			{
				this.fuel = this.asset.fuel;
			}
			else
			{
				this.fuel += amount;
			}
			VehicleManager.sendVehicleFuel(this, this.fuel);
		}

		public void askBurnBattery(ushort amount)
		{
			if (amount == 0 || this.isExploded)
			{
				return;
			}
			if (amount >= this.batteryCharge)
			{
				this.batteryCharge = 0;
			}
			else
			{
				this.batteryCharge -= amount;
			}
		}

		public void askChargeBattery(ushort amount)
		{
			if (amount == 0 || this.isExploded)
			{
				return;
			}
			if (amount >= 10000 - this.batteryCharge)
			{
				this.batteryCharge = 10000;
			}
			else
			{
				this.batteryCharge += amount;
			}
		}

		public void sendBatteryChargeUpdate()
		{
			VehicleManager.sendVehicleBatteryCharge(this, this.batteryCharge);
		}

		public void askDamage(ushort amount, bool canRepair)
		{
			if (amount == 0)
			{
				return;
			}
			if (this.isDead)
			{
				if (!canRepair)
				{
					this.explode();
				}
				return;
			}
			if (amount >= this.health)
			{
				this.health = 0;
			}
			else
			{
				this.health -= amount;
			}
			VehicleManager.sendVehicleHealth(this, this.health);
			if (this.isDead && !canRepair)
			{
				this.explode();
			}
		}

		public void askRepair(ushort amount)
		{
			if (amount == 0 || this.isExploded)
			{
				return;
			}
			if (amount >= this.asset.health - this.health)
			{
				this.health = this.asset.health;
			}
			else
			{
				this.health += amount;
			}
			VehicleManager.sendVehicleHealth(this, this.health);
		}

		private void explode()
		{
			Vector3 force = new Vector3(UnityEngine.Random.Range(this.asset.minExplosionForce.x, this.asset.maxExplosionForce.x), UnityEngine.Random.Range(this.asset.minExplosionForce.y, this.asset.maxExplosionForce.y), UnityEngine.Random.Range(this.asset.minExplosionForce.z, this.asset.maxExplosionForce.z));
			base.GetComponent<Rigidbody>().AddForce(force);
			base.GetComponent<Rigidbody>().AddTorque(16f, 0f, 0f);
			this.dropTrunkItems();
			if (this.asset.isExplosive)
			{
				List<EPlayerKill> list;
				DamageTool.explode(base.transform.position, 8f, EDeathCause.VEHICLE, CSteamID.Nil, 200f, 200f, 200f, 0f, 0f, 500f, 2000f, 500f, out list, EExplosionDamageType.CONVENTIONAL, 32f, true, false, EDamageOrigin.Vehicle_Explosion);
			}
			for (int i = 0; i < this.passengers.Length; i++)
			{
				Passenger passenger = this.passengers[i];
				if (passenger != null)
				{
					SteamPlayer player = passenger.player;
					if (player != null)
					{
						Player player2 = player.player;
						if (!(player2 == null))
						{
							if (!player2.life.isDead)
							{
								if (this.asset.isExplosive)
								{
									EPlayerKill eplayerKill;
									player2.life.askDamage(101, Vector3.up * 101f, EDeathCause.VEHICLE, ELimb.SPINE, CSteamID.Nil, out eplayerKill);
								}
								else
								{
									VehicleManager.forceRemovePlayer(this, player.playerID.steamID);
								}
							}
						}
					}
				}
			}
			int num = UnityEngine.Random.Range(3, 7);
			for (int j = 0; j < num; j++)
			{
				float f = UnityEngine.Random.Range(0f, 6.28318548f);
				ItemManager.dropItem(new Item(67, EItemOrigin.NATURE), base.transform.position + new Vector3(Mathf.Sin(f) * 3f, 1f, Mathf.Cos(f) * 3f), false, Dedicator.isDedicated, true);
			}
			VehicleManager.sendVehicleExploded(this);
			if (this.asset.isExplosive)
			{
				EffectManager.sendEffect(this.asset.explosion, EffectManager.LARGE, base.transform.position);
			}
		}

		public bool checkEnter(CSteamID enemyPlayer, CSteamID enemyGroup)
		{
			return !this.isHooked && ((Provider.isServer && !Dedicator.isDedicated) || !this.isLocked || enemyPlayer == this.lockedOwner || (this.lockedGroup != CSteamID.Nil && enemyGroup == this.lockedGroup));
		}

		public override bool checkUseable()
		{
			return !(Player.player == null) && (base.transform.position - Player.player.transform.position).sqrMagnitude <= 100f && !this.isExploded && this.checkEnter(Provider.client, Player.player.quests.groupID);
		}

		public override void use()
		{
			VehicleManager.enterVehicle(this);
		}

		public override bool checkHighlight(out Color color)
		{
			color = ItemTool.getRarityColorHighlight(this.asset.rarity);
			return true;
		}

		public override bool checkHint(out EPlayerMessage message, out string text, out Color color)
		{
			if (this.checkUseable())
			{
				message = EPlayerMessage.VEHICLE_ENTER;
				text = this.asset.vehicleName;
				color = ItemTool.getRarityColorUI(this.asset.rarity);
			}
			else
			{
				if (Player.player == null || (base.transform.position - Player.player.transform.position).sqrMagnitude > 100f)
				{
					message = EPlayerMessage.BLOCKED;
				}
				else
				{
					message = EPlayerMessage.LOCKED;
				}
				text = string.Empty;
				color = Color.white;
			}
			return !this.isExploded;
		}

		public void updateVehicle()
		{
			this.lastUpdatedPos = base.transform.position;
			if (this.nsb != null)
			{
				this.nsb.updateLastSnapshot(new TransformSnapshotInfo(base.transform.position, base.transform.rotation));
			}
			this.real = base.transform.position;
			this.isRecovering = false;
			this.lastRecover = Time.realtimeSinceStartup;
			this.isFrozen = false;
		}

		public void updatePhysics()
		{
			if (this.checkDriver(Provider.client) || (Provider.isServer && !this.isDriven))
			{
				base.GetComponent<Rigidbody>().useGravity = this.usesGravity;
				base.GetComponent<Rigidbody>().isKinematic = this.isKinematic;
				this.isPhysical = true;
				if (!this.isExploded)
				{
					if (this.tires != null)
					{
						for (int i = 0; i < this.tires.Length; i++)
						{
							this.tires[i].isPhysical = true;
						}
					}
					if (this.buoyancy != null)
					{
						this.buoyancy.gameObject.SetActive(true);
					}
				}
			}
			else
			{
				base.GetComponent<Rigidbody>().useGravity = false;
				base.GetComponent<Rigidbody>().isKinematic = true;
				this.isPhysical = false;
				if (this.tires != null)
				{
					for (int j = 0; j < this.tires.Length; j++)
					{
						this.tires[j].isPhysical = false;
					}
				}
				if (this.buoyancy != null)
				{
					this.buoyancy.gameObject.SetActive(false);
				}
			}
			Transform transform = base.transform.FindChild("Cog");
			if (transform)
			{
				base.GetComponent<Rigidbody>().centerOfMass = transform.localPosition;
			}
			else
			{
				base.GetComponent<Rigidbody>().centerOfMass = new Vector3(0f, -0.25f, 0f);
			}
		}

		public void updateEngine()
		{
			this.tellTaillights(this.isDriven && this.canTurnOnLights);
			if (!Dedicator.isDedicated)
			{
				foreach (GameObject gameObject in this.sirenGameObjects)
				{
					AudioSource component = gameObject.GetComponent<AudioSource>();
					if (component != null)
					{
						component.enabled = this.isDriven;
					}
				}
			}
		}

		public void tellLocked(CSteamID owner, CSteamID group, bool locked)
		{
			this._lockedOwner = owner;
			this._lockedGroup = group;
			this._isLocked = locked;
			if (this.onLockUpdated != null)
			{
				this.onLockUpdated();
			}
		}

		public void tellSkin(ushort newSkinID, ushort newMythicID)
		{
			this.skinID = newSkinID;
			this.mythicID = newMythicID;
			this.updateSkin();
			if (this.skinChanged != null)
			{
				this.skinChanged();
			}
		}

		public void updateSkin()
		{
			if (Dedicator.isDedicated)
			{
				return;
			}
			this.skinAsset = (Assets.find(EAssetType.SKIN, this.skinID) as SkinAsset);
			if (this.tempMesh != null)
			{
				HighlighterTool.remesh(base.transform, this.tempMesh, this.tempMesh, false);
			}
			if (this.tempMaterial != null)
			{
				Material material;
				HighlighterTool.rematerialize(base.transform, this.tempMaterial, out material);
			}
			if (this.effectSystems != null)
			{
				for (int i = 0; i < this.effectSystems.Length; i++)
				{
					Transform transform = this.effectSystems[i];
					if (transform != null)
					{
						UnityEngine.Object.Destroy(transform.gameObject);
					}
				}
			}
			if (this.skinAsset != null)
			{
				VehicleAsset vehicleAsset = Assets.find(EAssetType.VEHICLE, this.asset.sharedSkinLookupID) as VehicleAsset;
				if (this.mythicID != 0)
				{
					if (this.effectSlotsRoot == null)
					{
						this.effectSlotsRoot = base.transform.FindChild("Effect_Slots");
						if (this.effectSlotsRoot == null)
						{
							this.effectSlotsRoot = UnityEngine.Object.Instantiate<GameObject>(vehicleAsset.vehicle.transform.FindChild("Effect_Slots").gameObject).transform;
							this.effectSlotsRoot.parent = base.transform;
							this.effectSlotsRoot.name = "Effect_Slots";
							this.effectSlotsRoot.localPosition = Vector3.zero;
							this.effectSlotsRoot.localRotation = Quaternion.identity;
							this.effectSlotsRoot.localScale = Vector3.one;
						}
						this.effectSlots = new Transform[this.effectSlotsRoot.childCount];
						for (int j = 0; j < this.effectSlots.Length; j++)
						{
							this.effectSlots[j] = this.effectSlotsRoot.GetChild(j);
						}
						this.effectSystems = new Transform[this.effectSlots.Length];
					}
					ItemTool.applyEffect(this.effectSlots, this.effectSystems, this.mythicID, EEffectType.AREA);
				}
				if (this.skinAsset.overrideMeshes != null && this.skinAsset.overrideMeshes.Count > 0)
				{
					if (this.tempMesh == null)
					{
						this.tempMesh = new List<Mesh>();
					}
					HighlighterTool.remesh(base.transform, this.skinAsset.overrideMeshes, this.tempMesh, false);
				}
				if (this.skinAsset.primarySkin != null)
				{
					if (this.skinAsset.isPattern)
					{
						Material material2 = UnityEngine.Object.Instantiate<Material>(this.skinAsset.primarySkin);
						material2.SetTexture("_AlbedoBase", vehicleAsset.albedoBase);
						material2.SetTexture("_MetallicBase", vehicleAsset.metallicBase);
						material2.SetTexture("_EmissionBase", vehicleAsset.emissionBase);
						HighlighterTool.rematerialize(base.transform, material2, out this.tempMaterial);
					}
					else
					{
						HighlighterTool.rematerialize(base.transform, this.skinAsset.primarySkin, out this.tempMaterial);
					}
				}
			}
		}

		public void tellSirens(bool on)
		{
			this._sirensOn = on;
			if (!Dedicator.isDedicated)
			{
				foreach (GameObject gameObject in this.sirenGameObjects)
				{
					gameObject.SetActive(this.sirensOn);
				}
				if (this.sirenMaterials != null)
				{
					for (int i = 0; i < this.sirenMaterials.Length; i++)
					{
						if (this.sirenMaterials[i] != null)
						{
							this.sirenMaterials[i].SetColor("_EmissionColor", Color.black);
						}
					}
				}
			}
			if (this.onSirensUpdated != null)
			{
				this.onSirensUpdated();
			}
		}

		public void tellBlimp(bool on)
		{
			this.isBlimpFloating = on;
			if (this.asset.engine != EEngine.BLIMP)
			{
				return;
			}
			int childCount = this.buoyancy.childCount;
			for (int i = 0; i < childCount; i++)
			{
				this.buoyancy.GetChild(i).GetComponent<Buoyancy>().enabled = this.isBlimpFloating;
			}
			if (this.onBlimpUpdated != null)
			{
				this.onBlimpUpdated();
			}
		}

		public void tellHeadlights(bool on)
		{
			this._headlightsOn = on;
			if (!Dedicator.isDedicated)
			{
				if (this.headlights != null)
				{
					this.headlights.gameObject.SetActive(this.headlightsOn);
				}
				if (this.headlightsMaterial != null)
				{
					this.headlightsMaterial.SetColor("_EmissionColor", (!this.headlightsOn) ? Color.black : this.headlightsMaterial.color);
				}
			}
			if (this.onHeadlightsUpdated != null)
			{
				this.onHeadlightsUpdated();
			}
		}

		public void tellTaillights(bool on)
		{
			this._taillightsOn = on;
			if (!Dedicator.isDedicated)
			{
				if (this.taillights != null)
				{
					this.taillights.gameObject.SetActive(this.taillightsOn);
				}
				if (this.taillightsMaterial != null)
				{
					this.taillightsMaterial.SetColor("_EmissionColor", (!this.taillightsOn) ? Color.black : this.taillightsMaterial.color);
				}
				else if (this.taillightMaterials != null)
				{
					for (int i = 0; i < this.taillightMaterials.Length; i++)
					{
						if (this.taillightMaterials[i] != null)
						{
							this.taillightMaterials[i].SetColor("_EmissionColor", (!this.taillightsOn) ? Color.black : this.taillightMaterials[i].color);
						}
					}
				}
			}
			if (this.onTaillightsUpdated != null)
			{
				this.onTaillightsUpdated();
			}
		}

		public void tellHorn()
		{
			this.horned = Time.realtimeSinceStartup;
			if (!Dedicator.isDedicated && this.clipAudioSource != null)
			{
				this.clipAudioSource.pitch = 1f;
				this.clipAudioSource.PlayOneShot(this.asset.horn);
			}
			if (Provider.isServer)
			{
				AlertTool.alert(base.transform.position, 32f);
			}
		}

		public void tellFuel(ushort newFuel)
		{
			this.fuel = newFuel;
		}

		public void tellBatteryCharge(ushort newBatteryCharge)
		{
			this.batteryCharge = newBatteryCharge;
			if (this.batteryCharge == 0)
			{
				this.isEngineOn = false;
			}
			if (this.batteryChanged != null)
			{
				this.batteryChanged();
			}
		}

		public void tellExploded()
		{
			this.clearHooked();
			this.isExploded = true;
			this._lastExploded = Time.realtimeSinceStartup;
			if (this.sirensOn)
			{
				this.tellSirens(false);
			}
			if (this.isBlimpFloating)
			{
				this.tellBlimp(false);
			}
			if (this.headlightsOn)
			{
				this.tellHeadlights(false);
			}
			if (this.tires != null)
			{
				for (int i = 0; i < this.tires.Length; i++)
				{
					this.tires[i].isPhysical = false;
				}
			}
			if (this.buoyancy != null)
			{
				this.buoyancy.gameObject.SetActive(false);
			}
			if (!Dedicator.isDedicated)
			{
				if (this.asset.isExplosive)
				{
					HighlighterTool.color(base.transform, new Color(0.25f, 0.25f, 0.25f));
				}
				this.updateFires();
				if (this.tires != null)
				{
					for (int j = 0; j < this.tires.Length; j++)
					{
						if (!(this.tires[j].model == null))
						{
							this.tires[j].model.transform.parent = Level.effects;
							this.tires[j].model.GetComponent<Collider>().enabled = true;
							Rigidbody orAddComponent = this.tires[j].model.gameObject.getOrAddComponent<Rigidbody>();
							orAddComponent.interpolation = RigidbodyInterpolation.Interpolate;
							orAddComponent.collisionDetectionMode = CollisionDetectionMode.Discrete;
							orAddComponent.drag = 0.5f;
							orAddComponent.angularDrag = 0.1f;
							UnityEngine.Object.Destroy(this.tires[j].model.gameObject, 8f);
							if (j % 2 == 0)
							{
								orAddComponent.AddForce(-this.tires[j].model.right * 512f + Vector3.up * 128f);
							}
							else
							{
								orAddComponent.AddForce(this.tires[j].model.right * 512f + Vector3.up * 128f);
							}
						}
					}
				}
				if (this.rotors != null)
				{
					for (int k = 0; k < this.rotors.Length; k++)
					{
						UnityEngine.Object.Destroy(this.rotors[k].prop.gameObject);
					}
				}
				if (this.exhausts != null)
				{
					for (int l = 0; l < this.exhausts.Length; l++)
					{
						this.exhausts[l].emission.rateOverTime = 0f;
					}
				}
				if (this.asset.isExplosive)
				{
					if (this.front != null)
					{
						HighlighterTool.color(this.front, new Color(0.25f, 0.25f, 0.25f));
					}
					if (this.turrets != null)
					{
						for (int m = 0; m < this.turrets.Length; m++)
						{
							HighlighterTool.color(this.turrets[m].turretYaw, new Color(0.25f, 0.25f, 0.25f));
							HighlighterTool.color(this.turrets[m].turretPitch, new Color(0.25f, 0.25f, 0.25f));
						}
					}
				}
			}
		}

		public void updateFires()
		{
			if (!Dedicator.isDedicated)
			{
				if (this.fire != null)
				{
					this.fire.gameObject.SetActive((this.isExploded || this.isDead) && !this.isUnderwater);
				}
				if (this.smoke_0 != null)
				{
					this.smoke_0.gameObject.SetActive((this.isExploded || this.health < InteractableVehicle.HEALTH_0) && !this.isUnderwater);
				}
				if (this.smoke_1 != null)
				{
					this.smoke_1.gameObject.SetActive((this.isExploded || this.health < InteractableVehicle.HEALTH_1) && !this.isUnderwater);
				}
			}
		}

		public void tellHealth(ushort newHealth)
		{
			this.health = newHealth;
			if (this.isDead)
			{
				this._lastDead = Time.realtimeSinceStartup;
			}
			this.updateFires();
		}

		public void tellRecov(Vector3 newPosition, int newRecov)
		{
			this.lastTick = Time.realtimeSinceStartup;
			base.GetComponent<Rigidbody>().MovePosition(newPosition);
			this.isFrozen = true;
			base.GetComponent<Rigidbody>().useGravity = false;
			base.GetComponent<Rigidbody>().isKinematic = true;
			if (this.passengers[0] != null && this.passengers[0].player != null && this.passengers[0].player.player != null && this.passengers[0].player.player.input != null)
			{
				this.passengers[0].player.player.input.recov = newRecov;
			}
		}

		public void tellState(Vector3 newPosition, byte newAngle_X, byte newAngle_Y, byte newAngle_Z, byte newSpeed, byte newPhysicsSpeed, byte newTurn)
		{
			if (this.isDriver)
			{
				return;
			}
			this.lastTick = Time.realtimeSinceStartup;
			this.lastUpdatedPos = newPosition;
			if (this.nsb != null)
			{
				this.nsb.addNewSnapshot(new TransformSnapshotInfo(newPosition, Quaternion.Euler(MeasurementTool.byteToAngle2(newAngle_X), MeasurementTool.byteToAngle2(newAngle_Y), MeasurementTool.byteToAngle2(newAngle_Z))));
			}
			if (this.asset.engine == EEngine.TRAIN)
			{
				this.roadPosition = newPosition.x;
			}
			this._speed = (float)(newSpeed - 128);
			this._physicsSpeed = (float)(newPhysicsSpeed - 128);
			this._turn = (int)(newTurn - 1);
		}

		public bool checkDriver(CSteamID steamID)
		{
			return this.isDriven && this.passengers[0].player.playerID.steamID == steamID;
		}

		protected void grantTrunkAccess(Player player)
		{
			if (Provider.isServer && this.trunkItems != null && this.trunkItems.height > 0)
			{
				player.inventory.isStoring = true;
				player.inventory.isStorageTrunk = true;
				player.inventory.storage = null;
				player.inventory.updateItems(PlayerInventory.STORAGE, this.trunkItems);
				player.inventory.sendStorage();
			}
		}

		protected void revokeTrunkAccess(Player player)
		{
			if (Provider.isServer)
			{
				player.inventory.isStoring = false;
				player.inventory.isStorageTrunk = false;
				player.inventory.updateItems(PlayerInventory.STORAGE, null);
				player.inventory.sendStorage();
			}
		}

		protected void dropTrunkItems()
		{
			if (Provider.isServer && this.trunkItems != null)
			{
				for (byte b = 0; b < this.trunkItems.getItemCount(); b += 1)
				{
					ItemJar item = this.trunkItems.getItem(b);
					ItemManager.dropItem(item.item, base.transform.position, false, true, true);
				}
				this.trunkItems.clear();
				this.trunkItems = null;
				if (this.passengers[0].player != null && this.passengers[0].player.player != null)
				{
					this.revokeTrunkAccess(this.passengers[0].player.player);
				}
			}
		}

		public void addPlayer(byte seat, CSteamID steamID)
		{
			SteamPlayer steamPlayer = PlayerTool.getSteamPlayer(steamID);
			if (steamPlayer != null)
			{
				this.passengers[(int)seat].player = steamPlayer;
				if (steamPlayer.player != null)
				{
					steamPlayer.player.movement.setVehicle(this, seat, this.passengers[(int)seat].seat, Vector3.zero, 0, false);
					if (this.passengers[(int)seat].turret != null)
					{
						steamPlayer.player.equipment.turretEquipClient();
						if (Provider.isServer)
						{
							steamPlayer.player.equipment.turretEquipServer(this.passengers[(int)seat].turret.itemID, this.passengers[(int)seat].state);
						}
					}
				}
				this.updatePhysics();
				if (seat == 0)
				{
					this.grantTrunkAccess(steamPlayer.player);
				}
			}
			if (seat == 0)
			{
				this.isEngineOn = ((!this.usesBattery || this.hasBattery) && !this.isUnderwater);
			}
			this.updateEngine();
			if (seat == 0 && (this.asset.isStaminaPowered || this.fuel > 0) && !Dedicator.isDedicated && !this.isUnderwater)
			{
				if (this.clipAudioSource != null && this.isEngineOn)
				{
					this.clipAudioSource.pitch = UnityEngine.Random.Range(0.9f, 1.1f);
					this.clipAudioSource.PlayOneShot(this.asset.ignition);
				}
				if (this.engineAudioSource != null)
				{
					this.engineAudioSource.pitch = this.asset.pitchIdle;
				}
				if (this.engineAdditiveAudioSource != null)
				{
					this.engineAdditiveAudioSource.pitch = this.asset.pitchIdle;
				}
			}
			if (this.onPassengersUpdated != null)
			{
				this.onPassengersUpdated();
			}
		}

		public void removePlayer(byte seat, Vector3 point, byte angle, bool forceUpdate)
		{
			if ((int)seat < this.passengers.Length)
			{
				SteamPlayer player = this.passengers[(int)seat].player;
				if (player.player != null)
				{
					if (this.passengers[(int)seat].turret != null)
					{
						player.player.equipment.turretDequipClient();
						if (Provider.isServer)
						{
							player.player.equipment.turretDequipServer();
						}
					}
					player.player.movement.setVehicle(null, 0, LevelPlayers.models, point, angle, forceUpdate);
				}
				this.passengers[(int)seat].player = null;
				this.updatePhysics();
				if (Provider.isServer)
				{
					VehicleManager.sendVehicleFuel(this, this.fuel);
					VehicleManager.sendVehicleBatteryCharge(this, this.batteryCharge);
				}
				if (seat == 0)
				{
					this.revokeTrunkAccess(player.player);
				}
			}
			if (seat == 0)
			{
				this.isEngineOn = false;
			}
			this.updateEngine();
			if (seat == 0)
			{
				this.altSpeedInput = 0f;
				this.altSpeedOutput = 0f;
				if (!Dedicator.isDedicated)
				{
					if (this.engineAudioSource != null)
					{
						this.engineAudioSource.volume = 0f;
					}
					if (this.engineAdditiveAudioSource != null)
					{
						this.engineAdditiveAudioSource.volume = 0f;
					}
					if (this.windZone != null)
					{
						this.windZone.windMain = 0f;
					}
				}
				if (this.tires != null)
				{
					for (int i = 0; i < this.tires.Length; i++)
					{
						this.tires[i].reset();
					}
				}
			}
			if (this.onPassengersUpdated != null)
			{
				this.onPassengersUpdated();
			}
		}

		public void swapPlayer(byte fromSeat, byte toSeat)
		{
			if ((int)fromSeat < this.passengers.Length && (int)toSeat < this.passengers.Length)
			{
				SteamPlayer player = this.passengers[(int)fromSeat].player;
				if (player.player != null)
				{
					if (this.passengers[(int)fromSeat].turret != null)
					{
						player.player.equipment.turretDequipClient();
						if (Provider.isServer)
						{
							player.player.equipment.turretDequipServer();
						}
					}
					player.player.movement.setVehicle(this, toSeat, this.passengers[(int)toSeat].seat, Vector3.zero, 0, false);
					if (this.passengers[(int)toSeat].turret != null)
					{
						player.player.equipment.turretEquipClient();
						if (Provider.isServer)
						{
							player.player.equipment.turretEquipServer(this.passengers[(int)toSeat].turret.itemID, this.passengers[(int)toSeat].state);
						}
					}
				}
				this.passengers[(int)fromSeat].player = null;
				this.passengers[(int)toSeat].player = player;
				this.updatePhysics();
				if (Provider.isServer)
				{
					VehicleManager.sendVehicleFuel(this, this.fuel);
					VehicleManager.sendVehicleBatteryCharge(this, this.batteryCharge);
				}
				if (fromSeat == 0)
				{
					this.revokeTrunkAccess(player.player);
				}
				if (toSeat == 0)
				{
					this.grantTrunkAccess(player.player);
				}
			}
			if (toSeat == 0)
			{
				this.isEngineOn = ((!this.usesBattery || this.hasBattery) && !this.isUnderwater);
			}
			if (fromSeat == 0)
			{
				this.isEngineOn = false;
			}
			this.updateEngine();
			if (fromSeat == 0)
			{
				this.altSpeedInput = 0f;
				this.altSpeedOutput = 0f;
				if (!Dedicator.isDedicated)
				{
					if (this.engineAudioSource != null)
					{
						this.engineAudioSource.volume = 0f;
					}
					if (this.engineAdditiveAudioSource != null)
					{
						this.engineAdditiveAudioSource.volume = 0f;
					}
					if (this.windZone != null)
					{
						this.windZone.windMain = 0f;
					}
				}
				if (this.tires != null)
				{
					for (int i = 0; i < this.tires.Length; i++)
					{
						this.tires[i].reset();
					}
				}
			}
			if (this.onPassengersUpdated != null)
			{
				this.onPassengersUpdated();
			}
		}

		public bool tryAddPlayer(out byte seat, Player player)
		{
			seat = byte.MaxValue;
			if (this.isExploded)
			{
				return false;
			}
			if (!this.isExitable)
			{
				return false;
			}
			byte b = (player.animator.gesture != EPlayerGesture.ARREST_START) ? 0 : 1;
			while ((int)b < this.passengers.Length)
			{
				if (this.passengers[(int)b] != null && this.passengers[(int)b].player == null)
				{
					seat = b;
					return true;
				}
				b += 1;
			}
			return false;
		}

		public void forceRemoveAllPlayers()
		{
			for (int i = 0; i < this.passengers.Length; i++)
			{
				Passenger passenger = this.passengers[i];
				if (passenger != null)
				{
					SteamPlayer player = passenger.player;
					if (player != null)
					{
						Player player2 = player.player;
						if (!(player2 == null))
						{
							if (!player2.life.isDead)
							{
								VehicleManager.forceRemovePlayer(this, player.playerID.steamID);
							}
						}
					}
				}
			}
		}

		public bool forceRemovePlayer(out byte seat, CSteamID player, out Vector3 point, out byte angle)
		{
			seat = byte.MaxValue;
			point = Vector3.zero;
			angle = 0;
			byte b = 0;
			while ((int)b < this.passengers.Length)
			{
				if (this.passengers[(int)b] != null && this.passengers[(int)b].player != null && this.passengers[(int)b].player.playerID.steamID == player)
				{
					seat = b;
					this.getExit(seat, out point, out angle);
					return true;
				}
				b += 1;
			}
			return false;
		}

		public bool tryRemovePlayer(out byte seat, CSteamID player, out Vector3 point, out byte angle)
		{
			seat = byte.MaxValue;
			point = Vector3.zero;
			angle = 0;
			byte b = 0;
			while ((int)b < this.passengers.Length)
			{
				if (this.passengers[(int)b] != null && this.passengers[(int)b].player != null && this.passengers[(int)b].player.playerID.steamID == player)
				{
					seat = b;
					return this.getExit(seat, out point, out angle);
				}
				b += 1;
			}
			return false;
		}

		public bool trySwapPlayer(Player player, byte toSeat, out byte fromSeat)
		{
			fromSeat = byte.MaxValue;
			if ((int)toSeat >= this.passengers.Length)
			{
				return false;
			}
			if (player.animator.gesture == EPlayerGesture.ARREST_START && toSeat < 1)
			{
				return false;
			}
			byte b = 0;
			while ((int)b < this.passengers.Length)
			{
				if (this.passengers[(int)b] != null && this.passengers[(int)b].player != null && this.passengers[(int)b].player.player == player)
				{
					if (toSeat != b)
					{
						fromSeat = b;
						return this.passengers[(int)toSeat].player == null;
					}
					return false;
				}
				else
				{
					b += 1;
				}
			}
			return false;
		}

		public bool isExitable
		{
			get
			{
				Vector3 vector;
				byte b;
				return this.getExit(0, out vector, out b);
			}
		}

		protected bool isExitSafe(Vector3 point, Vector3 direction, float distance)
		{
			RaycastHit raycastHit;
			PhysicsUtility.raycast(new Ray(point, direction), out raycastHit, distance, RayMasks.BLOCK_EXIT, QueryTriggerInteraction.UseGlobal);
			return raycastHit.transform == null || (raycastHit.transform.IsChildOf(base.transform) && this.isExitSafe(raycastHit.point + direction * 0.01f, direction, distance - raycastHit.distance - 0.01f));
		}

		protected Vector3 getExitGroundPoint(Vector3 exitPoint, float length = 3f)
		{
			RaycastHit raycastHit;
			PhysicsUtility.raycast(new Ray(exitPoint, Vector3.down), out raycastHit, length, RayMasks.BLOCK_EXIT, QueryTriggerInteraction.UseGlobal);
			if (raycastHit.transform != null)
			{
				exitPoint = raycastHit.point;
			}
			return exitPoint + new Vector3(0f, 0.5f, 0f);
		}

		protected bool getExitSidePoint(float side, ref Vector3 point)
		{
			float num = PlayerStance.RADIUS + 0.1f;
			float num2 = this.asset.exit + Mathf.Abs(this.speed) * 0.1f + num;
			if (this.isExitSafe(this.center.position + this.center.up, this.center.right * side, num2))
			{
				point = this.getExitGroundPoint(this.center.position + this.center.up + this.center.right * side * (num2 - num), 3f);
				return true;
			}
			side = -side;
			if (this.isExitSafe(this.center.position + this.center.up, this.center.right * side, num2))
			{
				point = this.getExitGroundPoint(this.center.position + this.center.up + this.center.right * side * (num2 - num), 3f);
				return true;
			}
			return false;
		}

		public bool getExit(byte seat, out Vector3 point, out byte angle)
		{
			point = this.center.position;
			angle = MeasurementTool.angleToByte(this.center.rotation.eulerAngles.y);
			if (seat % 2 == 0)
			{
				if (this.getExitSidePoint(-1f, ref point))
				{
					return true;
				}
			}
			else if (this.getExitSidePoint(1f, ref point))
			{
				return true;
			}
			if (this.isExitSafe(this.center.position + this.center.up, Vector3.up, 3f))
			{
				point = this.center.position + this.center.up;
				return true;
			}
			point = this.getExitGroundPoint(this.center.position, 1024f);
			return true;
		}

		public void simulate(uint simulation, int recov, bool inputStamina, Vector3 point, Quaternion angle, float newSpeed, float newPhysicsSpeed, int newTurn, float delta)
		{
			if (this.asset.useStaminaBoost)
			{
				bool flag = this.passengers[0].player != null && this.passengers[0].player.player != null && this.passengers[0].player.player.life.stamina > 0;
				if (inputStamina && flag)
				{
					this.isBoosting = true;
				}
				else
				{
					this.isBoosting = false;
				}
			}
			else
			{
				this.isBoosting = false;
			}
			if (this.isRecovering)
			{
				if (recov < this.passengers[0].player.player.input.recov)
				{
					if (Time.realtimeSinceStartup - this.lastRecover > 5f)
					{
						this.lastRecover = Time.realtimeSinceStartup;
						VehicleManager.sendVehicleRecov(this, this.real, this.passengers[0].player.player.input.recov);
					}
					return;
				}
				this.isRecovering = false;
				this.isFrozen = false;
			}
			bool flag2 = Dedicator.serverVisibility == ESteamServerVisibility.LAN || PlayerMovement.forceTrustClient;
			if (!flag2)
			{
				if (this.asset.engine == EEngine.CAR)
				{
					if (Mathf.Pow(point.x - this.real.x, 2f) + Mathf.Pow(point.z - this.real.z, 2f) > ((!this.usesFuel || this.fuel != 0) ? this.asset.sqrDelta : 0.5f))
					{
						this.isRecovering = true;
						this.lastRecover = Time.realtimeSinceStartup;
						this.passengers[0].player.player.input.recov++;
						VehicleManager.sendVehicleRecov(this, this.real, this.passengers[0].player.player.input.recov);
						return;
					}
					if (point.y - this.real.y > 1f)
					{
						this.isRecovering = true;
						this.lastRecover = Time.realtimeSinceStartup;
						this.passengers[0].player.player.input.recov++;
						VehicleManager.sendVehicleRecov(this, this.real, this.passengers[0].player.player.input.recov);
						return;
					}
				}
				else if (this.asset.engine == EEngine.BOAT)
				{
					if (Mathf.Pow(point.x - this.real.x, 2f) + Mathf.Pow(point.z - this.real.z, 2f) > ((!WaterUtility.isPointUnderwater(point + new Vector3(0f, -4f, 0f))) ? 0.5f : this.asset.sqrDelta))
					{
						this.isRecovering = true;
						this.lastRecover = Time.realtimeSinceStartup;
						this.passengers[0].player.player.input.recov++;
						VehicleManager.sendVehicleRecov(this, this.real, this.passengers[0].player.player.input.recov);
						return;
					}
					if (point.y - this.real.y > 0.25f)
					{
						this.isRecovering = true;
						this.lastRecover = Time.realtimeSinceStartup;
						this.passengers[0].player.player.input.recov++;
						VehicleManager.sendVehicleRecov(this, this.real, this.passengers[0].player.player.input.recov);
						return;
					}
				}
				else if (this.asset.engine != EEngine.TRAIN)
				{
					if (Mathf.Pow(point.x - this.real.x, 2f) + Mathf.Pow(point.z - this.real.z, 2f) > this.asset.sqrDelta)
					{
						this.isRecovering = true;
						this.lastRecover = Time.realtimeSinceStartup;
						this.passengers[0].player.player.input.recov++;
						VehicleManager.sendVehicleRecov(this, this.real, this.passengers[0].player.player.input.recov);
						return;
					}
				}
			}
			if (this.usesFuel)
			{
				if (this.asset.engine == EEngine.CAR)
				{
					if (simulation - this.lastBurnFuel > 5u)
					{
						this.lastBurnFuel = simulation;
						this.askBurnFuel(1);
					}
				}
				else if (simulation - this.lastBurnFuel > 2u)
				{
					this.lastBurnFuel = simulation;
					this.askBurnFuel(1);
				}
			}
			this._speed = newSpeed;
			this._physicsSpeed = newSpeed;
			this._turn = newTurn;
			this.real = point;
			Vector3 pos;
			if (this.asset.engine == EEngine.TRAIN)
			{
				this.roadPosition = this.clampRoadPosition(point.x);
				this.teleportTrain();
				pos = new Vector3(this.roadPosition, 0f, 0f);
			}
			else
			{
				base.GetComponent<Rigidbody>().MovePosition(point);
				base.GetComponent<Rigidbody>().MoveRotation(angle);
				pos = point;
			}
			if (this.updates != null && (Mathf.Abs(this.lastUpdatedPos.x - this.real.x) > Provider.UPDATE_DISTANCE || Mathf.Abs(this.lastUpdatedPos.y - this.real.y) > Provider.UPDATE_DISTANCE || Mathf.Abs(this.lastUpdatedPos.z - this.real.z) > Provider.UPDATE_DISTANCE))
			{
				this.lastUpdatedPos = this.real;
				this.updates.Add(new VehicleStateUpdate(pos, angle));
			}
		}

		public void clearHooked()
		{
			foreach (HookInfo hookInfo in this.hooked)
			{
				if (!(hookInfo.vehicle == null))
				{
					hookInfo.vehicle.isHooked = false;
				}
			}
			this.hooked.Clear();
		}

		public void useHook()
		{
			if (this.hooked.Count > 0)
			{
				this.clearHooked();
			}
			else
			{
				int num = Physics.OverlapSphereNonAlloc(this.hook.position, 3f, InteractableVehicle.grab, RayMasks.VEHICLE);
				for (int i = 0; i < num; i++)
				{
					InteractableVehicle vehicle = DamageTool.getVehicle(InteractableVehicle.grab[i].transform);
					if (!(vehicle == null) && !(vehicle == this) && vehicle.isEmpty && !vehicle.isHooked && vehicle.asset.engine != EEngine.TRAIN)
					{
						HookInfo hookInfo = new HookInfo();
						hookInfo.target = vehicle.transform;
						hookInfo.vehicle = vehicle;
						hookInfo.deltaPosition = this.hook.InverseTransformPoint(vehicle.transform.position);
						hookInfo.deltaRotation = Quaternion.FromToRotation(this.hook.forward, vehicle.transform.forward);
						this.hooked.Add(hookInfo);
						vehicle.isHooked = true;
					}
				}
			}
		}

		public void simulate(uint simulation, int recov, int input_x, int input_y, float look_x, float look_y, bool inputBrake, bool inputStamina, float delta)
		{
			float num = (float)input_y;
			if (this.asset.useStaminaBoost)
			{
				bool flag = this.passengers[0].player != null && this.passengers[0].player.player != null && this.passengers[0].player.player.life.stamina > 0;
				if (inputStamina && flag)
				{
					this.isBoosting = true;
				}
				else
				{
					this.isBoosting = false;
					num *= this.asset.staminaBoost;
				}
			}
			else
			{
				this.isBoosting = false;
			}
			if (this.isFrozen)
			{
				this.isFrozen = false;
				base.GetComponent<Rigidbody>().useGravity = this.usesGravity;
				base.GetComponent<Rigidbody>().isKinematic = this.isKinematic;
				return;
			}
			if ((this.usesFuel && this.fuel == 0) || this.isUnderwater || this.isDead || !this.isEngineOn)
			{
				num = 0f;
			}
			this._factor = Mathf.InverseLerp(0f, (this.speed >= 0f) ? this.asset.speedMax : this.asset.speedMin, this.speed);
			bool flag2 = false;
			if (this.tires != null)
			{
				for (int i = 0; i < this.tires.Length; i++)
				{
					this.tires[i].simulate((float)input_x, num, inputBrake, delta);
					if (this.tires[i].isGrounded)
					{
						flag2 = true;
					}
				}
			}
			switch (this.asset.engine)
			{
			case EEngine.CAR:
				if (flag2)
				{
					base.GetComponent<Rigidbody>().AddForce(-base.transform.up * this.factor * 40f);
				}
				if (this.buoyancy != null)
				{
					float num2 = Mathf.Lerp(this.asset.steerMax, this.asset.steerMin, this.factor);
					this.speedTraction = Mathf.Lerp(this.speedTraction, (float)((!WaterUtility.isPointUnderwater(base.transform.position + new Vector3(0f, -1f, 0f))) ? 0 : 1), 4f * Time.deltaTime);
					if (num > 0f)
					{
						this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMax, delta / 4f);
					}
					else if (num < 0f)
					{
						this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMin, delta / 4f);
					}
					else
					{
						this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 8f);
					}
					this.altSpeedOutput = this.altSpeedInput * this.speedTraction;
					Vector3 forward = base.transform.forward;
					forward.y = 0f;
					base.GetComponent<Rigidbody>().AddForce(forward.normalized * this.altSpeedOutput * 2f * this.speedTraction);
					base.GetComponent<Rigidbody>().AddRelativeTorque((float)input_y * -2.5f * this.speedTraction, (float)input_x * num2 / 8f * this.speedTraction, (float)input_x * -2.5f * this.speedTraction);
				}
				break;
			case EEngine.PLANE:
			{
				float num3 = Mathf.Lerp(this.asset.airSteerMax, this.asset.airSteerMin, this.factor);
				if (num > 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMax, delta);
				}
				else if (num < 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 8f);
				}
				else
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 16f);
				}
				this.altSpeedOutput = this.altSpeedInput;
				base.GetComponent<Rigidbody>().AddForce(base.transform.forward * this.altSpeedOutput * 2f);
				base.GetComponent<Rigidbody>().AddForce(Mathf.Lerp(0f, 1f, base.transform.InverseTransformDirection(base.GetComponent<Rigidbody>().velocity).z / this.asset.speedMax) * this.asset.lift * -Physics.gravity);
				if (this.tires == null || this.tires.Length == 0 || (!this.tires[0].isGrounded && !this.tires[1].isGrounded))
				{
					base.GetComponent<Rigidbody>().AddRelativeTorque(Mathf.Clamp(look_y, -this.asset.airTurnResponsiveness, this.asset.airTurnResponsiveness) * num3, (float)input_x * this.asset.airTurnResponsiveness * num3 / 4f, Mathf.Clamp(look_x, -this.asset.airTurnResponsiveness, this.asset.airTurnResponsiveness) * -num3 / 2f);
				}
				if ((this.tires == null || this.tires.Length == 0) && num < 0f)
				{
					base.GetComponent<Rigidbody>().AddForce(base.transform.forward * this.asset.speedMin * 4f);
				}
				break;
			}
			case EEngine.HELICOPTER:
			{
				float num4 = Mathf.Lerp(this.asset.steerMax, this.asset.steerMin, this.factor);
				if (num > 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMax, delta / 4f);
				}
				else if (num < 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 8f);
				}
				else
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 16f);
				}
				this.altSpeedOutput = this.altSpeedInput;
				base.GetComponent<Rigidbody>().AddForce(base.transform.up * this.altSpeedOutput * 3f);
				base.GetComponent<Rigidbody>().AddRelativeTorque(Mathf.Clamp(look_y, -2f, 2f) * num4, (float)input_x * num4 / 2f, Mathf.Clamp(look_x, -2f, 2f) * -num4 / 4f);
				break;
			}
			case EEngine.BLIMP:
			{
				float num5 = Mathf.Lerp(this.asset.steerMax, this.asset.steerMin, this.factor);
				if (num > 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMax, delta / 4f);
				}
				else if (num < 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMin, delta / 4f);
				}
				else
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 8f);
				}
				this.altSpeedOutput = this.altSpeedInput;
				base.GetComponent<Rigidbody>().AddForce(base.transform.forward * this.altSpeedOutput * 2f);
				if (!this.isBlimpFloating)
				{
					base.GetComponent<Rigidbody>().AddForce(-Physics.gravity * 0.5f);
				}
				base.GetComponent<Rigidbody>().AddRelativeTorque(Mathf.Clamp(look_y, -this.asset.airTurnResponsiveness, this.asset.airTurnResponsiveness) * num5 / 4f, (float)input_x * this.asset.airTurnResponsiveness * num5 * 2f, Mathf.Clamp(look_x, -this.asset.airTurnResponsiveness, this.asset.airTurnResponsiveness) * -num5 / 4f);
				break;
			}
			case EEngine.BOAT:
			{
				float num6 = Mathf.Lerp(this.asset.steerMax, this.asset.steerMin, this.factor);
				this.speedTraction = Mathf.Lerp(this.speedTraction, (float)((!WaterUtility.isPointUnderwater(base.transform.position + new Vector3(0f, -1f, 0f))) ? 0 : 1), 4f * Time.deltaTime);
				if (num > 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMax, delta / 4f);
				}
				else if (num < 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMin, delta / 4f);
				}
				else
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 8f);
				}
				this.altSpeedOutput = this.altSpeedInput * this.speedTraction;
				Vector3 forward2 = base.transform.forward;
				forward2.y = 0f;
				base.GetComponent<Rigidbody>().AddForce(forward2.normalized * this.altSpeedOutput * 4f * this.speedTraction);
				if (this.tires == null || this.tires.Length == 0 || (!this.tires[0].isGrounded && !this.tires[1].isGrounded))
				{
					base.GetComponent<Rigidbody>().AddRelativeTorque(num * -10f * this.speedTraction, (float)input_x * num6 / 2f * this.speedTraction, (float)input_x * -5f * this.speedTraction);
				}
				break;
			}
			case EEngine.TRAIN:
				if (num > 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMax, delta / 8f);
				}
				else if (num < 0f)
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, this.asset.speedMin, delta / 8f);
				}
				else
				{
					this.altSpeedInput = Mathf.Lerp(this.altSpeedInput, 0f, delta / 8f);
				}
				this.altSpeedOutput = this.altSpeedInput;
				break;
			}
			if (this.asset.engine == EEngine.CAR)
			{
				this._speed = base.transform.InverseTransformDirection(base.GetComponent<Rigidbody>().velocity).z;
				this._physicsSpeed = this._speed;
			}
			else if (this.asset.engine == EEngine.TRAIN)
			{
				this._speed = this.altSpeedOutput;
				this._physicsSpeed = this.altSpeedOutput;
			}
			else
			{
				this._speed = this.altSpeedOutput;
				this._physicsSpeed = base.transform.InverseTransformDirection(base.GetComponent<Rigidbody>().velocity).z;
			}
			this._turn = input_x;
			if (this.usesFuel)
			{
				if (this.asset.engine == EEngine.CAR)
				{
					if (simulation - this.lastBurnFuel > 5u)
					{
						this.lastBurnFuel = simulation;
						this.askBurnFuel(1);
					}
				}
				else if (simulation - this.lastBurnFuel > 2u)
				{
					this.lastBurnFuel = simulation;
					this.askBurnFuel(1);
				}
			}
			this.lastUpdatedPos = base.transform.position;
			if (this.nsb != null)
			{
				this.nsb.updateLastSnapshot(new TransformSnapshotInfo(base.transform.position, base.transform.rotation));
			}
		}

		private void moveTrain(Vector3 frontPosition, Vector3 frontNormal, Vector3 frontDirection, Vector3 backPosition, Vector3 backNormal, Vector3 backDirection, TrainCar car)
		{
			Vector3 a = (frontPosition + backPosition) / 2f;
			Vector3 vector = Vector3.Lerp(backNormal, frontNormal, 0.5f);
			Vector3 normalized = (frontPosition - backPosition).normalized;
			Quaternion rotation = Quaternion.LookRotation(frontDirection, frontNormal);
			Quaternion rotation2 = Quaternion.LookRotation(backDirection, backNormal);
			Quaternion quaternion = Quaternion.LookRotation(normalized, vector);
			car.root.GetComponent<Rigidbody>().MovePosition(a + vector * this.asset.trainTrackOffset);
			car.root.GetComponent<Rigidbody>().MoveRotation(quaternion);
			car.root.position = a + vector * this.asset.trainTrackOffset;
			car.root.rotation = quaternion;
			car.trackFront.position = a + normalized * this.asset.trainWheelOffset;
			car.trackFront.rotation = rotation;
			car.trackBack.position = a - normalized * this.asset.trainWheelOffset;
			car.trackBack.rotation = rotation2;
		}

		private void teleportTrain()
		{
			foreach (TrainCar trainCar in this.trainCars)
			{
				Vector3 frontPosition;
				Vector3 frontNormal;
				Vector3 frontDirection;
				this.road.getTrackData(this.clampRoadPosition(this.roadPosition + trainCar.trackPositionOffset + this.asset.trainWheelOffset), out frontPosition, out frontNormal, out frontDirection);
				Vector3 backPosition;
				Vector3 backNormal;
				Vector3 backDirection;
				this.road.getTrackData(this.clampRoadPosition(this.roadPosition + trainCar.trackPositionOffset - this.asset.trainWheelOffset), out backPosition, out backNormal, out backDirection);
				this.moveTrain(frontPosition, frontNormal, frontDirection, backPosition, backNormal, backDirection, trainCar);
			}
		}

		private void linkTrain()
		{
			Vector3 a = this.trainCars[0].root.position + this.trainCars[0].root.forward * -this.asset.trainCarLength / 2f;
			for (int i = 1; i < this.trainCars.Length; i++)
			{
				Vector3 b = this.trainCars[i].root.position + this.trainCars[i].root.forward * this.asset.trainCarLength / 2f;
				Vector3 a2 = a - b;
				float sqrMagnitude = a2.sqrMagnitude;
				if (sqrMagnitude > 1f)
				{
					float d = Mathf.Clamp01((sqrMagnitude - 1f) / 8f);
					this.trainCars[i].root.position += a2 * d;
				}
				a = this.trainCars[i].root.position + this.trainCars[i].root.forward * -this.asset.trainCarLength / 2f;
			}
		}

		private TrainCar getTrainCar(Transform root)
		{
			Transform trackFront = root.FindChild("Objects").FindChild("Track_Front");
			Transform trackBack = root.FindChild("Objects").FindChild("Track_Back");
			return new TrainCar
			{
				root = root,
				trackFront = trackFront,
				trackBack = trackBack
			};
		}

		private float clampRoadPosition(float newRoadPosition)
		{
			if (!this.road.isLoop)
			{
				return Mathf.Clamp(newRoadPosition, 0.5f + this.asset.trainWheelOffset, this.road.trackSampledLength - (float)(this.trainCars.Length - 1) * this.asset.trainCarLength - this.asset.trainWheelOffset - 0.5f);
			}
			if (newRoadPosition < 0f)
			{
				return this.road.trackSampledLength + newRoadPosition;
			}
			if (newRoadPosition > this.road.trackSampledLength)
			{
				return newRoadPosition - this.road.trackSampledLength;
			}
			return newRoadPosition;
		}

		private void Update()
		{
			if (this.asset == null)
			{
				return;
			}
			float deltaTime = Time.deltaTime;
			if (Provider.isServer && this.hooked != null)
			{
				for (int i = 0; i < this.hooked.Count; i++)
				{
					HookInfo hookInfo = this.hooked[i];
					if (hookInfo != null && !(hookInfo.target == null))
					{
						hookInfo.target.position = this.hook.TransformPoint(hookInfo.deltaPosition);
						hookInfo.target.rotation = this.hook.rotation * hookInfo.deltaRotation;
					}
				}
			}
			if (Dedicator.isDedicated)
			{
				if (this.isPhysical && this.updates != null && this.updates.Count == 0 && (Mathf.Abs(this.lastUpdatedPos.x - base.transform.position.x) > Provider.UPDATE_DISTANCE || Mathf.Abs(this.lastUpdatedPos.y - base.transform.position.y) > Provider.UPDATE_DISTANCE || Mathf.Abs(this.lastUpdatedPos.z - base.transform.position.z) > Provider.UPDATE_DISTANCE))
				{
					this.lastUpdatedPos = base.transform.position;
					Vector3 position;
					if (this.asset.engine == EEngine.TRAIN)
					{
						position = new Vector3(this.roadPosition, 0f, 0f);
					}
					else
					{
						position = base.transform.position;
					}
					this.updates.Add(new VehicleStateUpdate(position, base.transform.rotation));
				}
			}
			else
			{
				this._steer = Mathf.Lerp(this.steer, (float)this.turn * this.asset.steerMax, 4f * deltaTime);
				this._spedometer = Mathf.Lerp(this.spedometer, this.speed, 4f * deltaTime);
				if (!this.isExploded)
				{
					if (this.isDriven)
					{
						this.fly += (this.spedometer + 8f) * 89f * Time.deltaTime;
					}
					this.spin += this.spedometer * 45f * Time.deltaTime;
					if (this.tires != null)
					{
						for (int j = 0; j < this.tires.Length; j++)
						{
							if (!(this.tires[j].model == null))
							{
								this.tires[j].model.localRotation = this.tires[j].rest;
								if (j < ((this.asset.engine != EEngine.CAR) ? 1 : 2) && !this.asset.hasCrawler)
								{
									this.tires[j].model.Rotate(0f, this.steer, 0f, Space.Self);
								}
								this.tires[j].model.Rotate(this.spin, 0f, 0f, Space.Self);
							}
						}
					}
					if (this.front != null)
					{
						this.front.localRotation = this.restFront;
						this.front.transform.Rotate(0f, 0f, this.steer, Space.Self);
					}
					if (this.rotors != null)
					{
						for (int k = 0; k < this.rotors.Length; k++)
						{
							Rotor rotor = this.rotors[k];
							if (rotor == null || rotor.prop == null || rotor.material_0 == null || rotor.material_1 == null)
							{
								break;
							}
							rotor.prop.localRotation = rotor.rest * Quaternion.Euler(0f, this.fly, 0f);
							Color color = rotor.material_0.color;
							if (this.asset.engine == EEngine.PLANE)
							{
								color.a = Mathf.Lerp(1f, 0f, (this.spedometer - 16f) / 8f);
							}
							else
							{
								color.a = Mathf.Lerp(1f, 0f, (this.spedometer - 8f) / 8f);
							}
							rotor.material_0.color = color;
							color.a = (1f - color.a) * 0.25f;
							rotor.material_1.color = color;
						}
					}
					float num = Mathf.Max(0f, Mathf.InverseLerp(0f, this.asset.speedMax, this.physicsSpeed));
					if (this.exhausts != null)
					{
						for (int l = 0; l < this.exhausts.Length; l++)
						{
							this.exhausts[l].emission.rateOverTime = (float)this.exhausts[l].main.maxParticles * num;
						}
					}
					if (this.wheel != null)
					{
						this.wheel.transform.localRotation = this.restWheel;
						this.wheel.transform.Rotate(0f, -this.steer, 0f, Space.Self);
					}
					if (this.pedalLeft != null && this.pedalRight != null && this.passengers[0].player != null && this.passengers[0].player.player != null)
					{
						Transform thirdSkeleton = this.passengers[0].player.player.animator.thirdSkeleton;
						Transform transform = thirdSkeleton.FindChild("Left_Hip").FindChild("Left_Leg").FindChild("Left_Foot");
						Transform transform2 = thirdSkeleton.FindChild("Right_Hip").FindChild("Right_Leg").FindChild("Right_Foot");
						if (this.passengers[0].player.hand)
						{
							this.pedalLeft.position = transform2.position + transform2.right * 0.325f;
							this.pedalRight.position = transform.position + transform.right * 0.325f;
						}
						else
						{
							this.pedalLeft.position = transform.position + transform.right * -0.325f;
							this.pedalRight.position = transform2.position + transform2.right * -0.325f;
						}
					}
				}
				if (this.isDriven && !this.isUnderwater)
				{
					if (this.asset.engine == EEngine.HELICOPTER)
					{
						if (this.engineAudioSource != null)
						{
							this.engineAudioSource.pitch = Mathf.Lerp(this.engineAudioSource.pitch, 0.5f + Mathf.Abs(this.spedometer) * 0.03f, 2f * deltaTime);
							this.engineAudioSource.volume = Mathf.Lerp(this.engineAudioSource.volume, (!this.asset.isStaminaPowered && (this.fuel <= 0 || !this.isEngineOn)) ? 0f : (0.25f + Mathf.Abs(this.spedometer) * 0.03f), 0.125f * deltaTime);
						}
						if (this.windZone != null)
						{
							this.windZone.windMain = Mathf.Lerp(this.windZone.windMain, (!this.asset.isStaminaPowered && this.fuel <= 0) ? 0f : (Mathf.Abs(this.spedometer) * 0.1f), 0.125f * deltaTime);
						}
					}
					else if (this.asset.engine == EEngine.BLIMP)
					{
						if (this.engineAudioSource != null)
						{
							this.engineAudioSource.pitch = Mathf.Lerp(this.engineAudioSource.pitch, 0.5f + Mathf.Abs(this.spedometer) * 0.1f, 2f * deltaTime);
							this.engineAudioSource.volume = Mathf.Lerp(this.engineAudioSource.volume, (!this.asset.isStaminaPowered && (this.fuel <= 0 || !this.isEngineOn)) ? 0f : (0.25f + Mathf.Abs(this.spedometer) * 0.1f), 0.125f * deltaTime);
						}
						if (this.windZone != null)
						{
							this.windZone.windMain = Mathf.Lerp(this.windZone.windMain, (!this.asset.isStaminaPowered && this.fuel <= 0) ? 0f : (Mathf.Abs(this.spedometer) * 0.5f), 0.125f * deltaTime);
						}
					}
					else
					{
						if (this.engineAudioSource != null)
						{
							this.engineAudioSource.pitch = Mathf.Lerp(this.engineAudioSource.pitch, this.asset.pitchIdle + Mathf.Abs(this.spedometer) * this.asset.pitchDrive, 2f * deltaTime);
							this.engineAudioSource.volume = Mathf.Lerp(this.engineAudioSource.volume, (!this.asset.isStaminaPowered && (this.fuel <= 0 || !this.isEngineOn)) ? 0f : 0.75f, 2f * deltaTime);
						}
						if (this.engineAdditiveAudioSource != null)
						{
							this.engineAdditiveAudioSource.pitch = Mathf.Lerp(this.engineAdditiveAudioSource.pitch, this.asset.pitchIdle + Mathf.Abs(this.spedometer) * this.asset.pitchDrive, 2f * deltaTime);
							this.engineAdditiveAudioSource.volume = Mathf.Lerp(this.engineAdditiveAudioSource.volume, Mathf.Lerp(0f, 0.75f, Mathf.Abs(this.spedometer) / 8f), 2f * deltaTime);
						}
					}
				}
			}
			if (!Provider.isServer && !this.isPhysical && this.asset.engine != EEngine.TRAIN && this.nsb != null)
			{
				TransformSnapshotInfo transformSnapshotInfo = (TransformSnapshotInfo)this.nsb.getCurrentSnapshot();
				base.GetComponent<Rigidbody>().MovePosition(transformSnapshotInfo.pos);
				base.GetComponent<Rigidbody>().MoveRotation(transformSnapshotInfo.rot);
			}
			if (this.headlightsOn && !this.canTurnOnLights)
			{
				this.tellHeadlights(false);
			}
			if (this.taillightsOn && !this.canTurnOnLights)
			{
				this.tellTaillights(false);
			}
			if (this.sirensOn && !this.canTurnOnLights)
			{
				this.tellSirens(false);
			}
			if (this.isUnderwater)
			{
				if (!this.isDrowned)
				{
					this._lastUnderwater = Time.realtimeSinceStartup;
					this._isDrowned = true;
					this.tellSirens(false);
					this.tellBlimp(false);
					this.tellHeadlights(false);
					this.updateFires();
					if (!Dedicator.isDedicated)
					{
						if (this.engineAudioSource != null)
						{
							this.engineAudioSource.volume = 0f;
						}
						if (this.engineAdditiveAudioSource != null)
						{
							this.engineAdditiveAudioSource.volume = 0f;
						}
						if (this.windZone != null)
						{
							this.windZone.windMain = 0f;
						}
					}
				}
			}
			else if (this._isDrowned)
			{
				this._isDrowned = false;
				this.updateFires();
			}
			if (this.isDriver)
			{
				if (!this.asset.hasTraction)
				{
					bool flag = LevelLighting.isPositionSnowy(base.transform.position);
					AmbianceVolume ambianceVolume;
					if (!flag && Level.info.configData.Use_Snow_Volumes && AmbianceUtility.isPointInsideVolume(base.transform.position, out ambianceVolume))
					{
						flag = ambianceVolume.canSnow;
					}
					flag &= (LevelLighting.snowyness == ELightingSnow.BLIZZARD);
					this._slip = Mathf.Lerp(this._slip, (float)((!flag) ? 0 : 1), Time.deltaTime * 0.05f);
				}
				else
				{
					this._slip = 0f;
				}
				if (this.tires != null)
				{
					for (int m = 0; m < this.tires.Length; m++)
					{
						if (this.tires[m] == null)
						{
							break;
						}
						this.tires[m].update(deltaTime);
					}
				}
				if (this.asset.engine == EEngine.TRAIN && this.road != null)
				{
					foreach (TrainCar trainCar in this.trainCars)
					{
						Vector3 frontPosition;
						Vector3 frontNormal;
						Vector3 frontDirection;
						this.road.getTrackData(this.clampRoadPosition(this.roadPosition + trainCar.trackPositionOffset + this.asset.trainWheelOffset), out frontPosition, out frontNormal, out frontDirection);
						Vector3 backPosition;
						Vector3 backNormal;
						Vector3 backDirection;
						this.road.getTrackData(this.clampRoadPosition(this.roadPosition + trainCar.trackPositionOffset - this.asset.trainWheelOffset), out backPosition, out backNormal, out backDirection);
						this.moveTrain(frontPosition, frontNormal, frontDirection, backPosition, backNormal, backDirection, trainCar);
					}
					float num2 = this.altSpeedOutput * deltaTime;
					Transform transform3;
					if (this.altSpeedOutput > 0f)
					{
						transform3 = this.overlapFront;
					}
					else
					{
						transform3 = this.overlapBack;
					}
					Vector3 vector = transform3.position + transform3.forward * num2 / 2f;
					Vector3 size = transform3.GetComponent<BoxCollider>().size;
					size.z = num2;
					bool flag2 = false;
					int num3 = Physics.OverlapBoxNonAlloc(vector, size / 2f, InteractableVehicle.grab, transform3.rotation, RayMasks.BLOCK_TRAIN, QueryTriggerInteraction.Ignore);
					for (int num4 = 0; num4 < num3; num4++)
					{
						bool flag3 = false;
						for (int num5 = 0; num5 < this.trainCars.Length; num5++)
						{
							if (InteractableVehicle.grab[num4].transform.IsChildOf(this.trainCars[num5].root) || InteractableVehicle.grab[num4].transform == this.trainCars[num5].root)
							{
								flag3 = true;
								break;
							}
						}
						if (!flag3)
						{
							if (InteractableVehicle.grab[num4].CompareTag("Vehicle"))
							{
								Rigidbody component = InteractableVehicle.grab[num4].GetComponent<Rigidbody>();
								if (!component.isKinematic)
								{
									component.AddForce(base.transform.forward * this.altSpeedOutput, ForceMode.VelocityChange);
								}
							}
							flag2 = true;
							break;
						}
					}
					if (flag2)
					{
						if (this.altSpeedOutput > 0f)
						{
							if (this.altSpeedInput > 0f)
							{
								this.altSpeedInput = 0f;
							}
						}
						else if (this.altSpeedInput < 0f)
						{
							this.altSpeedInput = 0f;
						}
					}
					else
					{
						this.roadPosition += num2;
						this.roadPosition = this.clampRoadPosition(this.roadPosition);
					}
				}
			}
			if (!Dedicator.isDedicated && this.road != null)
			{
				foreach (TrainCar trainCar2 in this.trainCars)
				{
					Vector3 b;
					Vector3 b2;
					Vector3 b3;
					this.road.getTrackData(this.clampRoadPosition(this.roadPosition + trainCar2.trackPositionOffset + this.asset.trainWheelOffset), out b, out b2, out b3);
					trainCar2.currentFrontPosition = Vector3.Lerp(trainCar2.currentFrontPosition, b, 8f * Time.deltaTime);
					trainCar2.currentFrontNormal = Vector3.Lerp(trainCar2.currentFrontNormal, b2, 8f * Time.deltaTime);
					trainCar2.currentFrontDirection = Vector3.Lerp(trainCar2.currentFrontDirection, b3, 8f * Time.deltaTime);
					Vector3 b4;
					Vector3 b5;
					Vector3 b6;
					this.road.getTrackData(this.clampRoadPosition(this.roadPosition + trainCar2.trackPositionOffset - this.asset.trainWheelOffset), out b4, out b5, out b6);
					trainCar2.currentBackPosition = Vector3.Lerp(trainCar2.currentBackPosition, b4, 8f * Time.deltaTime);
					trainCar2.currentBackNormal = Vector3.Lerp(trainCar2.currentBackNormal, b5, 8f * Time.deltaTime);
					trainCar2.currentBackDirection = Vector3.Lerp(trainCar2.currentBackDirection, b6, 8f * Time.deltaTime);
					this.moveTrain(trainCar2.currentFrontPosition, trainCar2.currentFrontNormal, trainCar2.currentFrontDirection, trainCar2.currentBackPosition, trainCar2.currentBackNormal, trainCar2.currentBackDirection, trainCar2);
				}
			}
			if (Provider.isServer)
			{
				if (this.isDriven)
				{
					if (this.tires != null)
					{
						for (int num7 = 0; num7 < this.tires.Length; num7++)
						{
							if (this.tires[num7] == null)
							{
								break;
							}
							this.tires[num7].checkForTraps();
						}
					}
				}
				else
				{
					this._speed = base.transform.InverseTransformDirection(base.GetComponent<Rigidbody>().velocity).z;
					this._physicsSpeed = this._speed;
					this._turn = 0;
					this.real = base.transform.position;
				}
				if (this.isDead && !this.isExploded && !this.isUnderwater && Time.realtimeSinceStartup - this.lastDead > InteractableVehicle.EXPLODE)
				{
					this.explode();
				}
			}
			if (!Provider.isServer && !this.isPhysical && Time.realtimeSinceStartup - this.lastTick > Provider.UPDATE_TIME * 2f)
			{
				this.lastTick = Time.realtimeSinceStartup;
				this._speed = 0f;
				this._physicsSpeed = 0f;
				this._turn = 0;
			}
			if (this.sirensOn && !Dedicator.isDedicated && Time.realtimeSinceStartup - this.lastWeeoo > 0.33f)
			{
				this.lastWeeoo = Time.realtimeSinceStartup;
				this.sirenState = !this.sirenState;
				foreach (GameObject gameObject in this.sirenGameObjects_0)
				{
					gameObject.SetActive(!this.sirenState);
				}
				foreach (GameObject gameObject2 in this.sirenGameObjects_1)
				{
					gameObject2.SetActive(this.sirenState);
				}
				if (this.sirenMaterials != null)
				{
					if (this.sirenMaterials[0] != null)
					{
						this.sirenMaterials[0].SetColor("_EmissionColor", this.sirenState ? Color.black : this.sirenMaterials[0].color);
					}
					if (this.sirenMaterials[1] != null)
					{
						this.sirenMaterials[1].SetColor("_EmissionColor", (!this.sirenState) ? Color.black : this.sirenMaterials[1].color);
					}
				}
			}
			if (this.usesBattery)
			{
				this.batteryBuffer += deltaTime * 20f;
				ushort num8 = (ushort)Mathf.FloorToInt(this.batteryBuffer);
				this.batteryBuffer -= (float)num8;
				if (num8 > 0)
				{
					if (this.isEngineOn && this.isDriven)
					{
						this.askChargeBattery(num8);
					}
					else if (this.headlightsOn || this.sirensOn)
					{
						this.askBurnBattery(num8);
					}
				}
			}
			if (Provider.isServer)
			{
				SafezoneNode safezoneNode;
				if (LevelNodes.isPointInsideSafezone(base.transform.position, out safezoneNode))
				{
					this.timeInsideSafezone += deltaTime;
					if (Provider.modeConfigData != null && Provider.modeConfigData.Vehicles.Unlocked_After_Seconds_In_Safezone > 0f && this.timeInsideSafezone > Provider.modeConfigData.Vehicles.Unlocked_After_Seconds_In_Safezone && this.isEmpty && this.isLocked)
					{
						VehicleManager.unlockVehicle(this, null);
					}
				}
				else
				{
					this.timeInsideSafezone = -1f;
				}
			}
		}

		protected virtual void handleTireAliveChanged(Wheel wheel)
		{
			if (this.isPhysical)
			{
				base.GetComponent<Rigidbody>().WakeUp();
			}
		}

		public void safeInit()
		{
			this._asset = (VehicleAsset)Assets.find(EAssetType.VEHICLE, this.id);
			if (!Dedicator.isDedicated)
			{
				this.fire = base.transform.FindChild("Fire");
				LightLODTool.applyLightLOD(this.fire);
				this.smoke_0 = base.transform.FindChild("Smoke_0");
				this.smoke_1 = base.transform.FindChild("Smoke_1");
			}
		}

		public void init()
		{
			this.safeInit();
			if (!Provider.isServer)
			{
				this.nsb = new NetworkSnapshotBuffer(Provider.UPDATE_TIME, Provider.UPDATE_DELAY);
			}
			if (Provider.isServer)
			{
				if (this.fuel == 65535)
				{
					if (Provider.mode == EGameMode.TUTORIAL)
					{
						this.fuel = 0;
					}
					else
					{
						this.fuel = (ushort)UnityEngine.Random.Range((int)this.asset.fuelMin, (int)this.asset.fuelMax);
					}
				}
				if (this.health == 65535)
				{
					this.health = (ushort)UnityEngine.Random.Range((int)this.asset.healthMin, (int)this.asset.healthMax);
				}
				if (this.batteryCharge == 65535)
				{
					if (this.usesBattery)
					{
						if (this.asset.canSpawnWithBattery && UnityEngine.Random.value < Provider.modeConfigData.Vehicles.Has_Battery_Chance)
						{
							float num = UnityEngine.Random.Range(Provider.modeConfigData.Vehicles.Min_Battery_Charge, Provider.modeConfigData.Vehicles.Max_Battery_Charge);
							num *= this.asset.batterySpawnChargeMultiplier;
							this.batteryCharge = (ushort)(10000f * num);
						}
						else
						{
							this.batteryCharge = 0;
						}
					}
					else
					{
						this.batteryCharge = 10000;
					}
				}
			}
			if (!Dedicator.isDedicated)
			{
				base.transform.FindAllChildrenWithName("Sirens", this.sirenGameObjects);
				base.transform.FindAllChildrenWithName("Siren_0", this.sirenGameObjects_0);
				base.transform.FindAllChildrenWithName("Siren_1", this.sirenGameObjects_1);
				foreach (GameObject gameObject in this.sirenGameObjects)
				{
					LightLODTool.applyLightLOD(gameObject.transform);
				}
				this.sirenMaterials = new Material[2];
				List<GameObject> list = new List<GameObject>();
				base.transform.FindAllChildrenWithName("Siren_0_Model", list);
				foreach (GameObject gameObject2 in list)
				{
					if (this.sirenMaterials[0] == null)
					{
						this.sirenMaterials[0] = gameObject2.GetComponent<Renderer>().material;
					}
					else
					{
						gameObject2.GetComponent<Renderer>().sharedMaterial = this.sirenMaterials[0];
					}
				}
				list.Clear();
				base.transform.FindAllChildrenWithName("Siren_1_Model", list);
				foreach (GameObject gameObject3 in list)
				{
					if (this.sirenMaterials[1] == null)
					{
						this.sirenMaterials[1] = gameObject3.GetComponent<Renderer>().material;
					}
					else
					{
						gameObject3.GetComponent<Renderer>().sharedMaterial = this.sirenMaterials[1];
					}
				}
				this._headlights = base.transform.FindChild("Headlights");
				LightLODTool.applyLightLOD(this.headlights);
				Transform transform = base.transform.FindChildRecursive("Headlights_Model");
				if (transform != null)
				{
					this.headlightsMaterial = transform.GetComponent<Renderer>().material;
				}
				this._taillights = base.transform.FindChild("Taillights");
				LightLODTool.applyLightLOD(this.taillights);
				Transform transform2 = base.transform.FindChildRecursive("Taillights_Model");
				if (transform2 != null)
				{
					this.taillightsMaterial = transform2.GetComponent<Renderer>().material;
				}
				else
				{
					InteractableVehicle.materials.Clear();
					for (int i = 0; i < 4; i++)
					{
						Transform transform3 = base.transform.FindChild("Taillight_" + i + "_Model");
						if (transform3 == null)
						{
							break;
						}
						InteractableVehicle.materials.Add(transform3.GetComponent<Renderer>().material);
					}
					if (InteractableVehicle.materials.Count > 0)
					{
						this.taillightMaterials = InteractableVehicle.materials.ToArray();
					}
				}
				if ((this.asset.engine == EEngine.HELICOPTER || this.asset.engine == EEngine.BLIMP) && this.clipAudioSource != null)
				{
					this.windZone = this.clipAudioSource.gameObject.AddComponent<WindZone>();
					this.windZone.mode = WindZoneMode.Spherical;
					this.windZone.radius = 64f;
					this.windZone.windMain = 0f;
					this.windZone.windTurbulence = 0f;
					this.windZone.windPulseFrequency = 0f;
					this.windZone.windPulseMagnitude = 0f;
				}
			}
			this._sirensOn = false;
			this._headlightsOn = false;
			this._taillightsOn = false;
			this.waterCenterTransform = base.transform.FindChild("Water_Center");
			Transform transform4 = base.transform.FindChild("Seats");
			if (transform4 == null)
			{
				Debug.LogError("Vehicle [" + this.id + "] missing seats!");
			}
			Transform transform5 = base.transform.FindChild("Objects");
			Transform transform6 = base.transform.FindChild("Turrets");
			this._passengers = new Passenger[transform4.childCount];
			for (int j = 0; j < this.passengers.Length; j++)
			{
				Transform newSeat = transform4.FindChild("Seat_" + j);
				Transform newObj = null;
				if (transform5 != null)
				{
					newObj = transform5.FindChild("Seat_" + j);
				}
				Transform transform7 = null;
				Transform newTurretPitch = null;
				Transform newTurretAim = null;
				if (transform6 != null)
				{
					Transform transform8 = transform6.FindChild("Turret_" + j);
					if (transform8 != null)
					{
						transform7 = transform8.FindChild("Yaw");
						if (transform7 != null)
						{
							Transform transform9 = transform7.FindChild("Seats");
							if (transform9 != null)
							{
								newSeat = transform9.FindChild("Seat_" + j);
							}
							Transform transform10 = transform7.FindChild("Objects");
							if (transform10 != null)
							{
								newObj = transform10.FindChild("Seat_" + j);
							}
							newTurretPitch = transform7.FindChild("Pitch");
						}
						newTurretAim = transform8.FindChildRecursive("Aim");
					}
				}
				this.passengers[j] = new Passenger(newSeat, newObj, transform7, newTurretPitch, newTurretAim);
			}
			this._turrets = new Passenger[this.asset.turrets.Length];
			for (int k = 0; k < this.turrets.Length; k++)
			{
				TurretInfo turretInfo = this.asset.turrets[k];
				if ((int)turretInfo.seatIndex < this.passengers.Length)
				{
					this.passengers[(int)turretInfo.seatIndex].turret = turretInfo;
					this._turrets[k] = this.passengers[(int)turretInfo.seatIndex];
				}
			}
			Transform transform11 = base.transform.FindChild("Tires");
			if (transform11 != null)
			{
				this.tires = new Wheel[transform11.childCount];
				for (int l = 0; l < transform11.childCount; l++)
				{
					Wheel wheel = new Wheel(this, (WheelCollider)transform11.FindChild("Tire_" + l).GetComponent<Collider>(), l < 2, l >= transform11.childCount - 2);
					wheel.reset();
					wheel.aliveChanged += this.handleTireAliveChanged;
					this.tires[l] = wheel;
				}
			}
			else
			{
				this.tires = new Wheel[0];
			}
			this.buoyancy = base.transform.FindChild("Buoyancy");
			if (this.buoyancy != null)
			{
				for (int m = 0; m < this.buoyancy.childCount; m++)
				{
					Transform child = this.buoyancy.GetChild(m);
					child.gameObject.AddComponent<Buoyancy>().density = (float)(this.buoyancy.childCount * 500);
					if (this.asset.engine == EEngine.BLIMP)
					{
						child.GetComponent<Buoyancy>().overrideSurfaceElevation = Level.info.configData.Blimp_Altitude;
					}
				}
			}
			this.hook = base.transform.FindChild("Hook");
			this.hooked = new List<HookInfo>();
			this.center = base.transform.FindChild("Center");
			if (this.center == null)
			{
				this.center = base.transform;
			}
			Transform transform12 = base.transform.FindChild("DepthMask");
			if (transform12 != null)
			{
				transform12.GetComponent<Renderer>().sharedMaterial = (Material)Resources.Load("Materials/DepthMask");
			}
			if (!Dedicator.isDedicated)
			{
				List<Wheel> list2 = new List<Wheel>(this.tires);
				Transform transform13 = base.transform.FindChild("Wheels");
				if (transform13 != null)
				{
					for (int n = 0; n < list2.Count; n++)
					{
						int num2 = -1;
						float num3 = 16f;
						for (int num4 = 0; num4 < transform13.childCount; num4++)
						{
							Transform child2 = transform13.GetChild(num4);
							float sqrMagnitude = (list2[n].wheel.transform.position - child2.position).sqrMagnitude;
							if (sqrMagnitude < num3)
							{
								num2 = num4;
								num3 = sqrMagnitude;
							}
						}
						if (num2 != -1)
						{
							Transform transform14 = transform13.GetChild(num2);
							if (transform14.childCount == 0)
							{
								Transform transform15 = base.transform.FindChildRecursive("Wheel_" + num2);
								if (transform15 != null)
								{
									transform14 = transform15;
								}
							}
							list2[n].model = transform14;
							list2[n].rest = transform14.localRotation;
						}
					}
					if (transform13.childCount != list2.Count)
					{
						for (int num5 = 0; num5 < transform13.childCount; num5++)
						{
							Transform transform16 = transform13.GetChild(num5);
							if (transform16.childCount != 0)
							{
								for (int num6 = 0; num6 < this.tires.Length; num6++)
								{
									if (list2[num6].model == transform16)
									{
										transform16 = null;
										break;
									}
								}
								if (!(transform16 == null))
								{
									list2.Add(new Wheel(this, null, false, false)
									{
										model = transform16,
										rest = transform16.localRotation
									});
								}
							}
						}
					}
				}
				this.tires = list2.ToArray();
				this.wheel = base.transform.FindChild("Objects").FindChild("Steer");
				this.pedalLeft = base.transform.FindChild("Objects").FindChild("Pedal_Left");
				this.pedalRight = base.transform.FindChild("Objects").FindChild("Pedal_Right");
				Transform transform17 = base.transform.FindChild("Rotors");
				if (transform17 != null)
				{
					this.rotors = new Rotor[transform17.childCount];
					for (int num7 = 0; num7 < transform17.childCount; num7++)
					{
						Transform child3 = transform17.GetChild(num7);
						Rotor rotor = new Rotor();
						rotor.prop = child3;
						rotor.material_0 = child3.FindChild("Model_0").GetComponent<Renderer>().material;
						rotor.material_1 = child3.FindChild("Model_1").GetComponent<Renderer>().material;
						rotor.rest = child3.localRotation;
						this.rotors[num7] = rotor;
					}
				}
				else
				{
					this.rotors = new Rotor[0];
				}
				Transform transform18 = base.transform.FindChild("Exhaust");
				if (transform18 != null)
				{
					this.exhausts = new ParticleSystem[transform18.childCount];
					for (int num8 = 0; num8 < transform18.childCount; num8++)
					{
						Transform child4 = transform18.GetChild(num8);
						this.exhausts[num8] = child4.GetComponent<ParticleSystem>();
					}
				}
				else
				{
					this.exhausts = new ParticleSystem[0];
				}
				if (this.wheel != null)
				{
					this.restWheel = this.wheel.localRotation;
				}
				this.front = base.transform.FindChild("Objects").FindChild("Front");
				if (this.front != null)
				{
					this.restFront = this.front.localRotation;
				}
				this.tellFuel(this.fuel);
				this.tellHealth(this.health);
				this.tellBatteryCharge(this.batteryCharge);
				this.updateSkin();
			}
			if (this.isExploded)
			{
				this.tellExploded();
			}
			if (this.asset.engine == EEngine.TRAIN)
			{
				Transform transform19 = base.transform.FindChild("Train_Cars");
				int childCount = transform19.childCount;
				this.trainCars = new TrainCar[1 + childCount];
				this.trainCars[0] = this.getTrainCar(base.transform);
				for (int num9 = 1; num9 <= childCount; num9++)
				{
					Transform transform20 = transform19.FindChild("Train_Car_" + num9);
					transform20.parent = LevelVehicles.models;
					transform20.GetOrAddComponent<VehicleRef>().vehicle = this;
					TrainCar trainCar = this.getTrainCar(transform20);
					trainCar.trackPositionOffset = (float)num9 * -this.asset.trainCarLength;
					this.trainCars[num9] = trainCar;
				}
				foreach (TrainCar trainCar2 in this.trainCars)
				{
					if (this.overlapFront == null)
					{
						this.overlapFront = trainCar2.root.FindChild("Overlap_Front");
					}
					if (this.overlapBack == null)
					{
						this.overlapBack = trainCar2.root.FindChild("Overlap_Back");
					}
					if (this.overlapFront != null && this.overlapBack != null)
					{
						break;
					}
				}
				foreach (LevelTrainAssociation levelTrainAssociation in Level.info.configData.Trains)
				{
					if (levelTrainAssociation.VehicleID == this.id)
					{
						this.roadIndex = levelTrainAssociation.RoadIndex;
						break;
					}
				}
				this.road = LevelRoads.getRoad((int)this.roadIndex);
				this.teleportTrain();
			}
		}

		private void Awake()
		{
			if (!Dedicator.isDedicated)
			{
				Transform transform = base.transform.FindChild("Sound");
				if (transform != null)
				{
					this.clipAudioSource = transform.GetComponent<AudioSource>();
				}
				this.engineAudioSource = base.GetComponent<AudioSource>();
				if (this.engineAudioSource != null)
				{
					this.engineAudioSource.maxDistance *= 2f;
				}
				Transform transform2 = base.transform.FindChild("Engine_Additive");
				if (transform2 != null)
				{
					this.engineAdditiveAudioSource = transform2.GetComponent<AudioSource>();
					this.engineAdditiveAudioSource.maxDistance *= 2f;
				}
			}
		}

		private void initBumper(Transform bumper, bool reverse, bool instakill)
		{
			if (bumper == null)
			{
				return;
			}
			if (Provider.isServer)
			{
				Bumper bumper2 = bumper.gameObject.AddComponent<Bumper>();
				bumper2.reverse = reverse;
				bumper2.instakill = instakill;
				bumper2.init(this);
			}
			else
			{
				UnityEngine.Object.Destroy(bumper.gameObject);
			}
		}

		private void initBumpers(Transform root)
		{
			if (!Provider.isServer)
			{
				Transform transform = root.FindChildRecursive("Nav");
				if (transform != null)
				{
					UnityEngine.Object.Destroy(transform.gameObject);
				}
			}
			Transform bumper = root.FindChildRecursive("Bumper");
			this.initBumper(bumper, false, this.asset.engine == EEngine.TRAIN);
			Transform bumper2 = root.FindChildRecursive("Bumper_Front");
			this.initBumper(bumper2, false, this.asset.engine == EEngine.TRAIN);
			Transform bumper3 = root.FindChildRecursive("Bumper_Back");
			this.initBumper(bumper3, true, this.asset.engine == EEngine.TRAIN);
		}

		private void Start()
		{
			if (this.trainCars != null && this.trainCars.Length > 0)
			{
				foreach (TrainCar trainCar in this.trainCars)
				{
					this.initBumpers(trainCar.root);
				}
			}
			else
			{
				this.initBumpers(base.transform);
			}
			this.updateVehicle();
			this.updatePhysics();
			this.updateEngine();
			this.updates = new List<VehicleStateUpdate>();
		}

		private void OnDestroy()
		{
			this.dropTrunkItems();
			if (this.isExploded && !Dedicator.isDedicated)
			{
				HighlighterTool.destroyMaterials(base.transform);
				if (this.turrets != null)
				{
					for (int i = 0; i < this.turrets.Length; i++)
					{
						HighlighterTool.destroyMaterials(this.turrets[i].turretYaw);
						HighlighterTool.destroyMaterials(this.turrets[i].turretPitch);
					}
				}
			}
			if (this.headlightsMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(this.headlightsMaterial);
			}
			if (this.taillightsMaterial != null)
			{
				UnityEngine.Object.DestroyImmediate(this.taillightsMaterial);
			}
			else if (this.taillightMaterials != null)
			{
				for (int j = 0; j < this.taillightMaterials.Length; j++)
				{
					if (this.taillightMaterials[j] != null)
					{
						UnityEngine.Object.DestroyImmediate(this.taillightMaterials[j]);
					}
				}
			}
			if (this.sirenMaterials != null)
			{
				for (int k = 0; k < this.sirenMaterials.Length; k++)
				{
					if (this.sirenMaterials[k] != null)
					{
						UnityEngine.Object.DestroyImmediate(this.sirenMaterials[k]);
					}
				}
			}
			if (this.rotors == null)
			{
				return;
			}
			for (int l = 0; l < this.rotors.Length; l++)
			{
				if (this.rotors[l].material_0 != null)
				{
					UnityEngine.Object.DestroyImmediate(this.rotors[l].material_0);
					this.rotors[l].material_0 = null;
				}
				if (this.rotors[l].material_1 != null)
				{
					UnityEngine.Object.DestroyImmediate(this.rotors[l].material_1);
					this.rotors[l].material_1 = null;
				}
			}
		}

		private static Collider[] grab = new Collider[4];

		private static List<Material> materials = new List<Material>();

		private static readonly float EXPLODE = 4f;

		private static readonly ushort HEALTH_0 = 100;

		private static readonly ushort HEALTH_1 = 200;

		public uint instanceID;

		public ushort id;

		public Items trunkItems;

		public ushort skinID;

		public ushort mythicID;

		protected SkinAsset skinAsset;

		private List<Mesh> tempMesh;

		protected Material tempMaterial;

		protected Transform effectSlotsRoot;

		protected Transform[] effectSlots;

		protected Transform[] effectSystems;

		public ushort roadIndex;

		public float roadPosition;

		public ushort fuel;

		public ushort health;

		public ushort batteryCharge;

		private uint lastBurnFuel;

		private float horned;

		private bool _isDrowned;

		private float _lastDead;

		private float _lastUnderwater;

		private float _lastExploded;

		private float _slip;

		public bool isExploded;

		private float _factor;

		private float _speed;

		private float _physicsSpeed;

		private float _spedometer;

		private int _turn;

		private float spin;

		private float _steer;

		private float fly;

		private Rotor[] rotors;

		private ParticleSystem[] exhausts;

		private Transform wheel;

		private Transform overlapFront;

		private Transform overlapBack;

		private Transform pedalLeft;

		private Transform pedalRight;

		private Transform front;

		private Quaternion restWheel;

		private Quaternion restFront;

		private Transform waterCenterTransform;

		private Transform fire;

		private Transform smoke_0;

		private Transform smoke_1;

		[Obsolete]
		public bool isUpdated;

		public List<VehicleStateUpdate> updates;

		private Material[] sirenMaterials;

		private bool sirenState;

		private List<GameObject> sirenGameObjects = new List<GameObject>();

		private List<GameObject> sirenGameObjects_0 = new List<GameObject>();

		private List<GameObject> sirenGameObjects_1 = new List<GameObject>();

		private bool _sirensOn;

		private Transform _headlights;

		private Material headlightsMaterial;

		private bool _headlightsOn;

		private Transform _taillights;

		private Material taillightsMaterial;

		private Material[] taillightMaterials;

		private bool _taillightsOn;

		private CSteamID _lockedOwner;

		private CSteamID _lockedGroup;

		private bool _isLocked;

		private VehicleAsset _asset;

		public float lastSeat;

		private Passenger[] _passengers;

		private Passenger[] _turrets;

		public bool isHooked;

		private Transform buoyancy;

		private Transform hook;

		private List<HookInfo> hooked;

		private Transform center;

		private Vector3 lastUpdatedPos;

		private NetworkSnapshotBuffer nsb;

		private Vector3 real;

		private float lastTick;

		private float lastWeeoo;

		private AudioSource clipAudioSource;

		private AudioSource engineAudioSource;

		private AudioSource engineAdditiveAudioSource;

		private WindZone windZone;

		private bool isRecovering;

		private float lastRecover;

		private bool isPhysical;

		private bool isFrozen;

		public bool isBlimpFloating;

		private float altSpeedInput;

		private float altSpeedOutput;

		private float speedTraction;

		private float batteryBuffer;
	}
}

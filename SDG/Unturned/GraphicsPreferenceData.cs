﻿using System;

namespace SDG.Unturned
{
	public class GraphicsPreferenceData
	{
		public GraphicsPreferenceData()
		{
			this.Use_Skybox_Ambience = false;
			this.Use_Lens_Dirt = true;
			this.Chromatic_Aberration_Intensity = 0.2f;
		}

		public bool Use_Skybox_Ambience;

		public bool Use_Lens_Dirt;

		public float Chromatic_Aberration_Intensity;
	}
}

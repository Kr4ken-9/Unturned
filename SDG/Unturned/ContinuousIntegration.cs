﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class ContinuousIntegration
	{
		public static bool isRunning
		{
			get
			{
				if (!ContinuousIntegration.gotRunningCI)
				{
					ContinuousIntegration.gotRunningCI = true;
					ContinuousIntegration.runningCIValue = Environment.CommandLine.Contains("-runningCI");
				}
				return ContinuousIntegration.runningCIValue;
			}
		}

		public static void reportSuccess()
		{
			if (!ContinuousIntegration.isRunning)
			{
				return;
			}
			if (ContinuousIntegration.isExiting)
			{
				return;
			}
			ContinuousIntegration.isExiting = true;
			PlayerContinuousIntegrationReport instance = new PlayerContinuousIntegrationReport();
			ReadWrite.serializeJSON<PlayerContinuousIntegrationReport>(Application.dataPath + "/CI_Report.json", false, false, instance);
			Debug.Log("Unturned CI success!");
			Provider.shutdown();
		}

		public static void reportFailure(object message)
		{
			if (!ContinuousIntegration.isRunning)
			{
				return;
			}
			if (ContinuousIntegration.isExiting)
			{
				return;
			}
			ContinuousIntegration.isExiting = true;
			string text = message.ToString();
			if (text.StartsWith("BoxColliders does not support negative scale or size."))
			{
				return;
			}
			if (text.StartsWith("cleaning the mesh failed"))
			{
				return;
			}
			PlayerContinuousIntegrationReport instance = new PlayerContinuousIntegrationReport(text);
			ReadWrite.serializeJSON<PlayerContinuousIntegrationReport>(Application.dataPath + "/CI_Report.json", false, false, instance);
			Debug.LogFormat("Unturned CI failure! [{0}]", new object[]
			{
				text
			});
			Environment.ExitCode = 1;
			Application.Quit();
		}

		protected static bool gotRunningCI;

		protected static bool runningCIValue;

		protected static bool isExiting;
	}
}

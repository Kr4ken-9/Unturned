﻿using System;

namespace SDG.Unturned
{
	public class IgnoredCraftingBlueprint
	{
		public bool matchesBlueprint(Blueprint blueprint)
		{
			return this.itemId == blueprint.sourceItem.id && this.blueprintIndex == blueprint.id;
		}

		public ushort itemId;

		public byte blueprintIndex;
	}
}

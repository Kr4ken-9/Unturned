﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class InteractableClock : InteractablePower
	{
		public override void updateState(Asset asset, byte[] state)
		{
			this.handHourTransform = base.transform.FindChild("Hand_Hour");
			this.handMinuteTransform = base.transform.FindChild("Hand_Minute");
			base.updateState(asset, state);
		}

		public override bool checkUseable()
		{
			return base.isWired;
		}

		public override bool checkHint(out EPlayerMessage message, out string text, out Color color)
		{
			text = string.Empty;
			color = Color.white;
			if (!base.isWired)
			{
				message = EPlayerMessage.POWER;
				return true;
			}
			message = EPlayerMessage.NONE;
			return false;
		}

		private void Update()
		{
			if (!base.isWired)
			{
				return;
			}
			if (this.handHourTransform == null || this.handMinuteTransform == null)
			{
				return;
			}
			float num;
			if (LightingManager.day < LevelLighting.bias)
			{
				num = LightingManager.day / LevelLighting.bias;
			}
			else
			{
				num = (LightingManager.day - LevelLighting.bias) / (1f - LevelLighting.bias);
			}
			float num2 = num - 0.5f;
			float num3 = num * 12f;
			this.handHourTransform.localRotation = Quaternion.Euler(0f, num2 * -360f, 0f);
			this.handMinuteTransform.localRotation = Quaternion.Euler(0f, num3 * -360f, 0f);
		}

		private Transform handHourTransform;

		private Transform handMinuteTransform;
	}
}

﻿using System;

namespace SDG.Unturned
{
	[AttributeUsage(AttributeTargets.Method)]
	public class SteamCall : Attribute
	{
		public SteamCall(ESteamCallValidation validation)
		{
			this.validation = validation;
		}

		public SteamCall(ESteamCallValidation validation, ESteamCallFrequency frequency)
		{
			this.validation = validation;
			this.frequency = frequency;
		}

		public ESteamCallValidation validation;

		public ESteamCallFrequency frequency;
	}
}

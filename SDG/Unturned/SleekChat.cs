﻿using System;
using UnityEngine;

namespace SDG.Unturned
{
	public class SleekChat : Sleek
	{
		public SleekChat()
		{
			base.init();
			this.avatarImage = new SleekImageTexture();
			this.avatarImage.positionOffset_Y = 4;
			this.avatarImage.sizeOffset_X = 32;
			this.avatarImage.sizeOffset_Y = 32;
			base.add(this.avatarImage);
			this.contentsLabel = new SleekLabel();
			this.contentsLabel.positionOffset_X = 40;
			this.contentsLabel.sizeOffset_X = -40;
			this.contentsLabel.sizeOffset_Y = 40;
			this.contentsLabel.sizeScale_X = 1f;
			this.contentsLabel.fontSize = 14;
			this.contentsLabel.fontAlignment = TextAnchor.MiddleLeft;
			this.contentsLabel.foregroundTint = ESleekTint.NONE;
			base.add(this.contentsLabel);
		}

		public ReceivedChatMessage representingChatMessage
		{
			get
			{
				return this._representingChatMessage;
			}
			set
			{
				this._representingChatMessage = value;
				this.representingChatMessage.queryIcon(new Provider.IconQueryCallback(this.onIconDownloaded));
				this.contentsLabel.foregroundColor = this.representingChatMessage.color;
				this.contentsLabel.richOverrideColor = this.representingChatMessage.color;
				this.contentsLabel.isRich = this.representingChatMessage.useRichTextFormatting;
				this.contentsLabel.text = this.representingChatMessage.contents;
			}
		}

		protected void onIconDownloaded(Texture2D icon)
		{
			this.avatarImage.texture = icon;
		}

		protected override void update()
		{
			base.update();
			if (this.shouldFadeOutWithAge)
			{
				float num = this.representingChatMessage.age - Provider.preferenceData.Chat.Fade_Delay;
				num = Mathf.Clamp01(num);
				float a = 1f - num;
				Color foregroundColor = this.avatarImage.foregroundColor;
				foregroundColor.a = a;
				this.avatarImage.backgroundColor = foregroundColor;
				Color foregroundColor2 = this.contentsLabel.foregroundColor;
				foregroundColor2.a = a;
				this.contentsLabel.foregroundColor = foregroundColor2;
			}
		}

		public SleekImageTexture avatarImage;

		public SleekLabel contentsLabel;

		public bool shouldFadeOutWithAge;

		protected ReceivedChatMessage _representingChatMessage;
	}
}

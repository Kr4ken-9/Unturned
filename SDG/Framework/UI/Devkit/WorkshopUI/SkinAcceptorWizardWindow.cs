﻿using System;
using System.IO;
using SDG.Framework.Debug;
using SDG.Framework.Translations;
using SDG.Framework.UI.Devkit.InspectorUI;
using SDG.Framework.UI.Sleek2;
using SDG.Framework.Utilities;
using SDG.Unturned;
using Steamworks;
using UnityEngine;

namespace SDG.Framework.UI.Devkit.WorkshopUI
{
	public class SkinAcceptorWizardWindow : Sleek2Window
	{
		public SkinAcceptorWizardWindow()
		{
			base.gameObject.name = "UGC_Skin_Acceptor_Wizard";
			base.tab.label.translation = new TranslatedText(new TranslationReference("SDG", "Devkit.Window.UGC_Skin_Acceptor_Wizard.Title"));
			base.tab.label.translation.format();
			this.inspector = new Sleek2Inspector();
			this.inspector.transform.anchorMin = new Vector2(0f, 0f);
			this.inspector.transform.anchorMax = new Vector2(1f, 1f);
			this.inspector.transform.pivot = new Vector2(0f, 1f);
			this.inspector.transform.offsetMin = new Vector2(0f, 40f);
			this.inspector.transform.offsetMax = new Vector2(0f, 0f);
			this.inspector.inspect(this);
			base.safePanel.addElement(this.inspector);
			this.inputButton = new Sleek2ImageTranslatedLabelButton();
			this.inputButton.transform.anchorMin = new Vector2(0f, 0f);
			this.inputButton.transform.anchorMax = new Vector2(1f, 0f);
			this.inputButton.transform.pivot = new Vector2(0.5f, 1f);
			this.inputButton.transform.offsetMin = new Vector2(0f, 20f);
			this.inputButton.transform.offsetMax = new Vector2(0f, 40f);
			this.inputButton.label.translation = new TranslatedText(new TranslationReference("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Input.Label"));
			this.inputButton.label.translation.format();
			this.inputButton.clicked += this.handleInputButtonClicked;
			base.safePanel.addElement(this.inputButton);
			this.iconButton = new Sleek2ImageTranslatedLabelButton();
			this.iconButton.transform.anchorMin = new Vector2(0f, 0f);
			this.iconButton.transform.anchorMax = new Vector2(1f, 0f);
			this.iconButton.transform.pivot = new Vector2(0.5f, 1f);
			this.iconButton.transform.offsetMin = new Vector2(0f, 0f);
			this.iconButton.transform.offsetMax = new Vector2(0f, 20f);
			this.iconButton.label.translation = new TranslatedText(new TranslationReference("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Icon.Label"));
			this.iconButton.label.translation.format();
			this.iconButton.clicked += this.handleIconButtonClicked;
			base.safePanel.addElement(this.iconButton);
			this.itemDownloaded = Callback<DownloadItemResult_t>.Create(new Callback<DownloadItemResult_t>.DispatchDelegate(this.onItemDownloaded));
			this.steamUGCQueryCompleted = CallResult<SteamUGCQueryCompleted_t>.Create(new CallResult<SteamUGCQueryCompleted_t>.APIDispatchDelegate(this.onSteamUGCQueryCompleted));
			this.personaStateChange = Callback<PersonaStateChange_t>.Create(new Callback<PersonaStateChange_t>.DispatchDelegate(this.onPersonaStateChange));
		}

		protected virtual Texture2D importTexture(string source, string destination, string file)
		{
			if (!File.Exists(source))
			{
				return null;
			}
			if (!Directory.Exists(destination))
			{
				Directory.CreateDirectory(destination);
			}
			string text = destination + file + Path.GetExtension(source);
			if (File.Exists(text))
			{
				File.Delete(text);
			}
			File.Copy(source, text);
			return null;
		}

		protected virtual Material importMaterial(string skinPath, SkinInfo info, string source, string destination, string file)
		{
			if (!Directory.Exists(destination))
			{
				Directory.CreateDirectory(destination);
			}
			Texture2D texture2D = this.importTexture(skinPath + info.albedoPath.absolutePath, source, "/Albedo");
			Texture2D texture2D2 = this.importTexture(skinPath + info.metallicPath.absolutePath, source, "/Metallic");
			Texture2D texture2D3 = this.importTexture(skinPath + info.emissionPath.absolutePath, source, "/Emission");
			if (texture2D == null && texture2D2 == null && texture2D3 == null)
			{
				return null;
			}
			bool flag = false;
			Material material;
			if (flag)
			{
				material = new Material(Shader.Find("Skins/Pattern"));
			}
			else
			{
				material = new Material(Shader.Find("Standard"));
				material.SetFloat("_Glossiness", 0f);
			}
			if (texture2D != null)
			{
				if (flag)
				{
					material.SetTexture("_AlbedoSkin", texture2D);
				}
				else
				{
					material.SetTexture("_MainTex", texture2D);
				}
			}
			if (texture2D2 != null)
			{
				if (flag)
				{
					material.SetTexture("_MetallicSkin", texture2D2);
				}
				else
				{
					material.SetTexture("_MetallicGlossMap", texture2D2);
				}
			}
			if (texture2D3 != null)
			{
				if (flag)
				{
					material.SetTexture("_EmissionSkin", texture2D3);
				}
				else
				{
					material.SetColor("_EmissionColor", Color.white);
					material.SetTexture("_EmissionMap", texture2D3);
				}
			}
			return material;
		}

		protected virtual void loadWorkshopItem()
		{
			ulong num;
			string str;
			uint num2;
			if (!SteamUGC.GetItemInstallInfo(this.workshopItemDetails.m_nPublishedFileId, out num, out str, 1024u, out num2))
			{
				Debug.LogError("Failed to load!");
				return;
			}
			Debug.Log("Loading: " + this.workshopItemDetails.m_nPublishedFileId);
			Debug.Log("Loaded: " + str);
		}

		protected void downloadWorkshopItem()
		{
			Debug.Log("Downloading...");
			this.isExpectingDownload = true;
			SteamUGC.DownloadItem(this.workshopItemDetails.m_nPublishedFileId, true);
		}

		protected void onSteamUGCQueryCompleted(SteamUGCQueryCompleted_t callback, bool io)
		{
			if (callback.m_handle != this.itemHandle)
			{
				return;
			}
			Debug.Log("Queried: " + callback.m_eResult);
			if (SteamUGC.GetQueryUGCResult(this.itemHandle, 0u, out this.workshopItemDetails))
			{
				this.hasPersonaInfo = false;
				if (!SteamFriends.RequestUserInformation((CSteamID)this.workshopItemDetails.m_ulSteamIDOwner, true))
				{
					this.downloadWorkshopItem();
				}
			}
			SteamUGC.ReleaseQueryUGCRequest(this.itemHandle);
			this.itemHandle = UGCQueryHandle_t.Invalid;
		}

		protected void queryItem(PublishedFileId_t publishedFileID)
		{
			Debug.Log("Querying...");
			if (this.itemHandle != UGCQueryHandle_t.Invalid)
			{
				SteamUGC.ReleaseQueryUGCRequest(this.itemHandle);
				this.itemHandle = UGCQueryHandle_t.Invalid;
			}
			this.itemHandle = SteamUGC.CreateQueryUGCDetailsRequest(new PublishedFileId_t[]
			{
				publishedFileID
			}, 1u);
			SteamAPICall_t hAPICall = SteamUGC.SendQueryUGCRequest(this.itemHandle);
			this.steamUGCQueryCompleted.Set(hAPICall, null);
		}

		protected void mainUpdated()
		{
			TimeUtility.updated -= this.mainUpdated;
			this.loadWorkshopItem();
		}

		protected void onItemDownloaded(DownloadItemResult_t callback)
		{
			if (!this.isExpectingDownload)
			{
				return;
			}
			this.isExpectingDownload = false;
			if (callback.m_nPublishedFileId.m_PublishedFileId != this.workshopItemDetails.m_nPublishedFileId.m_PublishedFileId)
			{
				Debug.LogWarning("Download ID doesn't match!");
				return;
			}
			Debug.Log("Downloaded: " + callback.m_eResult);
			TimeUtility.updated += this.mainUpdated;
		}

		protected void onPersonaStateChange(PersonaStateChange_t callback)
		{
			if (callback.m_ulSteamID != this.workshopItemDetails.m_ulSteamIDOwner)
			{
				return;
			}
			if (this.hasPersonaInfo)
			{
				return;
			}
			this.hasPersonaInfo = true;
			this.downloadWorkshopItem();
		}

		protected virtual void handleInputButtonClicked(Sleek2ImageButton button)
		{
			this.queryItem((PublishedFileId_t)this.workshopID);
		}

		protected virtual void handleIconButtonClicked(Sleek2ImageButton button)
		{
			IconUtils.getItemDefIcon(this.iconItemID, this.iconVehicleID, this.iconSkinID);
		}

		public override void destroy()
		{
			this.itemDownloaded.Dispose();
			this.itemDownloaded = null;
			this.steamUGCQueryCompleted.Dispose();
			this.steamUGCQueryCompleted = null;
			this.personaStateChange.Dispose();
			this.personaStateChange = null;
			base.destroy();
		}

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Workshop_ID.Name", null)]
		public ulong workshopID;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Pattern_ID.Name", null)]
		public string patternID;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Econ_Name.Name", null)]
		public string econName;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Econ_Desc.Name", null)]
		public string econDesc;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Econ_Type.Name", null)]
		public ESkinAcceptEconType econType;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Icon_Item_ID.Name", null)]
		public ushort iconItemID;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Icon_Vehicle_ID.Name", null)]
		public ushort iconVehicleID;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Icon_Skin_ID.Name", null)]
		public ushort iconSkinID;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Acceptor_Wizard.Icon_Econ_ID.Name", null)]
		public int iconEconID;

		protected Sleek2Inspector inspector;

		protected Sleek2ImageTranslatedLabelButton inputButton;

		protected Sleek2ImageTranslatedLabelButton iconButton;

		protected UGCQueryHandle_t itemHandle = UGCQueryHandle_t.Invalid;

		protected SteamUGCDetails_t workshopItemDetails;

		protected bool isExpectingDownload;

		protected bool hasPersonaInfo;

		protected CallResult<SteamUGCQueryCompleted_t> steamUGCQueryCompleted;

		protected Callback<DownloadItemResult_t> itemDownloaded;

		protected Callback<PersonaStateChange_t> personaStateChange;
	}
}

﻿using System;

namespace SDG.Framework.UI.Devkit.WorkshopUI
{
	public class EconStoreItemDefinition : EconItemDefinition
	{
		public int Price;

		public bool IsPurchasable;

		public bool IsCommodity;

		public bool IsTimeLimitedOnStore;

		public string LimitedWindowStart;

		public string LimitedWindowStop;
	}
}

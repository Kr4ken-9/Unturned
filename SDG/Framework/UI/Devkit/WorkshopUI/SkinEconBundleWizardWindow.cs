﻿using System;
using SDG.Framework.Debug;
using SDG.Framework.Translations;
using SDG.Framework.UI.Devkit.InspectorUI;
using SDG.Framework.UI.Sleek2;
using UnityEngine;

namespace SDG.Framework.UI.Devkit.WorkshopUI
{
	public class SkinEconBundleWizardWindow : Sleek2Window
	{
		public SkinEconBundleWizardWindow()
		{
			base.gameObject.name = "UGC_Skin_Econ_Bundle_Wizard";
			base.tab.label.translation = new TranslatedText(new TranslationReference("SDG", "Devkit.Window.UGC_Skin_Econ_Bundle_Wizard.Title"));
			base.tab.label.translation.format();
			this.inspector = new Sleek2Inspector();
			this.inspector.transform.anchorMin = new Vector2(0f, 0f);
			this.inspector.transform.anchorMax = new Vector2(1f, 1f);
			this.inspector.transform.pivot = new Vector2(0f, 1f);
			this.inspector.transform.offsetMin = new Vector2(0f, 20f);
			this.inspector.transform.offsetMax = new Vector2(0f, 0f);
			this.inspector.inspect(this);
			base.safePanel.addElement(this.inspector);
			this.setupButton = new Sleek2ImageTranslatedLabelButton();
			this.setupButton.transform.anchorMin = new Vector2(0f, 0f);
			this.setupButton.transform.anchorMax = new Vector2(1f, 0f);
			this.setupButton.transform.pivot = new Vector2(0.5f, 1f);
			this.setupButton.transform.offsetMin = new Vector2(0f, 0f);
			this.setupButton.transform.offsetMax = new Vector2(0f, 20f);
			this.setupButton.label.translation = new TranslatedText(new TranslationReference("#SDG::Devkit.Window.UGC_Skin_Econ_Bundle_Wizard.Input.Label"));
			this.setupButton.label.translation.format();
			this.setupButton.clicked += this.handleSetupButtonClicked;
			base.safePanel.addElement(this.setupButton);
		}

		protected virtual void handleSetupButtonClicked(Sleek2ImageButton button)
		{
		}

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Econ_Bundle_Wizard.Workshop_ID.Name", null)]
		public ulong workshopID;

		[Inspectable("#SDG::Devkit.Window.UGC_Skin_Econ_Bundle_Wizard.Pattern_ID.Name", null)]
		public string patternID;

		protected Sleek2Inspector inspector;

		protected Sleek2ImageTranslatedLabelButton setupButton;
	}
}

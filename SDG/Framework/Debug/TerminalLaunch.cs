﻿using System;
using System.Collections.Generic;
using System.IO;
using SDG.Framework.Utilities;
using SDG.Unturned;

namespace SDG.Framework.Debug
{
	public class TerminalLaunch
	{
		public static void execute()
		{
			if (TerminalLaunch.hasExecuted)
			{
				return;
			}
			TerminalLaunch.hasExecuted = true;
			for (int i = 0; i < CommandLineUtility.commands.Count; i++)
			{
				List<string> list = CommandLineUtility.commands[i];
				if (list.Count >= 1)
				{
					List<TerminalCommand> list2 = TerminalUtility.filterCommands(list[0]);
					if (list2.Count == 1)
					{
						TerminalUtility.execute(null, list, list2);
					}
				}
			}
			string[] array;
			if (Dedicator.isDedicated)
			{
				array = new string[]
				{
					ReadWrite.PATH + "/Servers/" + Dedicator.serverID + "/Server/Execute.config"
				};
			}
			else
			{
				array = new string[]
				{
					ReadWrite.PATH + "/Execute.config",
					ReadWrite.PATH + "/Cloud/Execute.config"
				};
			}
			foreach (string path in array)
			{
				if (ReadWrite.fileExists(path, false, false))
				{
					FileStream fileStream = null;
					StreamReader streamReader = null;
					try
					{
						fileStream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read);
						streamReader = new StreamReader(fileStream);
						string input;
						while ((input = streamReader.ReadLine()) != null)
						{
							TerminalUtility.execute(input, null, null);
						}
					}
					finally
					{
						if (fileStream != null)
						{
							fileStream.Close();
						}
						if (streamReader != null)
						{
							streamReader.Close();
						}
					}
				}
				else
				{
					Data data = new Data();
					ReadWrite.writeData(path, false, false, data);
				}
			}
		}

		private static bool hasExecuted;
	}
}

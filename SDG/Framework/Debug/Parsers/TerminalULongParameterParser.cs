﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalULongParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			ulong num;
			if (ulong.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalDoubleParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			double num;
			if (double.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

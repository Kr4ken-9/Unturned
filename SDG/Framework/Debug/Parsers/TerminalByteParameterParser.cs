﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalByteParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			byte b;
			if (byte.TryParse(input, out b))
			{
				return b;
			}
			return null;
		}
	}
}

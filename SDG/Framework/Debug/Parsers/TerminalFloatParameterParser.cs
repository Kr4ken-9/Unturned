﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalFloatParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			float num;
			if (float.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

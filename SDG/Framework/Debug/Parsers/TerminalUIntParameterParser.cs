﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalUIntParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			uint num;
			if (uint.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

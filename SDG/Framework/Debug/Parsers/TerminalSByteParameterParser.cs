﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalSByteParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			sbyte b;
			if (sbyte.TryParse(input, out b))
			{
				return b;
			}
			return null;
		}
	}
}

﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalShortParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			short num;
			if (short.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

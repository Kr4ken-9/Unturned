﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalIntParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			int num;
			if (int.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalLongParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			long num;
			if (long.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

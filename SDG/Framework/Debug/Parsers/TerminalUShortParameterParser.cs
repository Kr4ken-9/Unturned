﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalUShortParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			ushort num;
			if (ushort.TryParse(input, out num))
			{
				return num;
			}
			return null;
		}
	}
}

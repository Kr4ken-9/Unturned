﻿using System;

namespace SDG.Framework.Debug.Parsers
{
	public class TerminalBoolParameterParser : ITerminalParameterParser
	{
		public object parse(string input)
		{
			if (input.Equals("false", StringComparison.OrdinalIgnoreCase))
			{
				return false;
			}
			if (input.Equals("true", StringComparison.OrdinalIgnoreCase))
			{
				return true;
			}
			if (input.Equals("0", StringComparison.OrdinalIgnoreCase) || input.Equals("no", StringComparison.OrdinalIgnoreCase) || input.Equals("n", StringComparison.OrdinalIgnoreCase) || input.Equals("f", StringComparison.OrdinalIgnoreCase))
			{
				return false;
			}
			if (input.Equals("1", StringComparison.OrdinalIgnoreCase) || input.Equals("yes", StringComparison.OrdinalIgnoreCase) || input.Equals("y", StringComparison.OrdinalIgnoreCase) || input.Equals("t", StringComparison.OrdinalIgnoreCase))
			{
				return true;
			}
			return null;
		}
	}
}

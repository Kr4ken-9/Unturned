﻿using System;
using SDG.Framework.Debug.Parsers;
using SDG.Framework.Modules;

namespace SDG.Framework.Debug
{
	public class TerminalNexus : IModuleNexus
	{
		private void handleModulesInitialized()
		{
			TerminalLaunch.execute();
		}

		public void initialize()
		{
			TerminalCommandAttributeFinder.initialize();
			Terminal.initialize();
			Terminal.parserRegistry.add(typeof(bool), new TerminalBoolParameterParser());
			Terminal.parserRegistry.add(typeof(byte), new TerminalByteParameterParser());
			Terminal.parserRegistry.add(typeof(double), new TerminalDoubleParameterParser());
			Terminal.parserRegistry.add(typeof(float), new TerminalFloatParameterParser());
			Terminal.parserRegistry.add(typeof(int), new TerminalIntParameterParser());
			Terminal.parserRegistry.add(typeof(long), new TerminalLongParameterParser());
			Terminal.parserRegistry.add(typeof(sbyte), new TerminalSByteParameterParser());
			Terminal.parserRegistry.add(typeof(short), new TerminalShortParameterParser());
			Terminal.parserRegistry.add(typeof(string), new TerminalStringParameterParser());
			Terminal.parserRegistry.add(typeof(uint), new TerminalUIntParameterParser());
			Terminal.parserRegistry.add(typeof(ulong), new TerminalULongParameterParser());
			Terminal.parserRegistry.add(typeof(ushort), new TerminalUShortParameterParser());
			ModuleHook.onModulesInitialized += this.handleModulesInitialized;
		}

		public void shutdown()
		{
			ModuleHook.onModulesInitialized -= this.handleModulesInitialized;
			Terminal.parserRegistry.remove(typeof(bool));
			Terminal.parserRegistry.remove(typeof(byte));
			Terminal.parserRegistry.remove(typeof(double));
			Terminal.parserRegistry.remove(typeof(float));
			Terminal.parserRegistry.remove(typeof(int));
			Terminal.parserRegistry.remove(typeof(long));
			Terminal.parserRegistry.remove(typeof(sbyte));
			Terminal.parserRegistry.remove(typeof(short));
			Terminal.parserRegistry.remove(typeof(string));
			Terminal.parserRegistry.remove(typeof(uint));
			Terminal.parserRegistry.remove(typeof(ulong));
			Terminal.parserRegistry.remove(typeof(ushort));
			TerminalCommandAttributeFinder.shutdown();
		}
	}
}

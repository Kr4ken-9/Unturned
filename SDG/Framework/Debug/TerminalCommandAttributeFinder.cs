﻿using System;
using System.Collections.Generic;
using System.Reflection;
using System.Runtime.CompilerServices;
using SDG.Framework.Modules;

namespace SDG.Framework.Debug
{
	public class TerminalCommandAttributeFinder
	{
		private static void analyze(Type[] types)
		{
			if (types == null)
			{
				return;
			}
			if (TerminalCommandAttributeFinder.foundTypes.Contains(types))
			{
				return;
			}
			TerminalCommandAttributeFinder.foundTypes.Add(types);
			for (int i = 0; i < types.Length; i++)
			{
				Type type = types[i];
				if (types[i] != null)
				{
					foreach (MethodInfo methodInfo in type.GetMethods())
					{
						object[] array;
						try
						{
							array = methodInfo.GetCustomAttributes(typeof(TerminalCommandMethodAttribute), true);
						}
						catch
						{
							array = null;
						}
						if (array != null && array.Length > 0)
						{
							ParameterInfo[] parameters = methodInfo.GetParameters();
							TerminalCommandMethodAttribute terminalCommandMethodAttribute = array[0] as TerminalCommandMethodAttribute;
							TerminalCommandParameterAttribute[] array2 = new TerminalCommandParameterAttribute[parameters.Length];
							bool flag = true;
							for (int k = 0; k < parameters.Length; k++)
							{
								ParameterInfo parameterInfo = parameters[k];
								object[] array3;
								try
								{
									array3 = parameterInfo.GetCustomAttributes(typeof(TerminalCommandParameterAttribute), true);
								}
								catch
								{
									array3 = null;
								}
								if (array3 == null || array3.Length <= 0)
								{
									flag = false;
									break;
								}
								array2[k] = (array3[0] as TerminalCommandParameterAttribute);
							}
							if (flag)
							{
								TerminalCommandMethodInfo newMethod = new TerminalCommandMethodInfo(terminalCommandMethodAttribute.command, terminalCommandMethodAttribute.description, methodInfo);
								TerminalCommandParameterInfo[] array4 = new TerminalCommandParameterInfo[parameters.Length];
								for (int l = 0; l < parameters.Length; l++)
								{
									TerminalCommandParameterAttribute terminalCommandParameterAttribute = array2[l];
									array4[l] = new TerminalCommandParameterInfo(terminalCommandParameterAttribute.name, terminalCommandParameterAttribute.description, parameters[l].ParameterType, parameters[l].DefaultValue);
								}
								Terminal.registerCommand(new TerminalCommand(newMethod, array4, null));
							}
						}
					}
					foreach (PropertyInfo propertyInfo in type.GetProperties())
					{
						object[] array5;
						try
						{
							array5 = propertyInfo.GetCustomAttributes(typeof(TerminalCommandPropertyAttribute), true);
						}
						catch
						{
							array5 = null;
						}
						if (array5 != null && array5.Length > 0)
						{
							TerminalCommandPropertyAttribute terminalCommandPropertyAttribute = array5[0] as TerminalCommandPropertyAttribute;
							TerminalCommandMethodInfo newMethod2 = new TerminalCommandMethodInfo(terminalCommandPropertyAttribute.command, terminalCommandPropertyAttribute.description, propertyInfo.GetSetMethod());
							Terminal.registerCommand(new TerminalCommand(newMethod2, new TerminalCommandParameterInfo[]
							{
								new TerminalCommandParameterInfo("property", "value", propertyInfo.PropertyType, terminalCommandPropertyAttribute.defaultValue)
							}, propertyInfo.GetGetMethod()));
						}
					}
				}
			}
		}

		private static void onModuleLoaded(SDG.Framework.Modules.Module module)
		{
			TerminalCommandAttributeFinder.analyze(module.types);
		}

		public static void initialize()
		{
			TerminalCommandAttributeFinder.analyze(ModuleHook.coreTypes);
			for (int i = 0; i < ModuleHook.modules.Count; i++)
			{
				SDG.Framework.Modules.Module module = ModuleHook.modules[i];
				TerminalCommandAttributeFinder.analyze(module.types);
				SDG.Framework.Modules.Module module2 = module;
				if (TerminalCommandAttributeFinder.<>f__mg$cache0 == null)
				{
					TerminalCommandAttributeFinder.<>f__mg$cache0 = new ModuleLoaded(TerminalCommandAttributeFinder.onModuleLoaded);
				}
				module2.onModuleLoaded += TerminalCommandAttributeFinder.<>f__mg$cache0;
			}
		}

		public static void shutdown()
		{
			for (int i = 0; i < ModuleHook.modules.Count; i++)
			{
				SDG.Framework.Modules.Module module = ModuleHook.modules[i];
				SDG.Framework.Modules.Module module2 = module;
				if (TerminalCommandAttributeFinder.<>f__mg$cache1 == null)
				{
					TerminalCommandAttributeFinder.<>f__mg$cache1 = new ModuleLoaded(TerminalCommandAttributeFinder.onModuleLoaded);
				}
				module2.onModuleLoaded -= TerminalCommandAttributeFinder.<>f__mg$cache1;
			}
		}

		private static HashSet<Type[]> foundTypes = new HashSet<Type[]>();

		[CompilerGenerated]
		private static ModuleLoaded <>f__mg$cache0;

		[CompilerGenerated]
		private static ModuleLoaded <>f__mg$cache1;
	}
}

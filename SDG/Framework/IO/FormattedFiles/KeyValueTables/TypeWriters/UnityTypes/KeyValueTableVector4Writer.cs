﻿using System;
using UnityEngine;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeWriters.UnityTypes
{
	public class KeyValueTableVector4Writer : IFormattedTypeWriter
	{
		public void write(IFormattedFileWriter writer, object value)
		{
			writer.beginObject();
			Vector4 vector = (Vector4)value;
			writer.writeValue<float>("X", vector.x);
			writer.writeValue<float>("Y", vector.y);
			writer.writeValue<float>("Z", vector.z);
			writer.writeValue<float>("W", vector.w);
			writer.endObject();
		}
	}
}

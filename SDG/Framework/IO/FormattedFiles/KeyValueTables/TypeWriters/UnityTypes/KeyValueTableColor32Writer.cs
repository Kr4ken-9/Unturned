﻿using System;
using UnityEngine;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeWriters.UnityTypes
{
	public class KeyValueTableColor32Writer : IFormattedTypeWriter
	{
		public void write(IFormattedFileWriter writer, object value)
		{
			writer.beginObject();
			Color32 color = (Color32)value;
			writer.writeValue<byte>("R", color.r);
			writer.writeValue<byte>("G", color.g);
			writer.writeValue<byte>("B", color.b);
			writer.writeValue<byte>("A", color.a);
			writer.endObject();
		}
	}
}

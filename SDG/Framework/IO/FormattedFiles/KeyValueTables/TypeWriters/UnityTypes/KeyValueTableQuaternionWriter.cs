﻿using System;
using UnityEngine;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeWriters.UnityTypes
{
	public class KeyValueTableQuaternionWriter : IFormattedTypeWriter
	{
		public void write(IFormattedFileWriter writer, object value)
		{
			writer.beginObject();
			Quaternion quaternion = (Quaternion)value;
			writer.writeValue<float>("X", quaternion.x);
			writer.writeValue<float>("Y", quaternion.y);
			writer.writeValue<float>("Z", quaternion.z);
			writer.writeValue<float>("W", quaternion.w);
			writer.endObject();
		}
	}
}

﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableUIntReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			uint num;
			uint.TryParse(reader.readValue(), out num);
			return num;
		}
	}
}

﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableByteReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			byte b;
			byte.TryParse(reader.readValue(), out b);
			return b;
		}
	}
}

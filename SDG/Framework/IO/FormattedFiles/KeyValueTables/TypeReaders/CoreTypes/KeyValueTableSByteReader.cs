﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableSByteReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			sbyte b;
			sbyte.TryParse(reader.readValue(), out b);
			return b;
		}
	}
}

﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableShortReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			short num;
			short.TryParse(reader.readValue(), out num);
			return num;
		}
	}
}

﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableFloatReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			float num;
			float.TryParse(reader.readValue(), out num);
			return num;
		}
	}
}

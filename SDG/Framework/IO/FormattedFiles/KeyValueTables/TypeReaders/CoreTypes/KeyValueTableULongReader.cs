﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableULongReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			ulong num;
			ulong.TryParse(reader.readValue(), out num);
			return num;
		}
	}
}

﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableIntReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			int num;
			int.TryParse(reader.readValue(), out num);
			return num;
		}
	}
}

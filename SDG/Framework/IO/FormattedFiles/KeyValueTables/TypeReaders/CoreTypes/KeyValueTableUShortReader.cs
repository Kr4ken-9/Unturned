﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableUShortReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			ushort num;
			ushort.TryParse(reader.readValue(), out num);
			return num;
		}
	}
}

﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.CoreTypes
{
	public class KeyValueTableLongReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			long num;
			long.TryParse(reader.readValue(), out num);
			return num;
		}
	}
}

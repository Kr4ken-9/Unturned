﻿using System;

namespace SDG.Framework.IO.FormattedFiles.KeyValueTables.TypeReaders.SystemTypes
{
	public class KeyValueTableGUIDReader : IFormattedTypeReader
	{
		public object read(IFormattedFileReader reader)
		{
			return new Guid(reader.readValue());
		}
	}
}

﻿using System;
using SDG.Framework.Debug;
using SDG.Framework.Devkit.Interactable;
using SDG.Framework.Rendering;
using SDG.Unturned;
using UnityEngine;

namespace SDG.Framework.Devkit.Tools
{
	public class DevkitPositionHandle : MonoBehaviour, IDevkitHandle, IDevkitInteractableBeginHoverHandler, IDevkitInteractableBeginDragHandler, IDevkitInteractableContinueDragHandler, IDevkitInteractableEndDragHandler, IDevkitInteractableEndHoverHandler
	{
		[TerminalCommandProperty("input.devkit.pivot.position.delta_sensitivity", "multiplier for position delta", 1)]
		public static float handleSensitivity
		{
			get
			{
				return DevkitPositionHandle._handleSensitivity;
			}
			set
			{
				DevkitPositionHandle._handleSensitivity = value;
				TerminalUtility.printCommandPass("Set delta_sensitivity to: " + DevkitPositionHandle.handleSensitivity);
			}
		}

		[TerminalCommandProperty("input.devkit.pivot.position.screensize", "percentage of screen size", 0.5f)]
		public static float handleScreensize
		{
			get
			{
				return DevkitPositionHandle._handleScreensize;
			}
			set
			{
				DevkitPositionHandle._handleScreensize = value;
				TerminalUtility.printCommandPass("Set screensize to: " + DevkitPositionHandle.handleScreensize);
			}
		}

		public event DevkitPositionHandle.DevkitPositionTransformedHandler transformed;

		protected bool isSnapping
		{
			get
			{
				return Input.GetKey(ControlsSettings.other);
			}
		}

		public void suggestTransform(Vector3 position, Quaternion rotation)
		{
			base.transform.position = position;
			base.transform.rotation = rotation;
		}

		public void beginHover(InteractionData data)
		{
			if (data.collider == this.handleAxis_X)
			{
				this.hover = DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_X;
			}
			else if (data.collider == this.handleAxis_Y)
			{
				this.hover = DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Y;
			}
			else if (data.collider == this.handleAxis_Z)
			{
				this.hover = DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Z;
			}
			else if (data.collider == this.handlePlane_X)
			{
				this.hover = DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_X;
			}
			else if (data.collider == this.handlePlane_Y)
			{
				this.hover = DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Y;
			}
			else if (data.collider == this.handlePlane_Z)
			{
				this.hover = DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Z;
			}
		}

		public void beginDrag(InteractionData data)
		{
			if (data.collider == this.handleAxis_X)
			{
				this.drag = DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_X;
			}
			else if (data.collider == this.handleAxis_Y)
			{
				this.drag = DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Y;
			}
			else if (data.collider == this.handleAxis_Z)
			{
				this.drag = DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Z;
			}
			else if (data.collider == this.handlePlane_X)
			{
				this.drag = DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_X;
			}
			else if (data.collider == this.handlePlane_Y)
			{
				this.drag = DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Y;
			}
			else if (data.collider == this.handlePlane_Z)
			{
				this.drag = DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Z;
			}
			this.transformOrigin = base.transform.position;
			this.prevPositionResult = this.transformOrigin;
			this.handleOffset = data.point - base.transform.position;
			this.mouseOrigin = Input.mousePosition;
		}

		public void continueDrag(InteractionData data)
		{
			Vector3 vector;
			Vector3 onNormal;
			Vector3 onNormal2;
			if (this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_X || this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_X)
			{
				vector = base.transform.right;
				onNormal = base.transform.up;
				onNormal2 = base.transform.forward;
			}
			else if (this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Y || this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Y)
			{
				vector = base.transform.up;
				onNormal = base.transform.right;
				onNormal2 = base.transform.forward;
			}
			else
			{
				if (this.drag != DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Z && this.drag != DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Z)
				{
					return;
				}
				vector = base.transform.forward;
				onNormal = base.transform.right;
				onNormal2 = base.transform.up;
			}
			if (this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_X || this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Y || this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Z)
			{
				Vector3 b = MainCamera.instance.WorldToScreenPoint(this.transformOrigin);
				Vector3 vector2 = MainCamera.instance.WorldToScreenPoint(this.transformOrigin + vector) - b;
				Vector3 lhs = Input.mousePosition - this.mouseOrigin;
				float num = Vector3.Dot(lhs, vector2.normalized) / vector2.magnitude;
				num *= DevkitPositionHandle.handleSensitivity;
				if (Input.GetKey(ControlsSettings.snap))
				{
					num = (float)Mathf.RoundToInt(num / DevkitSelectionToolOptions.instance.snapPosition) * DevkitSelectionToolOptions.instance.snapPosition;
				}
				Vector3 a = this.transformOrigin + num * vector;
				Vector3 delta = a - this.prevPositionResult;
				this.triggerTransformed(delta);
				this.prevPositionResult = a;
			}
			else if (this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_X || this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Y || this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Z)
			{
				Plane plane = default(Plane);
				plane.SetNormalAndPosition(vector, this.transformOrigin);
				Ray pointerToWorldRay = DevkitInput.pointerToWorldRay;
				float d;
				plane.Raycast(pointerToWorldRay, out d);
				Vector3 a2 = pointerToWorldRay.origin + pointerToWorldRay.direction * d - this.handleOffset;
				Vector3 vector3 = a2 - this.transformOrigin;
				Vector3 vector4 = Vector3.Project(vector3, onNormal);
				Vector3 vector5 = Vector3.Project(vector3, onNormal2);
				float num2 = vector4.magnitude;
				num2 *= DevkitPositionHandle.handleSensitivity;
				if (Input.GetKey(ControlsSettings.snap))
				{
					num2 = (float)Mathf.RoundToInt(num2 / DevkitSelectionToolOptions.instance.snapPosition) * DevkitSelectionToolOptions.instance.snapPosition;
				}
				float num3 = vector5.magnitude;
				num3 *= DevkitPositionHandle.handleSensitivity;
				if (Input.GetKey(ControlsSettings.snap))
				{
					num3 = (float)Mathf.RoundToInt(num3 / DevkitSelectionToolOptions.instance.snapPosition) * DevkitSelectionToolOptions.instance.snapPosition;
				}
				a2 = this.transformOrigin + vector4.normalized * num2 + vector5.normalized * num3;
				Vector3 delta2 = a2 - this.prevPositionResult;
				this.triggerTransformed(delta2);
				this.prevPositionResult = a2;
			}
		}

		public void endDrag(InteractionData data)
		{
			this.drag = DevkitPositionHandle.EDevkitPositionHandleSelection.NONE;
		}

		public void endHover(InteractionData data)
		{
			this.hover = DevkitPositionHandle.EDevkitPositionHandleSelection.NONE;
		}

		protected void triggerTransformed(Vector3 delta)
		{
			if (this.transformed != null)
			{
				this.transformed(this, delta);
			}
		}

		protected void arrow(Vector3 normal, Vector3 view, bool isDragging, bool isHovering, Color color)
		{
			Vector3 b = Vector3.Cross(view, normal) * 0.1f;
			if (isDragging && this.isSnapping)
			{
				GL.Color(new Color(0f, 0f, 0f, 0.5f));
				float num = DevkitSelectionToolOptions.instance.snapPosition / base.transform.localScale.x;
				int num2 = Mathf.Max(2, Mathf.CeilToInt(2f / num));
				for (int i = -num2; i <= num2; i++)
				{
					Vector3 a = normal * num * (float)i;
					GLUtility.line(a - b, a + b);
				}
			}
			GL.Color((!isDragging) ? ((!isHovering) ? color : Color.yellow) : Color.white);
			GLUtility.line(Vector3.zero, normal);
			Vector3 a2 = normal * 0.75f;
			GLUtility.line(normal, a2 - b);
			GLUtility.line(normal, a2 + b);
		}

		protected void plane(Vector3 horizontal, Vector3 vertical, bool isDragging, bool isHovering, Color color)
		{
			if (isDragging && this.isSnapping)
			{
				GL.Color(new Color(0f, 0f, 0f, 0.5f));
				float num = DevkitSelectionToolOptions.instance.snapPosition / base.transform.localScale.x;
				int num2 = Mathf.Max(2, Mathf.CeilToInt(2f / num));
				Vector3 b = horizontal * num * (float)num2;
				Vector3 b2 = vertical * num * (float)num2;
				for (int i = -num2; i <= num2; i++)
				{
					Vector3 a = horizontal * num * (float)i;
					GLUtility.line(a - b2, a + b2);
				}
				for (int j = -num2; j <= num2; j++)
				{
					Vector3 a2 = vertical * num * (float)j;
					GLUtility.line(a2 - b, a2 + b);
				}
			}
			GL.Color((!isDragging) ? ((!isHovering) ? color : Color.yellow) : Color.white);
			Vector3 vector = horizontal * 0.25f;
			Vector3 vector2 = vertical * 0.25f;
			GLUtility.line(vector, vector + vector2);
			GLUtility.line(vector2, vector2 + vector);
		}

		protected void handleGLRender()
		{
			GLUtility.LINE_FLAT_COLOR.SetPass(0);
			GL.Begin(1);
			GLUtility.matrix = base.transform.localToWorldMatrix;
			this.plane(new Vector3(0f, this.inversion.y, 0f), new Vector3(0f, 0f, this.inversion.z), this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_X, this.hover == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_X, Color.red);
			this.plane(new Vector3(this.inversion.x, 0f, 0f), new Vector3(0f, 0f, this.inversion.z), this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Y, this.hover == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Y, Color.green);
			this.plane(new Vector3(this.inversion.x, 0f, 0f), new Vector3(0f, this.inversion.y, 0f), this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Z, this.hover == DevkitPositionHandle.EDevkitPositionHandleSelection.PLANE_Z, Color.blue);
			GLUtility.matrix = Matrix4x4.TRS(base.transform.position, Quaternion.identity, base.transform.localScale);
			this.arrow(base.transform.right * this.inversion.x, GLUtility.getDirectionFromViewToArrow(MainCamera.instance.transform.position, base.transform.position, base.transform.right * this.inversion.x), this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_X, this.hover == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_X, Color.red);
			this.arrow(base.transform.up * this.inversion.y, GLUtility.getDirectionFromViewToArrow(MainCamera.instance.transform.position, base.transform.position, base.transform.up * this.inversion.y), this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Y, this.hover == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Y, Color.green);
			this.arrow(base.transform.forward * this.inversion.z, GLUtility.getDirectionFromViewToArrow(MainCamera.instance.transform.position, base.transform.position, base.transform.forward * this.inversion.z), this.drag == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Z, this.hover == DevkitPositionHandle.EDevkitPositionHandleSelection.AXIS_Z, Color.blue);
			GL.End();
		}

		protected void updateScale()
		{
			Vector3 position = MainCamera.instance.transform.position;
			Vector3 a = base.transform.position - position;
			float magnitude = a.magnitude;
			Vector3 lhs = a / magnitude;
			float num = magnitude;
			num *= DevkitPositionHandle.handleScreensize;
			base.transform.localScale = new Vector3(num, num, num);
			this.inversion.x = (float)((!DevkitSelectionToolOptions.instance.lockHandles && Vector3.Dot(lhs, base.transform.right) >= 0f) ? -1 : 1);
			this.inversion.y = (float)((!DevkitSelectionToolOptions.instance.lockHandles && Vector3.Dot(lhs, base.transform.up) >= 0f) ? -1 : 1);
			this.inversion.z = (float)((!DevkitSelectionToolOptions.instance.lockHandles && Vector3.Dot(lhs, base.transform.forward) >= 0f) ? -1 : 1);
			this.handleAxis_X.center = new Vector3(this.inversion.x * 0.5f, 0f, 0f);
			this.handleAxis_Y.center = new Vector3(0f, this.inversion.y * 0.5f, 0f);
			this.handleAxis_Z.center = new Vector3(0f, 0f, this.inversion.z * 0.5f);
			this.handlePlane_X.center = new Vector3(0f, this.inversion.y * 0.125f, this.inversion.z * 0.125f);
			this.handlePlane_Y.center = new Vector3(this.inversion.x * 0.125f, 0f, this.inversion.z * 0.125f);
			this.handlePlane_Z.center = new Vector3(this.inversion.x * 0.125f, this.inversion.y * 0.125f, 0f);
		}

		protected void Update()
		{
			if (MainCamera.instance == null)
			{
				return;
			}
			this.updateScale();
		}

		protected void OnEnable()
		{
			GLRenderer.render += this.handleGLRender;
			base.gameObject.layer = LayerMasks.LOGIC;
			this.handleAxis_X = base.gameObject.AddComponent<BoxCollider>();
			this.handleAxis_X.size = new Vector3(1f, 0.1f, 0.1f);
			this.handleAxis_Y = base.gameObject.AddComponent<BoxCollider>();
			this.handleAxis_Y.size = new Vector3(0.1f, 1f, 0.1f);
			this.handleAxis_Z = base.gameObject.AddComponent<BoxCollider>();
			this.handleAxis_Z.size = new Vector3(0.1f, 0.1f, 1f);
			this.handlePlane_X = base.gameObject.AddComponent<BoxCollider>();
			this.handlePlane_X.size = new Vector3(0f, 0.25f, 0.25f);
			this.handlePlane_Y = base.gameObject.AddComponent<BoxCollider>();
			this.handlePlane_Y.size = new Vector3(0.25f, 0f, 0.25f);
			this.handlePlane_Z = base.gameObject.AddComponent<BoxCollider>();
			this.handlePlane_Z.size = new Vector3(0.25f, 0.25f, 0f);
			this.updateScale();
		}

		protected void OnDisable()
		{
			GLRenderer.render -= this.handleGLRender;
			UnityEngine.Object.Destroy(this.handleAxis_X);
			UnityEngine.Object.Destroy(this.handleAxis_Y);
			UnityEngine.Object.Destroy(this.handleAxis_Z);
			UnityEngine.Object.Destroy(this.handlePlane_X);
			UnityEngine.Object.Destroy(this.handlePlane_Y);
			UnityEngine.Object.Destroy(this.handlePlane_Z);
		}

		protected static float _handleSensitivity = 1f;

		protected static float _handleScreensize = 0.5f;

		protected DevkitPositionHandle.EDevkitPositionHandleSelection drag;

		protected DevkitPositionHandle.EDevkitPositionHandleSelection hover;

		protected BoxCollider handleAxis_X;

		protected BoxCollider handleAxis_Y;

		protected BoxCollider handleAxis_Z;

		protected BoxCollider handlePlane_X;

		protected BoxCollider handlePlane_Y;

		protected BoxCollider handlePlane_Z;

		protected Vector3 inversion;

		protected Vector3 transformOrigin;

		protected Vector3 mouseOrigin;

		protected Vector3 handleOffset;

		protected Vector3 prevPositionResult;

		public delegate void DevkitPositionTransformedHandler(DevkitPositionHandle handle, Vector3 delta);

		protected enum EDevkitPositionHandleSelection
		{
			NONE,
			AXIS_X,
			AXIS_Y,
			AXIS_Z,
			PLANE_X,
			PLANE_Y,
			PLANE_Z
		}
	}
}
